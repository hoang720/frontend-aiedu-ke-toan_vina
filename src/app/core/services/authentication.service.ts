import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MOODLE_CURRENT_USE, MOODLE_JWT_TOKEN } from '@core/config/constant';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  returnURL: string

  private userSubject = new BehaviorSubject(null);
  private user = this.userSubject.asObservable();

  constructor(
    private router: Router, 
    private http: HttpClient,
    private route: ActivatedRoute
  ) {
    this.returnURL = this.route.snapshot.queryParams['returnURL'] || '/';
  }

  ngOnDestroy() {
    this.stopRefreshTokenTimer();
  }
  setUser(UserData: any){
    localStorage.setItem(MOODLE_CURRENT_USE, JSON.stringify(UserData));
    this.userSubject.next(UserData);
  }
  getUser(){
    return this.user;
  }
  public get userValue(): any {
    return this.userSubject.value;
  }
  login(
    loginDataValue: {
      username: string;
      password: string;
    }
  ){
    return this.http.post<any>(`${environment.apiURL}/user/login`, loginDataValue, {
      withCredentials: true,
    }).pipe(map((res:any) => {
      console.log(loginDataValue)
      this.saveAccessTokenInLocalStorage(res.access_token);
      return res
    }))
  }
  logout(){
    this.stopRefreshTokenTimer();
    this.setUser(null);
    localStorage.removeItem(MOODLE_CURRENT_USE);
    this.router.navigateByUrl('/login')
  }
  saveAccessTokenInLocalStorage(accessToken: string) {
    let accessTokenEncode = btoa(accessToken);
    localStorage.setItem(MOODLE_JWT_TOKEN, JSON.stringify(accessTokenEncode))
  }

  getAccesTokenFromLocalStorage() {
    const accessTokenEncodeJson = localStorage.getItem(MOODLE_JWT_TOKEN);

    if (accessTokenEncodeJson != null) {
      let accessTokenEncode = JSON.parse(accessTokenEncodeJson);
      return atob(accessTokenEncode);
    }
    return null;
  }


  private refreshTokenTimeout: any;
  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

}
