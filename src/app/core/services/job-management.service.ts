import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobManagementService {
  private BASE_URL = environment.apiURL;
  constructor(private http: HttpClient) {}

  getJob(): Observable<any>{
    let apiUrl = `${this.BASE_URL}/viec_lam`;
    return this.http.get<any>(apiUrl)
  }
  
  getJobByName(name: any): Observable<any>{
    let apiUrl = `${this.BASE_URL}/viec_lam?ten_viec_lam=${name}`;
    return this.http.get<any>(apiUrl)
  }

  addJob(Job: any): Observable<any>{
    let apiUrl = `${this.BASE_URL}/viec_lam`;
    return this.http.post<any>(apiUrl, Job)
  }

  updateJob(Job: any, id: string): Observable<any>{
    let apiUrl = `${this.BASE_URL}/viec_lam/${id}`;
    return this.http.put<any>(apiUrl, Job)
  }

  deleteJob(id: string): Observable<any>{
    let apiUrl = `${this.BASE_URL}/viec_lam/${id}`;
    return this.http.delete<any>(apiUrl)
  }
}
