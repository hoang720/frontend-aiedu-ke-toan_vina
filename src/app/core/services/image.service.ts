import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
@Injectable({providedIn: 'root',})

export class ImageService {
  url = `${environment.apiImage}`;
  constructor(private http: HttpClient) {}
  
  add(anh: any):Observable<any>{
    var formData: any = new FormData();
    formData.append("uploaded_file", anh);
    return this.http.post<any>(`${this.url}`,formData).pipe(first());
  }
}

