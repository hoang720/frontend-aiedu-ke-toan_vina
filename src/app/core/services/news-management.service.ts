import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsManagementService {
  private BASE_URL = environment.apiURL;
  constructor(private http: HttpClient) {}

  getNew(): Observable<any>{
    let apiUrl = `${this.BASE_URL}/bai_viet`;
    return this.http.get<any>(apiUrl)
  }

  getNewByName(name: any): Observable<any>{
    let apiUrl = `${this.BASE_URL}/bai_viet?tieu_de=${name}`;
    return this.http.get<any>(apiUrl)
  }

  addNew(News: any): Observable<any>{
    let apiUrl = `${this.BASE_URL}/bai_viet`;
    return this.http.post<any>(apiUrl, News)
  }

  updateNew(News: any, id: string): Observable<any>{
    let apiUrl = `${this.BASE_URL}/bai_viet/${id}`;
    return this.http.put<any>(apiUrl, News)
  }

  delete(id: string){
    let apiUrl = `${this.BASE_URL}/bai_viet/${id}`;
    return this.http.delete<any>(apiUrl)
  }
}
