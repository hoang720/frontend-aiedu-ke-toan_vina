import { Inject, Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
    providedIn:'root'
})
export class LanguageChangeSharingService{
    private language = new BehaviorSubject<string>('vi');
    
    public get Language():string
    {
        return this.language.value  
    }

    public set Language(language_name:string)
    {
        this.language.next(language_name)
        //save to local storage
        localStorage.setItem('LANGUAGE',language_name)
    }

    public getLanguageObservable():Observable<any>
    {
        return this.language 
    }
}