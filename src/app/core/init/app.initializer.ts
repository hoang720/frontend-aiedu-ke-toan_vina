import { AuthenticationService } from '@core/services/authentication.service';

export function appInitializer(authService: AuthenticationService) {
  return () => {
    new Promise((resolve:any, reject) => {
      let accessToken = authService.getAccesTokenFromLocalStorage();
      if (accessToken) {
        authService.refreshToken().subscribe((res) => {
          authService.userInfor().subscribe().add(resolve);
        });
      } else resolve();
    });
  };
}
