export function mapToFormData(body: any): FormData{
    const formData = new FormData();
    for (const property in body) {
        if (body.hasOwnProperty(property) && isNotEmpty(body[property])) {
          formData.append(property, body[property]);
        }
      }
    return formData;
}

export function isNotEmpty(args: any): boolean {
  return !isEmpty(args)
}

export function isEmpty(args: any): boolean {
  return (
    args === null || args === undefined || args === '' || args.length === 0
  );
}