import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'moodle-admin-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();

  constructor(
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {}

  toggleSidebar(){
    this.toggleSidebarForMe.emit()
  }

  logOut(){
    this.authenticationService.logout()
  }
}
