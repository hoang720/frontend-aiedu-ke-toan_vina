import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout.component';

const routes: Routes = [
  { 
    path: '', component: AdminLayoutComponent,
    children: [
      { path: 'news-management', loadChildren: () => import('../page/news-management/news-management.module').then((m) => m.NewsManagementModule)},
      { path: 'job-management', loadChildren: () => import('../page/job-management/job-management.module').then((m) => m.JobManagementModule)}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminLayoutRoutingModule { }
