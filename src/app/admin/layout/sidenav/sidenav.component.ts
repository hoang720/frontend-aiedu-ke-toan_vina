import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  List: any = [
    {icon: 'assignment', title: 'Quản lý tin tức', path: 'news-management'},
    {icon: 'work', title: 'Việc làm kế toán', path: 'job-management'},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
