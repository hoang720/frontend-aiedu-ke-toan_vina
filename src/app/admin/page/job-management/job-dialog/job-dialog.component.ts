import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-job-dialog',
  templateUrl: './job-dialog.component.html',
  styleUrls: ['./job-dialog.component.scss']
})
export class JobDialogComponent implements OnInit {
  action: string;
  jobformdata: any;
  fg: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<JobDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any, fb: FormBuilder
  ) {
    this.action = this.data.action;
    this.jobformdata = this.data.data;
    this.fg = fb.group({
      ten_viec_lam: new FormControl( this.jobformdata.ten_viec_lam, [Validators.required]),
      anh: [null],
      dia_chi: new FormControl( this.jobformdata.dia_chi, [Validators.required]),
      mo_ta: new FormControl( this.jobformdata.mo_ta, [Validators.required]),
      muc_luong: new FormControl( this.jobformdata.muc_luong, [Validators.required])
    })
  }

  ngOnInit(): void {}

  uploadFile(event: any) {
    const file = event.target.files[0];
    this.fg.patchValue({
      anh: file,
    });
    this.fg.get('anh')
  }

  Action(){
    this.dialogRef.close({action: this.action, data: this.fg.value})
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }
}
