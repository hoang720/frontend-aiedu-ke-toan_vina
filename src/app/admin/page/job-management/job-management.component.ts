import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { JobManagementService } from '@core/services/job-management.service';
import { JobDialogComponent } from './job-dialog/job-dialog.component';
import { ImageService } from '@core/services/image.service';

@Component({
  selector: 'app-job-management',
  templateUrl: './job-management.component.html',
  styleUrls: ['./job-management.component.scss']
})
export class JobManagementComponent implements OnInit {
  job_list: Observable<any> = new Observable<any>();
  constructor(
    private jobService: JobManagementService,
    private imageService: ImageService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.job_list = this.getJob()
  }

  getJob(){
    return this.jobService.getJob()
  }

  formhttp(link: any){
    return link.replace('http://45.77.245.61:6868/',"https://ketoanvina.com.vn/image/")
  }
  openDialog(action: any, data: any): void{
    const dialogRef = this.dialog.open(JobDialogComponent, {
      width: '500px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
        return
      if(result.action == 'Thêm'){
        var add_job = {
          "anh": result.data.anh,
          "ten_viec_lam": result.data.ten_viec_lam,
          "dia_chi": result.data.dia_chi,
          "mo_ta": result.data.mo_ta,
          "muc_luong": result.data.muc_luong,
        }
        if(result.data.anh){
          this.imageService.add(result.data.anh).subscribe((img_link) => {
            add_job.anh = img_link
            this.addJob(add_job)
          })
        }
        else{
          this.addJob(add_job)
        }
        console.log(add_job)
      }
      else if (result.action == 'Cập Nhật'){
        var update_job = {
          "anh": result.data.anh,
          "ten_viec_lam": result.data.ten_viec_lam,
          "dia_chi": result.data.dia_chi,
          "mo_ta": result.data.mo_ta,
          "muc_luong": result.data.muc_luong,
        }
        if(result.data.anh){
          this.imageService.add(result.data.anh).subscribe((img_link) => {
            update_job.anh = img_link
            this.updateJob(update_job, data._id)
          })
        }
        else{
          this.updateJob(update_job, data._id)
        }
      }
    })
  }

  addJob(Job: any){
    this.jobService.addJob(Job).subscribe((data)=>{
      Swal.fire({
        title: 'Thêm thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.job_list = this.getJob()
    })
  }

  updateJob(Job: any, id: string){
    this.jobService.updateJob(Job, id).subscribe((data)=>{
      Swal.fire({
        title: 'Cập Nhật thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.job_list = this.getJob()
    })
  }

  deleteJob(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.jobService.deleteJob(id).subscribe(()=>{
          Swal.fire(
            'Thành công!',
            'Thông tin đã được xóa',
            'success'
          )
          this.job_list = this.getJob()
        })
      }
    })
  }
}
