import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsManagementComponent } from './news-management.component';

const routes: Routes = [
  { path: '', component: NewsManagementComponent },
  { 
    path: 'more-detail',
    loadChildren: () =>import('./news-detail/news-detail.module').then(m => m.NewsDetailModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsManagementRoutingModule { }
