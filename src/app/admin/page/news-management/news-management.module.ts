import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsManagementRoutingModule } from './news-management-routing.module';
import { NewsManagementComponent } from './news-management.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogModule } from '@angular/material/dialog';
import { NewsDialogComponent } from './news-dialog/news-dialog.component';
import { MatCardModule } from '@angular/material/card';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [
    NewsManagementComponent,
    NewsDialogComponent
  ],
  imports: [
    CommonModule,
    NewsManagementRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    AngularEditorModule,
  ]
})
export class NewsManagementModule { }
