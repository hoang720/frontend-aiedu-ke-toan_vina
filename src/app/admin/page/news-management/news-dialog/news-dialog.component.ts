import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-news-dialog',
  templateUrl: './news-dialog.component.html',
  styleUrls: ['./news-dialog.component.scss'],
})
export class NewsDialogComponent implements OnInit {
  action: string;
  newformdata: any;
  fg: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<NewsDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder
  ) {
    this.action = this.data.action;
    this.newformdata = this.data.data;
    this.fg = fb.group({
      tieu_de: new FormControl( this.newformdata.tieu_de, [Validators.required]),
      noi_dung: new FormControl( this.newformdata.noi_dung, [Validators.required]),
      anh: [null],
      mo_ta: new FormControl( this.newformdata.mo_ta, [Validators.required])
    })
  }

  ngOnInit(): void {}

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '300px',
    minHeight: '5rem',
    placeholder: 'Nhập nội dung....',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    enableToolbar: true,
    showToolbar: true,
    toolbarHiddenButtons: [
      [
        'bold',
        'fontSize'
      ]
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h2",
      },
    ]
  }

  uploadFile(event: any) {
    const file = event.target.files[0];
    this.fg.patchValue({
      anh: file,
    });
    this.fg.get('anh')
  }

  doAction(){
    this.dialogRef.close({action: this.action, data: this.fg.value})
  }

  closeDialog(){
    this.dialogRef.close({ event:'Hủy Bỏ' })
  }

}
