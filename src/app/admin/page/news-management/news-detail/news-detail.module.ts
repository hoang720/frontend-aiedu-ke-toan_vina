import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsDetailRoutingModule } from './news-detail-routing.module';
import { NewsDetailComponent } from './news-detail.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    NewsDetailComponent
  ],
  imports: [
    CommonModule,
    NewsDetailRoutingModule,
    MatIconModule
  ]
})
export class NewsDetailModule { }
