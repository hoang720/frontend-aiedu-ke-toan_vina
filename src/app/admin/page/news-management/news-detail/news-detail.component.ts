import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NewsManagementService } from '@core/services/news-management.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit {
  news_list: Observable<any> = new Observable<any>();
  news_name: any;
  data_global: any;

  constructor(
    private newService: NewsManagementService,
    private route: ActivatedRoute,
  ) {
    this.data_global = JSON.parse(localStorage.getItem('DATA_GLOBAL')!)
    this.news_name = this.route.snapshot.paramMap.get('news_name')
  }

  ngOnInit(): void {
    this.news_list = this.getNewsByName(this.news_name)
  }
  getNewsByName(name: any){
    return this.newService.getNewByName(name)
  }
}
