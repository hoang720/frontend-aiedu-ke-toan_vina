import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { NewsManagementService } from '@core/services/news-management.service';
import { NewsDialogComponent } from './news-dialog/news-dialog.component';
import { ImageService } from '@core/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-management',
  templateUrl: './news-management.component.html',
  styleUrls: ['./news-management.component.scss']
})
export class NewsManagementComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  news_list: Observable<any> = new Observable<any>();
  showMore = false;
  constructor(
    private newService: NewsManagementService,
    private imageService: ImageService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.news_list = this.getNews()
  }

  getNews(){
    return this.newService.getNew()
  }

  newsDetail(page_name: any, news_name: any){
    this.router.navigateByUrl(`admin/news-management/${page_name}/${news_name}`);
  }
  formhttp(link: any){
    return link.replace('http://45.77.245.61:6868/',"https://ketoanvina.com.vn/image/")
  }
  openDialog(action: any, data: any): void{
    const dialogRef = this.dialog.open(NewsDialogComponent, {
      width: '900px',
      height: '600px',
      data: {
        action: action,
        data: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result)
        return
      if(result.action == 'Thêm'){
        var add_data = {
          "anh": result.data.anh,
          "tieu_de": result.data.tieu_de,
          "noi_dung": result.data.noi_dung,
          "mo_ta": result.data.mo_ta,
        }
        if(result.data.anh){
          this.imageService.add(result.data.anh).subscribe((image_link) => {
            add_data.anh = image_link
            this.addNews(add_data)
          })
        }
        else{
          this.addNews(add_data)
        }
        console.log(add_data)
      }else if (result.action == 'Cập Nhật'){
        var update_data = {
          "anh": result.data.anh,
          "tieu_de": result.data.tieu_de,
          "noi_dung": result.data.noi_dung,
          "mo_ta": result.data.mo_ta,
        }
        if(result.data.anh){
          this.imageService.add(result.data.anh).subscribe((image_link) => {
            update_data.anh = image_link
            this.updateNews(update_data, data._id)
          })
        }
        else{
          this.updateNews(update_data, data._id)
        }
        console.log(update_data)
      }
    })
  }
  addNews(News: any){
    this.newService.addNew(News).subscribe((data) => {
      Swal.fire({
        title: 'Thêm thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.news_list = this.getNews()
    })
  }
  updateNews(News: any, id: string){
    this.newService.updateNew(News, id).subscribe((data) => {
      Swal.fire({
        title: 'Cập Nhật thành công!',
        icon: 'success',
        timer: 3000,
        showConfirmButton: true,
      });
      this.news_list = this.getNews()
    })
  }
  deleteNew(id: string){
    Swal.fire({
      title: 'Bạn chắc chắn muốn xóa ?',
      text: "Thông tin sẽ bị xóa và không thể quay trở lại !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Hủy'
    }).then((result) => {
      if(result.isConfirmed){
        this.newService.delete(id).subscribe(() => {
          Swal.fire(
            'Thành công!',
            'Thông tin đã được xóa',
            'success'
          )
          this.news_list = this.getNews()
        })
      }
    })
  }
}
