import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-footers',
  templateUrl: './footers.component.html',
  styleUrls: ['./footers.component.scss'],
})
export class FootersComponent {
  footer_content: any = {
    Content: {
      vi: [
        {
          content: `
            Cơ sở Hà Nội: P601 Tòa nhà Sunrise Building số 90 Trần Thái Tông, Quận Cầu Giấy, TP Hà Nội.
            Cơ sở Hồ Chí Minh: Số 95 đường số 5, Phường An Phú, Quận 2, TP. Hồ Chí Minh
            Cơ sở Hải Phòng: 4/23 phố Phạm Huy Thông, P. Lam Sơn, Q. Lê Chân, Hải Phòng
            Cơ sở Thái Bình: Số nhà 17/1, Ngõ 515 Lý Thường Kiệt, Trần Lãm, Thái Bình
            Điện thoại : 024.32008354/024.32008364.
            Hotline : 0967 66 1212.
            Zalo: 0906298089
            Email: dichvuketoanvina@gmail.com
          `
        }
      ],
      us: [
        {
          content: `
            Hanoi campus: P601 Sunrise Building at 90 Tran Thai Tong, Cau Giay District, Hanoi.
            Ho Chi Minh Campus: No. 95, 5th Street, An Phu Ward, District 2, City. Ho Chi Minh
            Hai Phong campus: 4/23 Pham Huy Thong Street, Lam Son Ward, Le Chan District, Hai Phong
            Thai Binh campus: House number 17/1, Alley 515 Ly Thuong Kiet, Tran Lam, Thai Binh
            Phone: 024.32008354/024.32008364.
            Hotline : 0967 66 1212.
            Zalo: 0906298089
            Email: dichvuketoanvina@gmail.com
          `
        }
      ]
    },
  }



  language_name:string = 'us'
  constructor(private LanguageService:LanguageChangeSharingService){
    //real time when user change another language
    this.LanguageService.getLanguageObservable().subscribe((language_name:string)=>
    {
      this.language_name = language_name
    })
  }
  
}
