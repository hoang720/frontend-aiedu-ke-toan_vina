import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'app-headers',
  templateUrl: './headers.component.html',
  styleUrls: ['./headers.component.scss'],
})
export class HeadersComponent {
  // onButtonGroupClick($event: { target: any; srcElement: any }) {
  //   let clickedElement = $event.target || $event.srcElement;
  //   console.log(clickedElement)
  //   if (clickedElement.nodeName === 'BUTTON') {
  //     let isCertainButtonAlreadyActive =
  //       clickedElement.parentElement.querySelector('.active');
  //     // if a Button already has Class: .active
  //     if (isCertainButtonAlreadyActive) {
  //       isCertainButtonAlreadyActive.classList.remove('active');
  //     }

  //     clickedElement.className += ' active';
  //   }
  // }
 
  data_translate: any = {
    home: { vi: 'Trang chủ', us: 'Home' },
    training: { vi: 'Đào tạo', us: 'Training' },
    research: { vi: 'Dịch vụ doanh nghiệp', us: 'Business Services' },
    product: { vi: 'Việc làm kế toán', us: 'Accounting Jobs' },
    partner: { vi: 'Kiến thức', us: 'Knowledge' },
    media: { vi: 'Truyền thông', us: 'Media'},
    contact: { vi: 'Liên hệ', us: 'Contact' },
  };

  language_structure: any = {
    us: { name: 'US', img: 'americanflag.jpg' },
    vi: { name: 'VN', img: 'vietnamflag.png' },
  };

  language_name: string = 'vi';
  user: any;

  constructor(
    private languageService: LanguageChangeSharingService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.languageService.getLanguageObservable().subscribe((res) => {
      this.language_name = res;
      console.log(res);
    });
  }

  ngOnInit(): void {
    this.authenticationService.getUser().subscribe((user: any) => {
      this.user = user
    })
  }

  ChangeLanguage(language:any) {
    this.languageService.Language = language;
  }

  loginToPage(){
    this.router.navigateByUrl('/login');
  }
  logOut(){
    this.authenticationService.logout();
  }
}
