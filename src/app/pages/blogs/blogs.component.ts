import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss'],
})
export class BlogsComponent {
  slideConfig = { 
    slidesToShow: 3, 
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ] 
  };
  data_translate: any = {
    intro: {
      title: {
        vi: '',
        us: '',
      },
      content: {
        vi: '',
        us: '',
      },
    },
    parter: [
      '13507069_573207212861582_4197986070493007577_n.png',
      '13627169_581367038712266_426686489375370770_n.png',
      '13903211_809088379226388_1880663345899659412_n.png',
      '13507069_573207212861582_4197986070493007577_n.png',
      '13627169_581367038712266_426686489375370770_n.png',
      '13903211_809088379226388_1880663345899659412_n.png',
      '13507069_573207212861582_4197986070493007577_n.png',
      '13627169_581367038712266_426686489375370770_n.png',
    ],
    content: [
      {
        title: {
          vi: 'Một số hình ảnh đào tạo kế toán tại VINA',
          us: 'Some pictures of accounting training at VINA',
        },
        title_2: {
          vi: 'Một số hình ảnh về kế toán ',
          us: 'Some pictures about accounting',
        },
      },
    ],
  };
  public slide = '0%';
  public index = 1;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
    setInterval(() => {
      if (this.index < this.data_translate.parter.length / 4) {
        this.slide = this.index * 100 + '%';
        this.index++;
      }else{
        this.index = 0;
        this.slide = this.index * 100 + '%';
      }
    }, 5000);
  }
}
