import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmorePrepareFinancialStatementsComponent } from './viewmore-prepare-financial-statements.component';

describe('ViewmorePrepareFinancialStatementsComponent', () => {
  let component: ViewmorePrepareFinancialStatementsComponent;
  let fixture: ComponentFixture<ViewmorePrepareFinancialStatementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmorePrepareFinancialStatementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmorePrepareFinancialStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
