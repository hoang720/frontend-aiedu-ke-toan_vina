import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { RegisterComponent } from '../viewmore-content/register/register.component';


@Component({
  selector: 'app-viewmore-reading-comprehension-and-analyzing',
  templateUrl: './viewmore-reading-comprehension-and-analyzing.component.html',
  styleUrls: ['./viewmore-reading-comprehension-and-analyzing.component.scss']
})
export class ViewmoreReadingComprehensionAndAnalyzingComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1, 
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  data_translate: any ={
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
    content_1: [
      {
        title_1: {
          vi: 'Đọc hiểu và phân tích báo cáo tài chính',
          us: 'Reading comprehension and analyzing financial statements',
        },
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
        image_1: '240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Kế toán trưởng`,
          us: `Chief accountant`,
        },
        sub_main_content_1_2:{
          vi: `Kế toán tổng hợp`,
          us: `General accounting`,
        },
        sub_main_content_1_3:{
          vi: `CEO/ Giám đốc`,
          us: `CEO/ Director`,
        },
        sub_main_content_1_4: {
          vi: `Và các đối tượng khác có quan tâm`,
          us: `And other subjects of interest`
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Mục đích khóa học là giúp cho người học có kiến thức tốt trong việc đọc hiểu - kiểm tra và phân tích báo cáo tài chính của doanh nghiệp mà mình quan tâm`,
          us: `The purpose of the course is to help learners have good knowledge in reading, examining and analyzing the financial statements of the businesses they are interested in.`,
        },
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Tự mình đọc hiểu, kiểm tra và phân tích BCTC của công ty mình `,
          us: `Read, understand, check and analyze the company's financial statements by yourself`,
        },
        sub_main_content_3_2: {
          vi: `Ra các quyết định đầu tư, kinh doanh cho doanh nghiệp hoặc mục đích đầu tư chứng khoán hoặc cho vay đối với ngân hàng…`,
          us: `Making investment and business decisions for businesses or for the purpose of investing in securities or lending to banks...`,
        },
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Hệ thống báo cáo tài chính`,
          us: `Financial reporting system`,
        },
        sub_main_content_4_2: {
          vi: `Đọc hiểu các báo cáo trong hệ thống báo cáo tài chính`,
          us: `Read and understand the reports in the financial reporting system`,
        },
        sub_main_content_4_3: {
          vi: `Phân tích các chỉ số tài chính, sức khỏe của doanh nghiệp`,
          us: `Analysis of financial indicators, health of the enterprises`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 3 buổi',
          us: 'Length of study: 3 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 2.000.000 đồng',
          us: 'Tuition: 2,000,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit(): void {
  }

}
