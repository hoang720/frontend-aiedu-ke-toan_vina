import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreReadingComprehensionAndAnalyzingComponent } from './viewmore-reading-comprehension-and-analyzing.component';

describe('ViewmoreReadingComprehensionAndAnalyzingComponent', () => {
  let component: ViewmoreReadingComprehensionAndAnalyzingComponent;
  let fixture: ComponentFixture<ViewmoreReadingComprehensionAndAnalyzingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreReadingComprehensionAndAnalyzingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreReadingComprehensionAndAnalyzingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
