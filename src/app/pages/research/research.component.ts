import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss'],
})
export class ResearchComponent {
 
  ngOnInit(): void {
    
  }

  data_translate: any = {
    field_list: [
      {
        content: {
          title: {
            vi: 'Dịch vụ Doanh Nghiệp',
            us: 'Business Services',
          },
          content: {
            content_1:{
              vi: 'Thành lập doanh nghiệp',
              us: 'Establish a business',
              link: '/thanh-lap-doanh-nghiep/'
            },
            content_2:{
              vi: 'Mở mã số thuế doanh nghiệp',
              us: 'Open corporate tax code',
              link: '/mo-ma-so-thue-doanh-nghiep/'
            },
            content_3:{
              vi: 'Thay đổi đăng ký doanh nghiệp',
              us: 'Change of business registration',
              link: '/thay-doi-dang-ky-doanh-nghiep/'
            },
            content_4:{
              vi: 'Chốt thuế chuyển quận',
              us: 'Closing the district transfer tax',
              link: '/chot-thue-chuyen-quan/'
            },
            content_5:{
              vi: 'Giải thể, tạm ngừng hoạt động',
              us: 'Disbanded, temporarily stopped working',
              link: '/giai-the-tam-ngung-hoat-dong/'
            },
            content_6: {},
            content_7: {},
            content_8: {},
          },
          img: 'business-people-partnership-support-team-urban-scene-concept 1.png',
        },
      },
      {
        content: {
          title: {
            vi: 'Dịch vụ kế toán',
            us: 'Accounting services',
          },
          content: {
            content_1:{
              vi: 'Dịch vụ kế toán thuế trọn gói ',
              us: 'Full tax accounting service',
              link: '/dich-vu-ke-toan-thue-tron-goi/'
            },
            content_2:{
              vi: 'Làm báo cáo tài chính ',
              us: 'Making the financial report',
              link: '/lam-bao-cao-tai-chinh/'
            },
            content_3:{
              vi: 'Quyết toán thuế',
              us: 'Tax settlement',
              link: '/quyet-toan-thue/'
            },
            content_4:{
              vi: 'Hoàn thuế',
              us: 'Tax refund',
              link: '/hoan-thue/'
            },
            content_5:{
              vi: 'Dịch vụ soát xét sổ sách kế toán',
              us: 'Service of reviewing accounting books',
              link: '/dich-vu-soat-xet-so-sach-ke-toan/'
            },
            content_6: {
              vi: 'Dịch vụ bảo hiểm',
              us: 'Insurance services',
              link: '/dich-vu-bao-hiem/'
            },
            content_7: {
              vi: 'Dịch vụ kế toán nội bộ',
              us: 'Internal accounting service',
              link: '/dich-vu-ke-toan-noi-bo/'
            },
            content_8: {
              vi: 'Đào tạo nhân sự kế toán tại đơn vị',
              us: 'Training accounting personnel at the unit',
              link: '/dao-tao-nhan-su-ke-toan-tai-don-vi/'
            },
          },
          img: '240_F_142184373_ZQ6mRFrEo9SOJOSlq8gl62YEYVZeM24l 1.png',
        },
      },
      {
        content: {
          title: {
            vi: 'Nhân sự kế toán',
            us: 'Accounting personnel',
          },
          content: {
            content_1:{
              vi: 'Cung cấp nhân lực kế toán',
              us: 'Supply accounting human resources',
            },
            content_2:{
              vi: 'Cho thuê kế toán trưởng',
              us: 'Hire a chief accountant',
            },
            content_3: {},
            content_4: {},
            content_5: {},
            content_6: {},
            content_7: {},
            content_8: {},
          },
          img: '240_F_437716738_40cqvfIIUCD0xzrVBlPRWCLD3ExFkyaf 1.png',
        },
      },
      {
        content: {
          title: {
            vi: 'Dịch vụ tài chính',
            us: 'Financial services',
          },
          content: {
            content_1:{
              vi: 'Dịch vụ kiểm soát nội bộ',
              us: 'Internal control service',
            },
            content_2:{
              vi: 'Tư vấn tài chính doanh nghiệp',
              us: 'Corporate financial consulting',
            },
            content_3: {},
            content_4: {},
            content_5: {},
            content_6: {},
            content_7: {},
            content_8: {},
          },
          img: 'banking-saving-money-copyspace-background-idea-concept 1.png',
        },
      },
      {
        content: {
          title: {
            vi: 'Dịch vụ khác',
            us: 'Other services',
          },
          content: {
            content_1:{
              vi: 'Xin giấy phép các ngành nghề  bắt buộc theo quy định',
              us: 'Apply for a license for the required occupations according to regulations',
            },
            content_2:{
              vi: 'Giấy chứng nhận ISO, an toàn vệ sinh, phòng cháy chữa cháy...',
              us: 'ISO certificate, hygiene and safety, fire prevention...',
            },
            content_3: {},
            content_4: {},
            content_5: {},
            content_6: {},
            content_7: {},
            content_8: {},
          },
          img: '240_F_160939969_Y0VJbhcCDlLzfNUzq3ZnQqB0kmcP4coS 1.png',
        },
      },
    ],
  };
  
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
