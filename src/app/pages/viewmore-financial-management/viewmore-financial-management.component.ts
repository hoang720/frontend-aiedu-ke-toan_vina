import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { RegisterComponent } from '../viewmore-content/register/register.component';


@Component({
  selector: 'app-viewmore-financial-management',
  templateUrl: './viewmore-financial-management.component.html',
  styleUrls: ['./viewmore-financial-management.component.scss']
})
export class ViewmoreFinancialManagementComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1, 
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  data_translate: any ={
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
    content_1: [
      {
        title_1: {
          vi: 'Quản trị tài chính doanh nghiệp',
          us: 'Enterprise Financial Management',
        },
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
        image_1: 'spreadsheet-document-information-financial-startup-concept 1.png',
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Kế toán trưởng`,
          us: `Chief accountant`,
        },
        sub_main_content_1_2:{
          vi: `Kế toán tổng hợp`,
          us: `General accounting`,
        },
        sub_main_content_1_3:{
          vi: `CEO/ Giám đốc`,
          us: `CEO/ Director`,
        },
        sub_main_content_1_4: {
          vi: `Và các đối tượng khác có quan tâm`,
          us: `And other subjects of interest`
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Giúp người học trang bị kiến thức về tài chính doanh nghiệp. Quản trị các dòng tiền thu vào, chi ra của doanh nghiệp từ đó tối đa hóa lợi nhuận và đưa ra các quyết định đúng đắn kịp thời`,
          us: `Help learners equip knowledge about corporate finance. Managing the cash inflows and outflows of the business, thereby maximizing profits and making the right decisions in a timely manner.`,
        },
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Sau khóa học người học có kiến thức tốt hơn về tài chính. Biết cách phân tích các chỉ số của doanh nghiệp`,
          us: `After the course learners have better knowledge of finance. Know how to analyze business metrics`,
        },
        sub_main_content_3_2: {
          vi: `Biết cách lập kế hoạch và lựa chọn dự án đạt hiệu quả và đưa ra quyết định kinh doanh sáng suốt`,
          us: `Know how to effectively plan and select projects and make informed business decisions`,
        },
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Đọc hiểu báo cáo tài chính của doanh nghiệp`,
          us: `Read and understand a company's financial statements`,
        },
        sub_main_content_4_2: {
          vi: `Thuế và các vấn đề thường hay sai phạm và cách khắc phục`,
          us: `Taxes and common problems and how to fix them`,
        },
        sub_main_content_4_3: {
          vi: `Dòng tiền vào và quản trị dòng vào của doanh nghiệp`,
          us: `Cash inflow and inflow management of the business`,
        },
        sub_main_content_4_4: {
          vi: `Dòng tiền ra và quản trị dòng ra của doanh nghiệp`,
          us: `Cash outflow and outflow management of the business`,
        },
        sub_main_content_4_5: {
          vi: `Lập kế hoạch cho dự án kinh doanh và lựa chọn dự án kinh doanh tốt.`,
          us: `Make a business project plan and choose a good business project.`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 5 buổi',
          us: 'Length of study: 5 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 10.000.000 đồng',
          us: 'Tuition: 10,000,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit(): void {
  }
}
