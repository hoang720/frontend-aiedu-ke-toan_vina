import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreFinancialManagementComponent } from './viewmore-financial-management.component';

describe('ViewmoreFinancialManagementComponent', () => {
  let component: ViewmoreFinancialManagementComponent;
  let fixture: ComponentFixture<ViewmoreFinancialManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreFinancialManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreFinancialManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
