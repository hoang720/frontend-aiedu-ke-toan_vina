import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThueTncnComponent } from './thue-tncn.component';

describe('ThueTncnComponent', () => {
  let component: ThueTncnComponent;
  let fixture: ComponentFixture<ThueTncnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThueTncnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThueTncnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
