import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThueTndnComponent } from './thue-tndn.component';

describe('ThueTndnComponent', () => {
  let component: ThueTndnComponent;
  let fixture: ComponentFixture<ThueTndnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThueTndnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThueTndnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
