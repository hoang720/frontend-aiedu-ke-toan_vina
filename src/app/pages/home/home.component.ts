import { Component, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { AuthenticationService } from '@core/services/authentication.service';
import { NewsManagementService } from '@core/services/news-management.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  slideConfig = { 
    slidesToShow: 3, 
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  slideConfig1 = { 
    slidesToShow: 5, 
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  
  slideConfig2 = { 
    slidesToShow: 4, 
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ] 
  };
  slideConfig3 ={
    slidesToShow: 1, 
    slidesToScroll: 1,
  }
  data_title: any = [
    {
      title_1: {
        vi: 'DỊCH VỤ CUNG CẤP NỔI BẬT',
        us: 'OUTSTANDING SERVICES PROVIDED',
      },
      title_2: {
        vi: 'ĐỐI TÁC NÓI GÌ VỀ CHÚNG TÔI',
        us: 'WHAT OUR PARTNER SAY ABOUT US',
      },
      title_3: {
        vi: 'TIN TỨC MỚI NHẤT',
        us: 'LATEST NEWS',
      },
      title_4: {
        vi: 'MỘT SỐ ĐỐI TÁC',
        us: 'SOME PARTNERSHIP',
      },
    }
  ]
  data_letter: any = [
    {
      let_open: {
        vi: 'Thư ngỏ,',
        us: 'Open letter,'
      },
      main_content: {
        vi: `
      Công ty TNHH Tư vấn Giải pháp Kế toán Việt Nam với tên giao dịch là Kế Toán Việt Nam được thành lập ngày 10 tháng 10 năm 2014. Kế toán Việt Nam là một trong những đơn vị hàng đầu chuyên đào tạo và cung cấp các dịch vụ về kế toán - tư vấn tài chính - tư vấn thuế. Chúng tôi tự tin có đầy đủ nguồn lực và khả năng để đáp ứng tất cả nhu cầu của khách hàng với chi phí hợp lý, giá cả cạnh tranh, chất lượng đảm bảo.
      Kế toán Việt Nam luôn lấy sự hài lòng của khách hàng làm thước đo cho thành công của mình. Chúng tôi cam kết chia sẻ những cơ hội, hiểu biết, kinh nghiệm… nhằm mang đến những giải pháp tối ưu giúp khách hàng tiết kiệm chi phí, tối đa hóa lợi nhuận.
      Với đội ngũ chuyên gia kế toán, kiểm toán nhiều năm kinh nghiệm đã kịp thời tháo gỡ khó khăn cho hàng nghìn khách hàng, chúng tôi tin tưởng rằng
      Kế toán Việt Nam sẽ là một đối tác tin cậy của tất cả các doanh nghiệp để hợp tác lâu dài và thành công trong tương lai.
      Chúng tôi, Công ty TNHH Tư vấn Giải pháp Kế toán Việt Nam mong muốn có cơ hội tham gia đồng hành và là một đối tác tin cậy của Quý vị.
      Trân trọng cảm ơn và mong muốn hợp tác lâu dài cùng Quý khách!`,
        us: `
      Vietnam Accounting Solution Consulting Co., Ltd with the transaction name is Accounting Vietnam was established on October 10, 2014. Accounting Vietnam is one of the leading units specializing in training and providing services. accounting services - financial consulting - tax consulting. We are confident that we have sufficient resources and capabilities to meet all of our customers' needs with reasonable costs, competitive prices, and guaranteed quality.
      Vietnamese accountants always take customer satisfaction as a measure of their success. We are committed to sharing opportunities, knowledge, experience ... to bring optimal solutions to help customers save costs, maximize profits.
      With a team of experienced accounting and auditing experts who have promptly removed difficulties for thousands of customers, we believe that Vietnam Accountants will be a reliable partner of all businesses to long-term and successful cooperation in the future.
      We, Vietnam Accounting Solution Consulting Co., Ltd would like to have the opportunity to join and be a reliable partner of you.
      Sincerely thank you and look forward to long-term cooperation with you!`
      },
      sincerely: {
        vi: 'Kính thư',
        us: 'Sincerely',
      },
      manager: {
        vi: 'GIÁM ĐỐC',
        us: 'MANAGER',
      },
      name: {
        vi: 'Phạm Thị Tuyết',
        us: 'Pham Thi Tuyet',
      }
    }
  ]
  data_translate: any = [
    {
      img: 'icon-02.png',
      text: {
        vi: 'TẦM NHÌN',
        us: 'VISION',
      },
      hover_img:'Group 286.png'
    },
    {
      img: 'AdobeStock_441704463-01.png',
      text: {
        vi: 'SỨ MỆNH',
        us: 'MISSION'
      },
      hover_img:'Group 287.png'
    },
    {
      img: 'icon-01.png',
      text: {
        vi: 'GIÁ TRỊ CỐT LÕI',
        us: 'CORE VALUES'
      },
      hover_img:'Group 288.png'
    },
  ];
  data_slide3: any = [
    {
      img: '../../../assets/images/home/phamquangtruong (1).png',
      
      mr: {
        vi: 'Ông Tạ Văn Lưu',
        us: 'Mr. Ta Van Luu',
      },
      job: {
        vi: 'Chủ tich hội đồng quản trị công ty Cổ Phần tập đoàn ASA group.',
        us: 'Chairman of the Board of Directors of ASA Group Joint Stock Company'
      },
      main_content: {
        vi: `“Tôi đã sử dụng dịch vụ kế toán soát xét và dịch vụ trọn gói tại Công ty Kế toán Việt Nam. Tôi thấy cách làm việc của công ty khá chuyên nghiệp, nhanh chóng. Công ty Kế toán Việt Nam đã giúp tôi đáng kể trong công viêc kế toán, tôi không còn lo nhân viên kế toán nghỉ việc như trước kia nữa.”`,
        us: `“I have used the full accounting service at Vietnam Accounting Company and am completely assured of the quality here. The staff is enthusiastic and professional, advising customers very carefully. I would gladly refer my friends to the company.”`
      },
    },
    {
      img: '../../../assets/images/home/12.jpg',
      mr: {
        vi: 'Ông Bùi Viết Phong',
        us: 'Mr. Bui Viet Phong',
      },
      job: {
        vi: 'Giám đốc Công ty TNHH TM XNK Hapaco Hà Nội',
        us: 'Director of Hapaco Hanoi Import-Export Trading Co., Ltd'
      },
      main_content: {
        vi: `“Tôi đã sử dụng dịch vụ kế toán trọn gói tại công ty Kế toán Việt Nam và hoàn toàn yên tâm về chất lượng tại đây. Nhân viên làm việc nhiệt tình và chuyên nghiệp, tư vấn cho khách hàng rất cẩn thận. Tôi sẵn sàng giới thiệu bạn bè của tôi cho công ty.”`,
        us: `“I have used the full accounting service at Vietnam Accounting Company and am completely assured of the quality here. The staff is enthusiastic and professional, advising customers very carefully. I would gladly refer my friends to the company.”`
      },
    },
    
  ];

  data_slide: any = [
    {
      img: '../../../assets/images/home/business-office-scene.png',
      text:'Khóa học kế toán tổng hợp',
      link: '/slide-1/',
      content: 'Tham gia khóa học này bạn sẽ được học một cách có hệ thống từ nguyên lý kế toán, kế toán thuế đến lên báo cáo tài chính và kiểm tra báo cáo tài chính. Học viên lựa chọn loại hình tham gia như thương mại, xây dựng, sản xuất…'
    },
    {
      img: '../../../assets/images/home/calculator-magnifying-glass-table.png',
      text:'Khóa học theo yêu cầu',
      content: 'Tham gia khóa học này bạn sẽ được hướng dẫn theo chính doanh nghiệp của mình và sử dụng hóa đơn chứng từ của doanh nghiệp mình để lên báo cáo tài chính. Kết thúc khóa học, người học được hỗ trợ đồng hành làm việc đến bao giờ lên được báo cáo tài chính thì thôi. '
    },
    {
      img: '../../../assets/images/home/240_F_174178810_ineaew8N5ScFovoaCmA4NNAQwAPx88Wg 1.png',
      text:'Dịch vụ kế toán trọn gói',
      content: 'Khi sử dụng dịch vụ này khách hàng sẽ được tư vấn tổng thể đảm bảo tối ưu thuế, kê khai và làm báo cáo tài chính định kỳ. Chúng tôi sẽ thay mặt khách hàng tham gia giải trình số liệu khi có yêu cầu. Chúng tôi hoàn toàn chịu trách nhiệm với những gì đã làm, cam kết nộp phạt toàn bộ nếu làm sai.'
    },
    {
      img: '../../../assets/images/home/business-report-graphs-charts-business-reports-pile-documents-business-concept.png',
      text:'Dịch vụ làm báo cáo tài chính',
      content: 'Công việc làm báo cáo tài chính đó là kiểm tra tờ khai thuế đã nộp,sau đó điều chỉnh nếu có sai sót, nhập liệu để lên báo cáo tài chính . Tư vấn tổng thể các vấn đề rủi ro gặp phải, sau khi kết thúc chúng tôi sẽ in toàn bộ sổ sách giấy tờ bàn giao cùng với file mềm những gì đã thực hiện.'
    },
  ];

  data_service2: any = [
    {
      img: '../../../assets/images/home/business-background-design.png',
      text:'Dịch vụ quyết toán thuế',
      link: '/slide-1/',
      content: 'Tham gia khóa học này bạn sẽ được học một cách có hệ thống từ nguyên lý kế toán, kế toán thuế đến lên báo cáo tài chính và kiểm tra báo cáo tài chính. Học viên lựa chọn loại hình tham gia như thương mại, xây dựng, sản xuất…'
    },
    {
      img: '../../../assets/images/home/business-communication-connection-people-concept.png',
      text:'Dịch vụ quyết toán thuế',
      content: 'Tham gia khóa học này bạn sẽ được hướng dẫn theo chính doanh nghiệp của mình và sử dụng hóa đơn chứng từ của doanh nghiệp mình để lên báo cáo tài chính. Kết thúc khóa học, người học được hỗ trợ đồng hành làm việc đến bao giờ lên được báo cáo tài chính thì thôi. '
    },
    {
      img: '../../../assets/images/home/day-city-view.png',
      text:'Dịch vụ thành lập doanh nghiệp',
      content: 'Khi sử dụng dịch vụ này khách hàng sẽ được tư vấn tổng thể đảm bảo tối ưu thuế, kê khai và làm báo cáo tài chính định kỳ. Chúng tôi sẽ thay mặt khách hàng tham gia giải trình số liệu khi có yêu cầu. Chúng tôi hoàn toàn chịu trách nhiệm với những gì đã làm, cam kết nộp phạt toàn bộ nếu làm sai.'
    },
  ];

  data_partner: any = [
    {
      img: 'ktt5 1.png',
    },
    {
      img: 'huong-coi.png',
    },
    {
      img: 'hoang-mien.png',
    },
    {
      img: 'trinh-trang.png',
    },
    {
      img: 'charly-an.png',
    },
    {
      img: 'hoang-tuoi.png',
    },
    {
      img: 'thao-green.png',
    },
  ];

  data_parner2: any = [
    {
      img: '../../../assets/images/home/phamquangtruong (1).png',
      title:'Ông Phạm Quang Trưởng',
      subtitle: 'Giám đốc công ty Cổ Phần tập đoàn ASA group.',
      content: `“Tôi đã sử dụng dịch vụ kế toán soát xét và dịch vụ trọn gói tại Công ty Kế toán Việt Nam. Tôi thấy cách làm việc của công ty khá chuyên nghiệp, nhanh chóng. Công ty Kế toán Việt Nam đã giúp tôi đáng kể trong công viêc kế toán, tôi không còn lo nhân viên kế toán nghỉ việc như trước kia nữa.”`,
    },
    {
      img: '../../../assets/images/home/12.jpg',
      title:'Ông Bùi Viết Phong',
      subtitle: 'Giám đốc công ty TNHH TM XNK Hapaco Hà Nội.',
      content: `“Tôi đã sử dụng dịch vụ kế toán trọn gói tại công ty Kế toán Việt Nam và hoàn toàn yên tâm về chất lượng tại đây. Nhân viên làm việc nhiệt tình và chuyên nghiệp, tư vấn cho khách hàng rất cẩn thận. Tôi sẵn sàng giới thiệu bạn bè của tôi cho công ty.”`,
    },
    {
      img: '../../../assets/images/home/12.jpg',
      title:'Đặng Thị Hương',
      subtitle: '20 tuổi',
      content: `“Chỉ mới học hết cấp 3 tôi đã tham gia khóa học “kế toán tổng hợp” tại công ty. Mới đầu rất bỡ ngỡ và khó khăn nhưng chị Tuyết và các anh chị trong công ty đã giúp đỡ tôi rất nhiều. Ngoài giờ học tôi đến công ty thực tập và làm trực tiếp nên tôi đã rất tự tin xin việc. Hiện nay tôi đang làm kế toán lương thử việc là 8 triệu/tháng. Tôi vô cùng biết ơn đến chị Tuyết cùng đội ngũ Công ty Kế toán Việt Nam. Xin chúc chị sức khỏe và công ty ngày càng phát triển.”`,
    },
    {
      img: '../../../assets/images/home/12.jpg',
      title:'Nguyễn Thị Huyền',
      subtitle: '30 tuổi',
      content: `“Tôi đã tham gia “khóa học theo yêu cầu – khóa học nhà hàng khách sạn” tại công ty Kế toán Việt Nam. Mặc dù trước đó tôi đã làm kế toán mảng thương mại 5 năm nhưng khi sang công ty mới tôi lại ko biết cách lên báo cáo tài chính. Tôi đã được hướng dẫn cách hạch toán kế toán, cách tập hợp chi phí, cân đối thuế và lên được báo cáo tài chính. Trong quá trình làm tại Doanh nghiệp mới tôi còn được chị Tuyết hỗ trợ nhiệt tình. Từ tận đáy long, tôi vô cùng cảm ơn đến chị Tuyết và chúc chị cùng công ty ngày càng được nhiều người biết đến. Tôi khuyên mọi người nên học tại đây để nâng cao tay nghề. Một lần nữa xin chân thành cảm ơn!.”`,
    },
    {
      img: '../../../assets/images/home/12.jpg',
      title:'Phạm Thị Yến',
      subtitle: '23 tuổi',
      content: `“Tôi biết đến Kế toán Vina thông qua một người bạn đã từng học tại đây, khi đấy tôi còn là sinh viên năm 2. Tôi đã đăng ký học lớp “kế toán tổng hơp”. Cứ có thời gian rảnh và nghỉ hè ở trường là tôi lại đến công ty để thực tập nên cũng tích lũy cho mình kha khá kinh nghiệm. Không chỉ có vậy, trình độ tin học của tôi cũng cải thiện đáng kể vì ở đây tôi được làm mọi việc từ sắp xếp hóa đơn, kê khai, nhập chứng từ và lên báo cáo tài chính. Khi ra trường tôi đã được tuyển ngay vào vị trí kế toán tổng hợp với mức lương khá ổn. Hy vọng nhiều bạn sinh viên hãy tìm cho mình một môi trường như vậy để có thể học tập và trau dồi kinh nghiệm. Xin chân thành cảm ơn chị Tuyết cùng anh chị trong phòng dịch vụ đã giúp đỡ tôi trong suốt thời gian ở đây. Chúc công ty ngày càng phát triển.”`,
    },
  ]

  data_new: any = [
    {
      img: 'title.png',
      title: {
        vi: 'Giảm thuế GTGT 2%',
        us: '2% VAT reduction',
      },
      link: '/viewmore-GTGT-2%/',
    },
    {
      img: 'title.png',
      title: {
        vi: 'Tình huống hỏi đáp liên quan kê khai thuế GTGT theo TT80',
        us: 'Question and answer situation related to PIT declaration',
      },
      link: '/viewmore-GTGT-TT80/'
    },
    {
      img: 'title.png',
      title: {
        vi: 'Tình huống hỏi đáp liên quan kê khai thuế TNCN',
        us: 'Question and answer situation related to VAT declaration according to TT80',
      },
      link: '/viewmore-TNCN/'
    },
    {
      img: 'title.png',
      title: {
        vi: 'Tình huống hỏi đáp liên quan kê khai thuế TNDN',
        us: 'Question and answer situation related to CIT declaration',
      },
      link: '/viewmore-TNDN/'
    },
    {
      img: 'title.png',
      title: {
        vi: 'Kinh nghiệm làm việc',
        us: 'Work experience',
      },
      link: '/viewmore-work-ex/'
    }
  ];
  img:any =[
    'Untitled-1-01.png',
    'Untitled-1-02.png',
    'Untitled-1-03.png',
    'Untitled-1-04.png',
    'Untitled-1-05.png',
    'Untitled-1-06.png',
    'Untitled-1-07.png',
    'Untitled-1-08.png',
  ]
  public slide = '0%';
  public index = 1;
  language_name: string = 'vi';
  user: any = null;
  constructor(
    private LanguageService: LanguageChangeSharingService,
    private authenticationService: AuthenticationService,
    private newService: NewsManagementService, 
    private router: Router,
  ) {
    //real time when user change another language
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
   /*  setInterval(() => {
      if (this.index > this.data_translate.parter.length / 3) {
        this.slide = this.index * 100 + '%';
        this.index++;
      } else {
        this.index = 0;
        this.slide = this.index * 100 + '%';
      }
    }, 0); */
  }
  news_list: Observable<any> = new Observable<any>();
  showMore = false;
  ngOnInit(): void {
    this.getUser()
    this.news_list = this.getNews()
  }
  getUser(){
    this.authenticationService.getUser().subscribe((user: any)=>{
      this.user = user;
    })
  }
  formathttps(link: any){ 
    return link.replace('http://45.77.245.61:6868/',"https://ketoanvina.com.vn/image/")
  }
  getNews(){
    return this.newService.getNew();
  }
  redirectToPage(page_name: any, news_name: any){
    this.router.navigateByUrl(`/${page_name}/${news_name}`);
  }
}
