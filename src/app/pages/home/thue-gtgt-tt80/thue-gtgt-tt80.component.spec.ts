import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThueGtgtTt80Component } from './thue-gtgt-tt80.component';

describe('ThueGtgtTt80Component', () => {
  let component: ThueGtgtTt80Component;
  let fixture: ComponentFixture<ThueGtgtTt80Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThueGtgtTt80Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThueGtgtTt80Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
