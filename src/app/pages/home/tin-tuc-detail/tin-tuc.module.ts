import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TinTucRoutingModule } from './tin-tuc-routing.module'


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TinTucRoutingModule
  ]
})
export class TinTucModule { }
