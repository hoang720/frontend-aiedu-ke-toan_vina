import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TinTucDetailComponent } from './tin-tuc-detail.component';

const routes: Routes = [
  {
    path: '', component: TinTucDetailComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TinTucRoutingModule { }
