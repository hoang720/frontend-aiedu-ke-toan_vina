import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NewsManagementService } from '@core/services/news-management.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tin-tuc-detail',
  templateUrl: './tin-tuc-detail.component.html',
  styleUrls: ['./tin-tuc-detail.component.scss']
})
export class TinTucDetailComponent implements OnInit {
  news_list: Observable<any> = new Observable<any>();
  news_name: any;
  data_global: any;
  constructor(
    private newService: NewsManagementService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.data_global = JSON.parse(localStorage.getItem('DATA_GLOBAL')!);
    this.news_name = this.route.snapshot.paramMap.get('news_name')
  }

  ngOnInit(): void {
    this.news_list = this.getNewsByName(this.news_name)
  }
  getNewsByName(name: any){
    return this.newService.getNewByName(name)
  }
  getNewsBasedID(){
    this.route.params.subscribe(params =>{
      this.news_name = params['news_name'];
    })
  }
}
