import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';


@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.scss']
})
export class WorkExperienceComponent implements OnInit {

  work_ex: any =[
    {
      content_1: {
        vi: `Theo Khoản 2, Điều 1, Nghị định 132/2020/NĐ-CP thì Giao dịch liên kết (GDLK) được định nghĩa: Giao dịch liên kết là giao dịch phát sinh giữa các bên có quan hệ liên kết trong quá trình hoạt động sản xuất, kinh doanh`,
        us: `According to Clause 2, Article 1 of Decree 132/2020/ND-CP, a related transaction (GDLK) is defined as: An associated transaction is a transaction arising between related parties in the course of operation. manufacturing business`,
      },
      content_2: {
        vi: `Một doanh nghiệp bảo lãnh hoặc cho một doanh nghiệp khác vay vốn dưới bất kỳ hình thức nào (bao gồm cả các khoản vay từ bên thứ ba được đảm bảo từ nguồn tài chính của bên liên kết và các giao dịch tài chính có bản chất tương tự) với điều kiện khoản vốn vay ít nhất bằng 25% vốn góp của chủ sở hữu của doanh nghiệp đi vay và chiếm trên 50% tổng giá trị các khoản nợ trung và dài hạn của doanh nghiệp đi vay;`,
        us: `An enterprise guarantees or lends capital to another enterprise in any form (including loans from third parties secured from the related party's financial resources and related financial transactions). similar nature) provided that the loan amount is at least 25% of the contributed capital of the owner of the borrowing enterprise and accounts for more than 50% of the total value of medium and long-term debts of the borrowing enterprise;`,
      },
      content_3: {
        vi: `Một doanh nghiệp chỉ định thành viên ban lãnh đạo điều hành hoặc nắm quyền kiểm soát của một doanh nghiệp khác với điều kiện số lượng các thành viên được doanh nghiệp thứ nhất chỉ định chiếm trên 50% tổng số thành viên ban lãnh đạo điều hành hoặc nắm quyền kiểm soát của doanh nghiệp thứ hai; hoặc một thành viên được doanh nghiệp thứ nhất chỉ định có quyền quyết định các chính sách tài chính hoặc hoạt động kinh doanh của doanh nghiệp thứ hai;`,
        us: `An enterprise appoints a member of the management board to operate or take control of another enterprise provided that the number of members appointed by the first enterprise accounts for more than 50% of the total number of members of the management board. operate or take control of a second enterprise; or a member appointed by the first enterprise has the power to decide the financial policies or business activities of the second enterprise;`,
      },
      content_4: {
        vi: `Hai doanh nghiệp được điều hành hoặc chịu sự kiểm soát về nhân sự, tài chính và hoạt động kinh doanh bởi các cá nhân thuộc một trong các mối quan hệ vợ, chồng; cha mẹ đẻ, cha mẹ nuôi, cha dượng, mẹ kế, cha mẹ vợ, cha mẹ chồng; con đẻ, con nuôi, con riêng của vợ hoặc chồng, con dâu, con rể; anh, chị, em cùng cha mẹ, anh, chị em cùng cha khác mẹ, anh, chị, em cùng mẹ khác cha, anh rể, em rể, chị dâu, em dâu của người cùng cha mẹ hoặc cùng cha khác mẹ, cùng mẹ khác cha; ông bà nội, ông bà ngoại; cháu nội, cháu ngoại; cô, dì, chú, cậu, bác ruột và cháu ruột;`,
        us: `Two enterprises are operated or controlled in terms of human resources, finance and business activities by individuals in one of the husband and wife relationships; natural parents, adoptive parents, stepfather, stepmother, parents-in-law, parents-in-law; natural, adopted, step-child of a spouse, daughter-in-law, son-in-law; brother, sister, half-brother, half-brother, half-brother, half-brother, brother-in-law, brother-in-law, sister-in-law, sister-in-law of the person of the same parent or half-brother, with a different mother; paternal grandparents, maternal grandparents; grandson, grandchild; aunt, uncle, uncle, aunt and nephew;`
      },
      content_5: {
        vi: `Doanh nghiệp có phát sinh các giao dịch nhượng, nhận chuyển nhượng vốn góp ít nhất 25% vốn góp của chủ sở hữu của doanh nghiệp trong kỳ tính thuế; vay, cho vay ít nhất 10% vốn góp của chủ sở hữu tại thời điểm phát sinh giao dịch trong kỳ tính thuế với cá nhân điều hành, kiểm soát doanh nghiệp hoặc với cá nhân thuộc trong một các mối quan hệ theo quy định tại điểm g khoản này.`,
        us: `The enterprise has transactions of transferring or receiving at least 25% of the contributed capital of the enterprise's owner in the tax period; Borrowing or lending at least 10% of the owner's contributed capital at the time of a transaction in the tax period with an individual who runs or controls the business or with an individual in a relationship as prescribed in Clause 1 of this Article. point g of this clause.`
      },
      content_6: {
        vi: `Công ty Kế toán Việt Nam có Giao dịch liên kết trong năm có một số thông tin liên quan đến BCTC như sau : lợi nhuận thuần : 1.200 triệu; cp khấu hao : 200 tr; lãi vay 100 triệu; lãi tiền gửi và tiền cho vay thu được 100 tr`,
        us: `The Vietnam Accounting Company with associated transactions during the year has some information related to the financial statements as follows: net profit: 1,200 million; Depreciation shares: 200 million; loan interest of 100 million; interest on deposits and loans earned 100 million VND`
      },
      content_7: {
        vi: `Công ty Kế toán Việt Nam có Giao dịch liên kết trong năm có một số thông tin liên quan đến BCTC như sau: lợi nhuận thuần : -300 triệu; cp khấu hao : 200 tr; lãi vay 150 triệu; lãi tiền gửi và tiền cho vay thu được 100 tr`,
        us: `The Vietnam Accounting Company that had a related transaction during the year has some information related to the financial statements as follows: net profit: -300 million; Depreciation shares: 200 million; loan interest 150 million; interest on deposits and loans earned 100 million VND`
      },
      content_8: {
        vi: `Công ty Kế toán Việt Nam có Giao dịch liên kết trong năm có một số thông tin liên quan đến BCTC như sau: lợi nhuận thuần : 1.200 triệu; cp khấu hao : 200 tr; lãi vay 800 triệu; lãi tiền gửi và tiền cho vay thu được 100 tr`,
        us: `The Vietnam Accounting Company with associated transactions during the year has some information related to the financial statements as follows: net profit: 1,200 million; Depreciation shares: 200 million; loan interest 800 million; interest on deposits and loans earned 100 million VND`
      }
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
