import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-thue-gtgt',
  templateUrl: './thue-gtgt.component.html',
  styleUrls: ['./thue-gtgt.component.scss']
})
export class ThueGtgtComponent implements OnInit {

  ThueGTGT: any =[
    {
      content_1: {
        vi: `Từ ngày 1/2/2022, Nghị định số 15/2022/NĐ-CP của Chính phủ về giảm thuế giá trị gia tăng từ 10% xuống còn 8% chính thức có hiệu lực. Tuy nhiên, không phải tất cả các hàng hóa, dịch vụ đều được giảm thuế. Vì vậy ngay từ ngày đầu thực hiện chính sách mới, nhiều doanh nghiệp đang phải loay hoay trong việc xác định mã sản phẩm để áp dụng giảm thuế. Bài viết này sẽ hướng dẫn cách xác định hàng hoá được giảm thuế VAT một cách nhanh nhất:`,
        us: `From February 1, 2022, the Government's Decree No. 15/2022/ND-CP on reducing value-added tax from 10% to 8% officially took effect. However, not all goods and services are eligible for tax reduction. Therefore, right from the first day of implementing the new policy, many businesses are struggling to determine the product code to apply for tax reduction. This article will guide you on how to determine the fastest way to determine VAT-reduced goods:`,
      },
      content_2: {
        vi: `Bước 1. Kiểm tra mã ngành kinh doanh thực tế của đơn vị mình, tra ở trong giấy phép đăng ký kinh doanh trước hết có nằm trong nhóm mặt hàng không được giảm thuế từ mục (1)-(3) phần nguyên tắc trên không?. Nếu không nằm trong đó thì được giảm. Nếu nằm trong đó thì tiếp tục bước 2`,
        us: `Step 1. Check the actual business code of your unit, check in the business registration certificate, first of all, it is in the group of items not eligible for tax reduction from item (1)-(3) of the above principles. are not?. If it is not included, it will be reduced. If it is in it, continue to step 2`,
      
      },
      content_3: {
        vi: `Doanh nghiệp xuất hoá đơn trực tiếp khi xuất hoá đơn, tại cột”thành tiền” ghi đầy đủ tiền hàng hoá, dịch vụ trước khi giảm. Tại dòng “cộng tiền hàng hoá dịch vụ ” ghi theo số đã giảm 20% mức tỷ lệ % trên doanh thu, đồng thời ghi chú: “ đã giảm…(số tiền) tương ứng 20% mức tỷ lệ % để tính thuế GTGT theo nghị quyết số 43/2022/QH15”`,
        us: `Enterprises issue invoices directly when issuing invoices, in the column “to money” write the full amount of goods and services before reducing. In the line "plus money for goods and services" write the number that has reduced 20% of the percentage of revenue, and at the same time note: "reduced...(amount) corresponds to 20% of the percentage rate for VAT calculation. according to Resolution No. 43/2022/QH15”`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
