import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThueGtgtComponent } from './thue-gtgt.component';

describe('ThueGtgtComponent', () => {
  let component: ThueGtgtComponent;
  let fixture: ComponentFixture<ThueGtgtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThueGtgtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThueGtgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
