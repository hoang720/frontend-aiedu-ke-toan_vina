import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';


@Component({
  selector: 'app-slide1',
  templateUrl: './slide1.component.html',
  styleUrls: ['./slide1.component.scss']
})
export class Slide1Component implements OnInit {

  slide_1: any =[
    {
      content_1: {
        vi: `Đó là nhận định chung của hầu hết các bạn sau khi trải qua khóa học kế toán thực hành tại Kế toán Vina. 100% các bạn sau khi học xong khóa học này tại đây đã xin được việc. Lý do vì sao?`,
        us: `That is the common opinion of most of you after going through the practical accounting course at Vina Accountants. 100% of you after completing this course here have found a job. The reason why?`,
      },
      content_3: {
        vi: `Giai đoạn 1 : HỌC ĐỂ BIẾT LÀM . Thời gian 1-2 tháng, trong thời gian này, bạn sẽ được học một cách bài bản tất cả các vấn đề để trở thành một kế toán tổng hợp. Từ việc sắp xếp hóa đơn chứng từ cho đến việc cân đối doanh thu chi phí, kê khai thuế, lên báo cáo tài chính và hoàn thiện chứng từ, đọc hiểu - kiểm tra báo cáo tài chính. Tất cả đều học trên phần mềm HTKK của Tổng cục thuế, phần mềm kế toán, excel.`,
        us: `Phase 1: LEARN TO DO. During 1-2 months, during this time, you will learn methodically all the issues to become a general accountant. From arranging invoices and documents to balancing revenue and expenses, declaring taxes, preparing financial statements and completing documents, reading and checking financial statements. All of them learn on HTKK software of the General Department of Taxation, accounting software, excel.`,
      
      },
      content_4: {
        vi: `Giai đoạn 2 : THỰC HÀNH TRÊN CHỨNG TỪ THẬT 100%. Sau khi học xong giai đoạn 1, bạn sẽ trải qua 1 bài test để được vào học việc trong phòng dịch vụ. Tại đây, bạn sẽ làm việc trên các chứng từ thật 100% , làm các công việc giao dịch với cơ quan thuế, bảo hiểm. Thời gian từ 1-3 tháng.`,
        us: `Phase 2: PRACTICE ON 100% TRUE EVIDENCE. After completing phase 1, you will pass a test to get into the service department as an apprentice. Here, you will work on 100% real documents, do transactions with tax and insurance agencies. Duration 1-3 months.`,
      },
      content_5: {
        vi: `=> Trong quá trình học việc, các bạn có thể tham gia ứng tuyển ngay khi có thông tin tuyển dụng của nhà tuyển dụng (do các bạn tìm kiếm hoặc do Kế toán Vina tìm giúp) và được sự hỗ trợ từ phía công ty, về việc hoàn thiện hồ sơ, phỏng vấn cũng như nghiệp vụ.`,
        us: `=> During the apprenticeship, you can apply for jobs as soon as there is recruitment information from the employer (searched by you or by Vina Accountants) and supported by the company. , about completing the profile, interview as well as professional.`,
      },
      content_6: {
        vi: `=> Đặc biệt hơn nữa, khi đã đi làm tại các DN rồi, có bất kỳ vướng mắc, Kế toán Vina vẫn luôn đồng hành và giúp đỡ bạn ( tất nhiên là hoàn toàn free - trong khi những dịch vụ này, công ty vẫn thu phí đối với người không học tại trung tâm). `,
        us: `=> More specifically, when you have worked at enterprises, have any problems, Vina Accountants will always accompany and help you (of course completely free - while these services, the company fees are still charged for those who do not study at the center).`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
