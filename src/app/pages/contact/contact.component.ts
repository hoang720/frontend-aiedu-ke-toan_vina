import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent {
  translate_data:any = {
    contact_now: {
      vi: 'Liên hệ ngay',
      us: 'Contact now',
    },
    name: {
      vi: 'Tên của bạn',
      us: 'Your name',
    },
    phone_number: {
      vi: 'Số điện thoại',
      us: 'Phone number',
    },
    message: {
      vi: 'Lời nhắn',
      us: 'Message',
    },
    send: {
      vi: 'Gửi tin nhắn',
      us: 'Send message',
    },
    address: {
      vi: `
      Cơ sở Hà Nội: P601 Tòa nhà Sunrise Building số 90 Trần Thái Tông, Quận Cầu Giấy, TP Hà Nội.
      Cơ sở Hồ Chí Minh: Số 95 đường số 5, Phường An Phú, Quận 2, TP. Hồ Chí Minh
      Cơ sở Hải Phòng: 4/23 phố Phạm Huy Thông, P. Lam Sơn, Q. Lê Chân, Hải Phòng
      Cơ sở Thái Bình: Số nhà 17/1, Ngõ 515 Lý Thường Kiệt, Trần Lãm, Thái Bình
      `,
      us: `
      Hanoi campus: P601 Sunrise Building at 90 Tran Thai Tong, Cau Giay District, Hanoi. 
      Ho Chi Minh Campus: No. 95, 5th Street, An Phu Ward, District 2, City. Ho Chi Minh 
      Hai Phong campus: 4/23 Pham Huy Thong Street, Lam Son Ward, Le Chan District, Hai Phong 
      Thai Binh campus: House number 17/1, Alley 515 Ly Thuong Kiet, Tran Lam, Thai Binh`,
    },
    phone: '024.32008354 / 024.32008364',
    emergency_call: '0967 66 1212',
    website: 'ketoanvina.com.vn',
  };
  language_name: any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe((res) => {
      this.language_name = res;
    });
  }
}
