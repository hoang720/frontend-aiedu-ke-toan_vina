import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  @Input('index') index: any;
  @Input('item') item:any;
 
  language_name:any;
  constructor(private languageService: LanguageChangeSharingService, private router: Router) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ViewMore(){
    this.router.navigateByUrl('/viewmore');
  }

  ngOnInit(): void {
  }

}
