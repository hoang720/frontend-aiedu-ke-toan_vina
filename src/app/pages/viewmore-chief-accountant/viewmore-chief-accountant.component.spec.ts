import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreChiefAccountantComponent } from './viewmore-chief-accountant.component';

describe('ViewmoreChiefAccountantComponent', () => {
  let component: ViewmoreChiefAccountantComponent;
  let fixture: ComponentFixture<ViewmoreChiefAccountantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreChiefAccountantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreChiefAccountantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
