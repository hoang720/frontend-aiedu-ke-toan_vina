import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { RegisterComponent } from '../viewmore-content/register/register.component';


@Component({
  selector: 'app-viewmore-chief-accountant',
  templateUrl: './viewmore-chief-accountant.component.html',
  styleUrls: ['./viewmore-chief-accountant.component.scss']
})
export class ViewmoreChiefAccountantComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1, 
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  data_translate: any ={
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
    content_1: [
      {
        title_1: {
          vi: 'Kế toán trưởng',
          us: 'Chief accountant',
        },
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
        image_1: '240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Kế toán viên`,
          us: `Accountant`,
        },
        sub_main_content_1_3:{
          vi: `CEO/ Giám đốc`,
          us: `CEO/ Director`,
        },
        sub_main_content_1_4: {
          vi: `Và các đối tượng khác có quan tâm`,
          us: `And other subjects of interest`
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Giúp người học nắm được kiến thức cốt lõi của kế toán đồng thời nâng cao nghiệp vụ quản trị`,
          us: `Help learners understand the core knowledge of accounting and improve management skills`,
        },
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Có kiến thức tốt hơn trong chuyên môn, cập nhật kiến thức tổng quát cho kế toán trưởng `,
          us: `Have better knowledge in expertise, update general knowledge for chief accountants`,
        },
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Pháp luật liên quan đến doanh nghiệp`,
          us: `Laws related to enterprises`,
        },
        sub_main_content_4_2: {
          vi: `Quản lý tài chính doanh nghiệp`,
          us: `Corporate financial management`,
        },
        sub_main_content_4_3: {
          vi: `Pháp luật về thuế`,
          us: `Tax law`,
        },
        sub_main_content_4_4: {
          vi: `Thẩm định dự án đầu tư`,
          us: `Investment project appraisal`,
        },
        sub_main_content_4_5: {
          vi: `Quan hệ tín dụng thanh toán và bảo lãnh giữa doanh nghiệp với ngân hàng `,
          us: `Credit, payment and guarantee relationship between enterprises and banks`,
        },
        sub_main_content_4_6: {
          vi: `Quản trị tài chính doanh nghiệp`,
          us: `Enterprise Financial Management`,
        },
        sub_main_content_4_7: {
          vi: `Báo cáo tài chính và phân tích báo cáo tài chính`,
          us: `Financial reporting and financial statement analysis`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 7 buổi',
          us: 'Length of study: 7 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 3.000.000 đồng',
          us: 'Tuition: 3,000,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit(): void {
  }
}
