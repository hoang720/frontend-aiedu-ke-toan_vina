import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnInit {
  @Input('bg_img') bg_img: any;
  @Input('is_right') is_right: any;

  translate_data: any= {
    Caption: {
      vi: [
        {
          title: 'ĐÀO TẠO KẾ TOÁN',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
        {
          title: 'TƯ VẤN TÀI CHÍNH',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
        {
          title: 'DỊCH VỤ KẾ TOÁN',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
      ],
      us: [
        {
          title: 'ACCOUNTANT TRAINING',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
        {
          title: 'FINANCE SUPPORT',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
        {
          title: 'ACCOUNTING SERVICES',
          image: 'kisspng-gold-metal-company-silver-ingot-checkmark-5ace322c119ac9 1.png',
        },
      ]
    }
  };

  bg_url = `url('assets/images/banner/banner.jpg')`;
  language_name = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

  ngOnInit(): void {}
}
