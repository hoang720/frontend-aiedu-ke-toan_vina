import { Component, Input, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { jobs } from '../data/jobs';
import { CvDialogComponent } from './cv-dialog/cv-dialog.component';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { JobManagementService } from '@core/services/job-management.service';

@Component({
  selector: 'app-product-content',
  templateUrl: './product-content.component.html',
  styleUrls: ['./product-content.component.scss'],
})
export class ProductContentComponent implements OnInit {
  @Input('data') data:any;
  @Input('index') index:any;
  job:any
  jobs:any[] = []
  id:any
  language_name: any;

  job_list: Observable<any> = new Observable<any>();
  data_global: any;
  job_name: any;
  constructor(
    private jobService: JobManagementService,
    private router: Router,
    private languageService: LanguageChangeSharingService,
    private route: ActivatedRoute,
    public dialog: MatDialog
    ) {
    this.languageService.getLanguageObservable().subscribe((res) => {
      this.language_name = res;
    });
    this.data_global = JSON.parse(localStorage.getItem('DATA_GLOBAL')!);
    this.job_name = this.route.snapshot.paramMap.get('job_name')
  }
  openDialog() {
    this.dialog.open(CvDialogComponent, {
      width: '800px',
      height: '800px'
    });
  }

  ngOnInit(): void {
    this.job_list = this.getJobByName(this.job_name)
  }

  getJobByName(name: any){
    return this.jobService.getJobByName(name)
  }
}
