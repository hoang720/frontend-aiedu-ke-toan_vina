import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv-dialog.component.html',
  styleUrls: ['./cv-dialog.component.scss'],
})
export class CvDialogComponent  {
  data_translate: any = {
    name: {
      vi: 'Họ và tên:',
      us: 'First and last name:',
    },
    email: {
      vi: 'Email',
      us: 'Email'
    },
    phone: {
      vi: 'Số điện thoại:',
      us: 'Phone number:',
    },
    nominee: {
      vi: 'Vị trí ứng tuyển:',
      us: 'Nominee:'
    },
    upload: {
      vi: 'Tải lên CV:',
      us: 'Upload CV:',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
