import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { ChatboxPopup } from './chatbox-popup/chatbox-popup.component';

@Component({
  selector: 'box',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.scss']
})
export class ChatboxComponent implements OnInit {
  title = "chat-float";
  response = "";
  config = {
    title: "ChatBot",
    subTitle: "New Way of learning"
  };
  setData(message: string) {
    this.response = message;
  }
  getMessage($event: any) {
    console.log($event);
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog() {
    this.dialog.open(ChatboxPopup, {
    });
  }
  ngOnInit(): void {
  }

}
