import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreTaxAccountingComponent } from './viewmore-tax-accounting.component';

describe('ViewmoreTaxAccountingComponent', () => {
  let component: ViewmoreTaxAccountingComponent;
  let fixture: ComponentFixture<ViewmoreTaxAccountingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreTaxAccountingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreTaxAccountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
