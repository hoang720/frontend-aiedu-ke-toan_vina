import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { RegisterComponent } from '../viewmore-content/register/register.component';

@Component({
  selector: 'app-viewmore-tax-accounting',
  templateUrl: './viewmore-tax-accounting.component.html',
  styleUrls: ['./viewmore-tax-accounting.component.scss']
})
export class ViewmoreTaxAccountingComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ] 
  };
  data_translate: any ={
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
    content_1: [
      {
        title_1: {
          vi: 'Kế toán thuế',
          us: 'Tax accounting',
        },
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
        image_1: 'office-with-documents-money-accounts 1.png',
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Người chưa biết gì về kế toán`,
          us: `People who do not know anything about accounting`,
        },
        sub_main_content_1_2:{
          vi: `Người đã học nhưng quên kiến thức`,
          us: `People who have learned but forgotten knowledge`,
        },
        sub_main_content_1_3:{
          vi: `Người đang làm kế toán học để cập nhật luật thuế mới `,
          us: `People who are working as accountants to update new tax laws`,
        },
        sub_main_content_1_4: {
          vi: `Giám đốc`,
          us: `Manager`
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Khóa học giúp cho kế toán/giám đốc hiểu và biết cách tính - nộp thuế cho đơn vị mình. Nắm được các luật thuế mới nhất `,
          us: `The course helps accountants/directors understand and know how to calculate - pay taxes for their units. Stay up to date with the latest tax laws`,
        },
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Sau khi học xong người học tự mình biết cách tính thuế, biết kê khai làm báo cáo thuế và nộp tờ khai cho đơn vị của mình`,
          us: `After completing the course, learners know how to calculate tax by themselves, know how to declare and make tax reports and submit declarations to their units.`,
        },
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Phí môn bài`,
          us: `License fee`,
        },
        sub_main_content_4_2: {
          vi: `Các công việc ở doanh nghiệp mới thành lập`,
          us: `Jobs in start-up businesses`,
        },
        sub_main_content_4_3: {
          vi: `Thuế GTGT (thuế nhà thầu, thuê vãng lai)`,
          us: `GTGT tax (contractor tax, current hire)`,
        },
        sub_main_content_4_4: {
          vi: `Thuế thu nhập cá nhân`,
          us: `Personal income tax`,
        },
        sub_main_content_4_5: {
          vi: `Thuế xuất-nhập khẩu`,
          us: `Import-Export Tax`,
        },
        sub_main_content_4_6: {
          vi: `Thuế thu nhập doanh nghiệp`,
          us: `Corporate income tax`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 8 buổi',
          us: 'Length of study: 8 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 1.200.000 đồng',
          us: 'Tuition: 1,200,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit(): void {
  }

}
