import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreAccountingConstructionEnterprisesComponent } from './viewmore-accounting-construction-enterprises.component';

describe('ViewmoreAccountingConstructionEnterprisesComponent', () => {
  let component: ViewmoreAccountingConstructionEnterprisesComponent;
  let fixture: ComponentFixture<ViewmoreAccountingConstructionEnterprisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreAccountingConstructionEnterprisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreAccountingConstructionEnterprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
