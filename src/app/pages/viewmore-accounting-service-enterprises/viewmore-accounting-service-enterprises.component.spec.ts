import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreAccountingServiceEnterprisesComponent } from './viewmore-accounting-service-enterprises.component';

describe('ViewmoreAccountingServiceEnterprisesComponent', () => {
  let component: ViewmoreAccountingServiceEnterprisesComponent;
  let fixture: ComponentFixture<ViewmoreAccountingServiceEnterprisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreAccountingServiceEnterprisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreAccountingServiceEnterprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
