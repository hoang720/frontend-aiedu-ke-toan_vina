import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt3',
  templateUrl: './viewmoredvkt3.component.html',
  styleUrls: ['./viewmoredvkt3.component.scss']
})
export class Viewmoredvkt3Component implements OnInit {
  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Quyết toán thuế là công việc của cơ quan thuế trong việc kiểm tra tính tuân thủ của Doanh nghiệp. Các doanh nghiệp trong quá trình hoạt động, bên cạnh hoạt động sản xuất kinh doanh thì phải có nghĩa vụ kê khai và nộp thuế. Hiện nay công việc này nhà nước giao cho doanh nghiệp tự kê khai tự nộp thuế và tự chịu trách nhiệm về việc làm của mình. Để kiểm tra việc khai báo của doanh nghiệp cơ quan thuế thường định kỳ thanh kiểm tra tại đơn vị, thường từ 3-5 năm, thậm chí có doanh nghiệp kiểm tra 1 năm 1 lần, ngoài ra còn có những doanh nghiệp làm thủ tục giải thể, hoàn thuế thì cũng cần quyết toán.`,
        us: `Tax finalization is the job of tax authorities in checking the compliance of enterprises. Enterprises in the course of operation, besides production and business activities, are obliged to declare and pay taxes. Currently, this job is assigned by the state to enterprises to declare and pay taxes themselves and take responsibility for their own work. To check the declaration of enterprises, the tax authorities usually periodically inspect at the unit, usually from 3-5 years, there are even enterprises that check once a year, in addition, there are also enterprises that do the following procedures. Dissolution and tax refund procedures also need to be settled.`,
      },
      content_2: {
        vi: `Để hoạt động quyết toán thuế diễn ra thuận lợi, Kế toán Việt Nam xin hướng dẫn Hồ sơ cần chuẩn bị khi quyết toán thuế cho doanh nghiệp như sau:`,
        us: `In order for the tax finalization to go smoothly, the Vietnamese Accountant would like to guide the documents that need to be prepared when making tax finalization for businesses as follows:`

      },
      content_3: {
        vi: ` Tùy theo thực tế của mỗi doanh nghiệp, doanh nghiệp cần chuẩn bị thêm các hồ sơ chứng từ khác theo yêu cầu kiểm tra của cơ quan thuế.`,
        us: ` Depending on the reality of each business, enterprises need to prepare additional documents according to the inspection requirements of the tax authorities.`,
      
      },
      content_4: {
        vi: ` Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: ` With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`,
      },
      content_5: {
        vi: `Nội dung công việc làm dịch vụ báo cáo tài chính mà công ty Kế toán Việt Nam cung cấp:`,
        us: `Contents of work in financial reporting services provided by Vietnam Accounting Company:`
      },
      content_6: {
        vi: `Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: `With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
