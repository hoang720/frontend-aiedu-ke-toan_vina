import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt3Component } from './viewmoredvkt3.component';

describe('Viewmoredvkt3Component', () => {
  let component: Viewmoredvkt3Component;
  let fixture: ComponentFixture<Viewmoredvkt3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
