import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt5',
  templateUrl: './viewmoredvkt5.component.html',
  styleUrls: ['./viewmoredvkt5.component.scss']
})
export class Viewmoredvkt5Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Báo cáo tài chính (BCTC) là một trong những báo cáo bắt buộc mà Doanh nghiệp phải nộp hàng năm cho cơ quan thuế và một số cơ quan khác ( Thống kê, Chứng khoán, Ngân hàng…). BCTC cung cấp cho người đọc các thông tin về tình hình hoạt động của Doanh nghiệp mình, nhưng Kế toán mà DN đang thuê đã làm đúng và đủ hay chưa ? Đặc biệt là đối với những Doanh nghiệp có sự thay đổi về nhân sự kế toán thường xuyên; hoặc những DN đang chuẩn bị quyết toán thì việc sử dụng dịch vụ soát xét Báo cáo tài chính là cần thiết.`,
        us: `Financial statements (BCTC) are one of the mandatory reports that enterprises must submit annually to tax authorities and a number of other agencies (Statistics, Securities, Banks...). Financial statements provide readers with information about the operation of their business, but have the accountants that the business is hiring done correctly and sufficiently? Especially for enterprises with frequent changes in accounting personnel; or businesses are preparing to settle, it is necessary to use the financial statement review service.`,
      },
      content_2: {
        vi: `Kiểm tra sự phù hợp của các chứng từ kế toán: Sắp xếp, phân loại, kiểm tra chứng từ kế toán về tính hợp lý, hợp lệ, hợp pháp của chứng từ theo pháp luật thuế hiện hành. `,
        us: `Checking the conformity of accounting vouchers: Sorting, classifying and checking accounting vouchers for the reasonableness, validity and legality of vouchers according to current tax laws.`

      },
      content_3: {
        vi: ` Trên cơ sở những sai sót của quý Công ty, sau khi đã thống nhất về phương án sửa chữa, chúng tôi sẽ hướng dẫn kế toán của Doanh nghiệp làm hoặc chúng tôi sẽ làm hoàn thiện bổ sung sổ sách cho Doanh nghiệp.`,
        us: ` On the basis of your company's errors, after agreeing on a repair plan, we will guide your accountants to do it or we will complete and supplement the books for you.`,
      
      },
      content_4: {
        vi: ` Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: ` With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
