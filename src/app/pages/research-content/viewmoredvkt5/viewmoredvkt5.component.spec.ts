import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt5Component } from './viewmoredvkt5.component';

describe('Viewmoredvkt5Component', () => {
  let component: Viewmoredvkt5Component;
  let fixture: ComponentFixture<Viewmoredvkt5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
