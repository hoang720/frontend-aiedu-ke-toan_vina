import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmorebs4Component } from './viewmorebs4.component';

describe('Viewmorebs4Component', () => {
  let component: Viewmorebs4Component;
  let fixture: ComponentFixture<Viewmorebs4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmorebs4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmorebs4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
