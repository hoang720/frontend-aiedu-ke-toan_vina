import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmorebs4',
  templateUrl: './viewmorebs4.component.html',
  styleUrls: ['./viewmorebs4.component.scss']
})
export class Viewmorebs4Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Việc chốt thuế chuyển quận là thủ tục thay đổi giấy phép đăng ký kinh doanh khi doanh nghiệp chuyển đổi địa chỉ kinh doanh khác quận, thành phố (khác chi cục thuế quản lý)..`,
        us: `The closing of the district transfer tax is the procedure for changing the business registration license when the enterprise changes its business address to another district or city (differently managed by the tax department).`,
      },
      content_2: {
        vi: `Hồ sơ thay đổi đăng ký kinh doanh`,
        us: `Dossier to change business registration`

      },
      content_3: {
        vi: ` Để thay đổi đăng ký kinh doanh, doanh nghiệp cần chuẩn bị hồ sơ gồm:`,
        us: ` To change business registration, enterprises need to prepare documents including:`,
      
      },
      content_4: {
        vi: ` Căn cứ theo nội dung thay đổi mà doanh nghiệp sẽ cần chuẩn bị một số tài liệu trong những tài liệu sau theo Nghị định 01/2021/NĐ-CP:`,
        us: ` Based on the changes, enterprises will need to prepare a number of documents in the following documents according to Decree 01/2021/ND-CP:`,
      },
      content_5: {
        vi: `Cần một công ty uy tín làm dịch vụ thay đổi đăng ký kinh doanh hãy liên hệ với chúng tôi – Công ty Tư vấn Giải pháp Kế toán Việt Nam`,
        us: `Need a reputable company to do business registration change services, please contact us – Vietnam Accounting Solution Consulting Company`
      }
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
