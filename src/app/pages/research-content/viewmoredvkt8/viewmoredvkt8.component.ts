import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt8',
  templateUrl: './viewmoredvkt8.component.html',
  styleUrls: ['./viewmoredvkt8.component.scss']
})
export class Viewmoredvkt8Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Kế toán là một trong những bộ phận không thể thiếu đối với bất kỳ Doanh nghiệp nào. Bộ phận Kế toán có trách nhiệm ghi chép, tổng hợp lại các hoạt động kinh doanh của Doanh nghiệp. Các Báo cáo của bộ phận kế toán sẽ phản ánh tình hình sức khỏe tài chính và kết quả kinh doanh của Doanh nghiệp.`,
        us: `Accounting is one of the indispensable parts for any business. The Accounting Department is responsible for recording and summarizing business activities of the Enterprise. The reports of the accounting department will reflect the financial health and business results of the Enterprise.`,
      },
      content_2: {
        vi: `Với vai trò quan trọng như vậy, việc Doanh nghiệp xây dựng được một bộ máy kế toán có chuyên môn nghiệp vụ tốt, quy trình quản lý bài bản là một trong những điều vô cùng quan trọng. Bộ máy kế toán vận hành tốt sẽ đưa ra được các số liệu chính xác, kịp thời, từ đó giúp cho việc đưa ra các quyết định kinh doanh của Nhà quản trị dễ dàng hơn, giúp Doanh nghiệp phát triển nhanh hơn, mạnh hơn. Nếu thông tin kế toán sai lệch sẽ dẫn đến các quyết định của Nhà quản trị không phù hợp, khiến Doanh nghiệp có thể rơi vào tình trạng khó khăn.`,
        us: `With such an important role, it is extremely important for the Enterprise to build a professional accounting system with good professional skills and a methodical management process. A well-functioning accounting system will provide accurate and timely data, thereby making it easier for the Administrator to make business decisions, helping the Enterprise grow faster and stronger. . If the accounting information is wrong, it will lead to inappropriate decisions of the Administrator, causing the Enterprise to fall into a difficult situation.`

      },
      content_3: {
        vi: `Chương trình được thiết kế phù hợp với tình hình hoạt động sản xuất kinh doanh của doanh nghiệp. Trước khi tham gia đào tạo Kế toán Việt Nam tiến hành tìm hiểu doanh nghiệp, xem xét đặc điểm của nhân sự kế toán hiện có về trình độ chuyên môn, sự hiểu biết. Từ đó xây dựng chương trình học phù hợp`,
        us: `The program is designed to suit the business situation of enterprises. Before participating in the training of Vietnamese accountants, conduct business research, consider the characteristics of existing accounting personnel in terms of professional qualifications and understanding. From there, build a suitable learning program`,
      
      },
      content_4: {
        vi: `Phần lý thuyết : tuỳ vào đối tượng nhân sự kế toán có thể sẽ dạy lại phần nguyên lý kế toán để người học hiểu bản chất. Học về luật thuế để nắm được quy định của pháp luật trong lĩnh vực kế toán tại đơn vị. Học cách làm báo cáo tài chính và cách đọc hiểu, kiểm tra báo cáo tài chính`,
        us: `Theory part: depending on the audience, accounting personnel may re-teach the principles of accounting so that learners can understand the essence. Learn about tax law to understand the legal provisions in the field of accounting at the unit. Learn how to make financial statements and how to read, understand and check financial statements`,
      },
      content_5: {
        vi: `Phần thực hành : trực tiếp sử dụng chứng từ của đơn vị để học và làm. Kết thúc khoá học, nhân sự kế toán tự tin sẽ làm được việc, lên được báo cáo cho doanh nghiệp`,
        us: `Practical part: directly use the unit's voucher to learn and do. At the end of the course, accounting personnel are confident that they will be able to do the job and report to the business`
      },
      content_6: {
        vi: `Tiết kiệm chi phí và thời gian đi lại vì Kế toán Vina sẽ đến tại đơn vị để đào tạo`,
        us: `Save cost and travel time because Vina Accounting will come to the unit for training`,
      },
      content_7: {
        vi: `Sau khi khoá học kết thúc, Kế toán Vina sẽ vẫn luôn đồng hành hướng dẫn để nhân sự làm được việc.`,
        us: `After the course ends, Vina Accountant will always accompany and guide the staff to do the job.`
      },
      content_8: {
        vi: `Nếu cần một đơn vị chuyên nghiệp, làm việc tận tâm hãy gọi ngay cho chúng tôi. Kế toán Việt Nam – đào tạo tận tâm – dịch vụ tận tình`,
        us: `If you need a professional, dedicated unit, call us immediately. Vietnamese accountants – dedicated training – dedicated service`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
