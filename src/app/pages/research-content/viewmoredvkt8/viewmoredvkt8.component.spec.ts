import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt8Component } from './viewmoredvkt8.component';

describe('Viewmoredvkt8Component', () => {
  let component: Viewmoredvkt8Component;
  let fixture: ComponentFixture<Viewmoredvkt8Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt8Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
