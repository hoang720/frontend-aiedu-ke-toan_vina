import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmorebs3',
  templateUrl: './viewmorebs3.component.html',
  styleUrls: ['./viewmorebs3.component.scss']
})
export class Viewmorebs3Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Thay đổi giấy phép đăng ký kinh doanh là việc cập nhật thông tin trên sở kế hoạch đầu tư khi doanh nghiệp có thay đổi các thông tin liên quan như vốn, người đại diện, tên công ty, địa chỉ.`,
        us: `Change of business registration license means updating information on the investment planning department when an enterprise changes related information such as capital, representative, company name, address.`,
      },
      content_2: {
        vi: `Hồ sơ thay đổi đăng ký kinh doanh`,
        us: `Dossier to change business registration`

      },
      content_3: {
        vi: ` Để thay đổi đăng ký kinh doanh, doanh nghiệp cần chuẩn bị hồ sơ gồm:`,
        us: ` To change business registration, enterprises need to prepare documents including:`,
      
      },
      content_4: {
        vi: ` Căn cứ theo nội dung thay đổi mà doanh nghiệp sẽ cần chuẩn bị một số tài liệu trong những tài liệu sau theo Nghị định 01/2021/NĐ-CP:`,
        us: ` Based on the changes, enterprises will need to prepare a number of documents in the following documents according to Decree 01/2021/ND-CP:`,
      },
      content_5: {
        vi: `Một số lưu ý khi thay đổi đăng ký kinh doanh`,
        us: `Some notes when changing business registration`,
      },
      content_6: {
        vi: ` 1.Có phải mọi nội dung thay đổi đăng ký kinh doanh đều phải nộp hồ sơ lên Sở Kế hoạch và đầu tư?`,
        us: ` 1.Does all changes in business registration have to be submitted to the Department of Planning and Investment?`,
      },
      content_7: {
        vi: ` Các trường hợp dưới đây cần nộp hồ sơ thay đổi đăng ký kinh doanh tại Sở kế hoạch và đầu tư:`,
        us: ` The following cases need to submit the application for change of business registration at the Department of Planning and Investment:`,
      },
      content_8: {
        vi: `Các trường hợp không phải nộp hồ sơ thay đổi đăng ký kinh doanh lên Sở Kế hoạch và đầu tư`,
        us: `Cases in which the application for change of business registration is not required to be submitted to the Department of Planning and Investment`,
      },
      content_9: {
        vi: `Doanh nghiệp không được thực hiện việc đăng ký thay đổi nội dung đăng ký doanh nghiệp trong các trường hợp sau:`,
        us: `Enterprises are not allowed to register changes to business registration contents in the following cases:`,
      },
      content_10:{
        vi: `2.Công ty thực hiện thủ tục thay đổi đăng ký kinh doanh tại đâu?`,
        us: `2.Where does the company carry out procedures to change business registration?`
      },
      content_11:{
        vi: `Khi muốn thay đổi đăng ký kinh doanh, doanh nghiệp cần thực hiện thủ tục tại Phòng đăng ký kinh doanh / Sở Kế hoạch và Đầu tư nơi doanh nghiệp đặt trụ sở chính.`,
        us: `When you want to change your business registration, you need to carry out the procedures at the Business Registration Office / Department of Planning and Investment where your head office is located.`
      },
      content_12:{
        vi: `Trường hợp công ty thay đổi đăng ký kinh doanh của chi nhánh/văn phòng đại diện/địa điểm kinh doanh của công ty thì thực hiện tại: Phòng đăng ký kinh doanh / Sở Kế hoạch và Đầu tư nơi doanh nghiệp đặt trụ sở của chi nhánh/văn phòng đại diện/địa điểm kinh doanh.`,
        us: `In case the company changes the business registration of the branch/representative office/business location of the company, it shall be done at: Business registration office / Department of Planning and Investment where the enterprise's head office is located. branch/representative office/business location.`
      },
      content_13:{
        vi: `3.Mức phạt chậm thông báo thay đổi đăng ký kinh doanh tới cơ quan đăng ký`,
        us: `3.Penalty for late notification of business registration changes to the registry`
      },
      content_14:{
        vi: `Khi doanh nghiệp thay đổi đăng ký kinh doanh không thông báo hoặc thông báo chậm với cơ quan nhà nước sẽ bị phạt hành chính như sau:`,
        us: `When an enterprise changes its business registration without notifying or notifying the state agency late, it will be administratively fined as follows:`
      },
      content_15: {
        vi: `Cần một công ty uy tín làm dịch vụ thay đổi đăng ký kinh doanh hãy liên hệ với chúng tôi – Công ty Tư vấn Giải pháp Kế toán Việt Nam`,
        us: `Need a reputable company to do business registration change services, please contact us – Vietnam Accounting Solution Consulting Company`
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
