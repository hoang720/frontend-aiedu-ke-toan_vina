import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmorebs3Component } from './viewmorebs3.component';

describe('Viewmorebs3Component', () => {
  let component: Viewmorebs3Component;
  let fixture: ComponentFixture<Viewmorebs3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmorebs3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmorebs3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
