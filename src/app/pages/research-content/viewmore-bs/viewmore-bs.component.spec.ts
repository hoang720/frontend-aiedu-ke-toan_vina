import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreBsComponent } from './viewmore-bs.component';

describe('ViewmoreBsComponent', () => {
  let component: ViewmoreBsComponent;
  let fixture: ComponentFixture<ViewmoreBsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreBsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreBsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
