import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmore-bs',
  templateUrl: './viewmore-bs.component.html',
  styleUrls: ['./viewmore-bs.component.scss']
})
export class ViewmoreBsComponent implements OnInit {
  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Thành lập công ty (Thành lập doanh nghiệp) là bước đầu tiên để thương nhân bước chân gia nhập thị trường. Theo đó, để một công ty được ra đời cần phải thực hiện thủ tục hành chính xin cấp Giấy chứng nhận đăng ký doanh nghiệp tại Phòng Đăng ký kinh doanh – Sở Kế hoạch và Đầu tư nơi doanh nghiệp đặt trụ sở chính của doanh nghiệp`,
        us: `Establishing a company (Establishing a business) is the first step for traders to enter the market. Accordingly, in order for a company to be established, it is necessary to carry out administrative procedures for an enterprise registration certificate at the Business Registration Office - Department of Planning and Investment where the enterprise's head office is located.`,
      },
      content_2: {
        vi: `Một số lưu ý đặc biệt khi thành lập công ty trong năm 2022`,
        us: `Some special notes when establishing a company in 2022`

      },
      content_2_1: {
        vi: `Có thể nói thủ tục thành lập công ty ngày càng đơn giản giúp hỗ trợ tối đa việc doanh nghiệp khởi nghiệp. Thủ tục thành lập công ty rất đơn giản và dễ dàng nhưng để đảm bảo các điều kiện tốt nhất cho công ty hoạt động hoặc muốn đóng cửa công ty do chưa lường trước được các khó khăn sau khi thành lập doanh nghiệp/công ty thì vô cùng phức tạp. Vì vậy, với mong muốn các doanh nhân có cái nhìn tổng thể về công ty trước khi thành lập, Kế toán Việt Nam đưa ra một số lưu ý đặc biệt như sau:`,
        us: `It can be said that the company establishment procedure is increasingly simple to help maximize the start-up business. The procedure for setting up a company is very simple and easy, but in order to ensure the best conditions for the company to operate or want to close the company due to unforeseen difficulties after the establishment of the business/company, then extremely complicated. Therefore, with the desire for entrepreneurs to have an overview of the company before its establishment, Vietnam Accountants makes some special notes as follows:`
      },
      content_3: {
        vi: ` Để đảm bảo công ty có thể phát hành được hóa đơn sau khi thành lập, doanh nghiệp ngoài việc không được đặt trụ sở ở nhà tập thể, nhà chung cư khi thuê nhà, mượn nhà làm trụ sở công ty Quý khách hàng nên ký kết hợp đồng thuê nhà, mượn nhà và yêu cầu chủ nhà cung cấp cho 02 bản sao Giấy chứng nhận quyền sử dụng đất hoặc giấy tờ tương đương.`,
        us: ` In order to ensure that the company can issue invoices after its establishment, businesses are not allowed to locate their headquarters in group houses or apartment buildings when renting or borrowing houses as public offices. Customers should sign a contract to rent or borrow a house and ask the landlord to provide 02 copies of the certificate of land use right or an equivalent document.`,
      
      },
      content_4: {
        vi: ` Quý khách hàng không cần có hộ khẩu tại tỉnh/thành phố nơi đặt trụ sở vẫn có quyền thành lập công ty.`,
        us: ` Customers who do not need a household registration in the province/city where they are headquartered still have the right to establish a company.`,
      },
      content_5: {
        vi: ` Theo quy định của Luật Doanh nghiệp hiện hành (Luật doanh nghiệp 2014), Quý khách hàng có thể lựa chọn các loại hình doanh nghiệp: Công ty TNHH một thành viên, công ty TNHH hai thành viên trở lên, Công ty cổ phần, doanh nghiệp tư nhân, Công ty hợp danh). Tuy nhiên, căn cứ vào thực tế thì nếu Quý khách hàng kinh doanh các ngành nghề thông thường nên lựa chọn 3  loại hình doanh nghiệp phổ biến là Công ty TNHH một thành viên, công ty TNHH hai thành viên trở lên, Công ty cổ phần. Trên thực tế sự khác biệt lớn nhất của công ty cổ phần so với công ty TNHH là ở vấn đề công ty cổ phần có thể huy động vốn linh hoạt và tham gia thị trường chứng khoán do đó số lượng cổ đông tối thiểu có 03 người và không hạn chế tối đa, dễ dáng chuyển nhượng sau khi không còn là cổ đông sáng lập. Còn ưu việt lớn nhất của công ty TNHH là sự tham gia của các thành viên vào công ty là rất chặt chẽ, số lượng người tham gia hạn chế từ 01 đến 50 người. Ngoài ra, chỉ khi công ty có nhu cầu tham gia thị trường chứng khoán sau này mới nên lựa chọn loại hình công ty cổ phần bởi hoạt động của công ty cổ phần, cơ cấu tổ chức và các quy định về vấn đề nội bộ của công ty cổ phần tương đối phức tạp mà doanh nghiệp chỉ sơ suất nhỏ có thể đã vi phạm các quy định của pháp luật liên quan đến các vấn đề pháp lý nội bộ của công ty cổ phần.`,
        us: `According to the provisions of the current Law on Enterprises (Law on Enterprises 2014), customers can choose from the following types of business: One-member limited liability company, two-member limited liability company. or more, Joint Stock Company, Private Enterprise, Partnership). However, based on reality, if you do business in ordinary industries, you should choose 3 popular types of businesses: One member limited liability companies, two or more members limited liability companies, joint stock companies, etc. part. In fact, the biggest difference between a joint stock company and a limited company is that a joint stock company can mobilize capital flexibly and participate in the stock market, so the minimum number of shareholders is 03. people and unlimited maximum, easy to transfer after no longer being a founding shareholder. The biggest advantage of a limited company is that the participation of members in the company is very tight, the number of participants is limited from 01 to 50 people. In addition, only when the company has a need to participate in the stock market in the future, it is advisable to choose the type of joint stock company because of the operation of the joint stock company, the organizational structure and regulations on internal issues. of a joint-stock company is relatively complicated, but with a small negligence, the enterprise may have violated the provisions of the law related to the internal legal issues of the joint-stock company.`,
      },
      content_6: {
        vi: ` Trên thực tế tên công ty ngày càng hạn chế do số lượng doanh nghiệp ngày một nhiều. Tuy nhiên, để có thể đặt được tên công ty theo mong muốn rất đơn giản bằng cách thêm các tiền tố hoặc hậu tố vào tên công ty là có thể đăng ký được. Tuy nhiên, cần tránh các tên nổi tiếng đã đăng ký độc quyền nhãn hiệu vì có thể doanh nghiệp có nguy cơ bị yêu cầu đổi tên do trùng với nhãn hiệu đã được bảo hộ độc quyền tại Việt Nam.`,
        us: ` In fact, company names are increasingly limited due to the increasing number of businesses. However, to be able to set the desired company name is very simple by adding prefixes or suffixes to the company name that can be registered. However, it is necessary to avoid well-known names that have been exclusively registered for trademarks because there is a risk that enterprises may be asked to change their names because they are identical with trademarks that have been exclusively protected in Vietnam.`,
      },
      content_7: {
        vi: ` Theo quy định của Luật Doanh nghiệp thì vốn là doanh nghiệp tự kê khai và tự chịu trách nhiệm (kể cả các ngành nghề có yêu cầu vốn pháp định thì doanh nghiệp cũng chỉ cần kê khai đủ mức vốn quy định mà không cần chứng minh hay xác nhận nguồn vốn thực tế). Tuy nhiên, căn cứ vào nhu cầu hoạt động như: mức hợp đồng ký kết với đối tác, sự tham gia vào dự án, số vốn phải ký quỹ đối với một số ngành đặc thù, mức thuế môn bài muốn đóng mà doanh nghiệp lựa chọn mức vốn điều lệ hợp lý, phù hợp và tính đến tính chịu trách nhiệm của các chủ sở hữu doanh nghiệp/công ty khi cam kết mức vốn của mình.`,
        us: ` According to the provisions of the Law on Enterprises, capital is an enterprise that declares and is responsible for itself (including industries requiring legal capital, the enterprise shall be responsible for the registration and declaration of charter capital). It is only necessary to declare the required amount of capital without needing to prove or confirm the actual capital source). However, based on operational needs such as: level of contract signed with a partner, participation in the project, the amount of capital that must be deposited for some specific industries, the license tax rate that enterprises want to pay. select a reasonable and appropriate level of charter capital and take into account the responsibility of business/company owners when committing their capital.`,
      },
      content_8: {
        vi: `Theo quy định của pháp luật thủ tục tăng vốn điều lệ công ty tương đối đơn giản, trong khi đó thủ tục giảm vốn điều lệ công ty lại tương đối nhiều điều kiện và cần thời gian nhất định.`,
        us: `According to the law, the procedure to increase the charter capital of the company is relatively simple, while the procedure to reduce the charter capital of the company is relatively complicated and requires a certain time.`,
      },
      content_9: {
        vi: ` Hiện nay doanh nghiệp được kinh doanh các ngành nghề pháp luật không cấm. Tuy nhiên, doanh nghiệp chỉ được phép kinh doanh các ngành nghề mà mình đã đăng ký và kê khai trong hồ sơ đăng ký doanh nghiệp, doanh nghiệp nên lựa chọn phạm vi rộng khi đăng ký các ngành nghề kinh doanh cho công ty trong hồ sơ đăng ký thành lập công ty. Có thể nói điểm ưu việt của Luật Doanh nghiệp hiện hành là doanh nghiệp chưa cần xuất trình các điều kiện đối với các ngành nghề yêu cầu chứng chỉ hành nghề. Do đó, tùy thuộc vào nhu cầu hoạt động doanh nghiệp có thể lựa chọn mở rộng các ngành nghề kinh doanh để tránh sau khi hoạt động phát sinh thêm thủ tục bổ sung ngành nghề do khi thành lập chưa bao quát các ngành nghề dự định kinh doanh.`,
        us: ` Currently, enterprises are allowed to do business in sectors not prohibited by law. However, enterprises are only allowed to do business in the lines of business that they have registered and declared in the business registration dossier, enterprises should choose a wide range when registering business lines for the company in the application. company registration form. It can be said that the advantage of the current Enterprise Law is that enterprises do not need to present the conditions for professions requiring a practicing certificate. Therefore, depending on operational needs, enterprises can choose to expand their business lines to avoid additional procedures arising after the operation because they do not cover the intended industries when established. business.`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
