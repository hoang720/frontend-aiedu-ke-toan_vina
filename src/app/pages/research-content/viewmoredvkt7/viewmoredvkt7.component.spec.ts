import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt7Component } from './viewmoredvkt7.component';

describe('Viewmoredvkt7Component', () => {
  let component: Viewmoredvkt7Component;
  let fixture: ComponentFixture<Viewmoredvkt7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
