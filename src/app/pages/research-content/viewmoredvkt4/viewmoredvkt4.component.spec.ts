import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt4Component } from './viewmoredvkt4.component';

describe('Viewmoredvkt4Component', () => {
  let component: Viewmoredvkt4Component;
  let fixture: ComponentFixture<Viewmoredvkt4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
