import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt4',
  templateUrl: './viewmoredvkt4.component.html',
  styleUrls: ['./viewmoredvkt4.component.scss']
})
export class Viewmoredvkt4Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Hoàn thuế là việc nhà nước trả lại tiền mà doanh nghiệp đã nộp thừa. Trong phần hoàn thuế này có hoàn về thuế GTGT, thu nhập cá nhân`,
        us: `Tax refund is the state's return of money that enterprises have overpaid. In this tax refund section, there is a refund of VAT and personal income`,
      },
      content_2: {
        vi: `Tuy nhiên trên thực tế để thu lại tiền thuế dư thừa ấy không phải dễ, quy trình phải trải qua nhiều thủ tục, hồ sơ phức tạp, thậm chí có thể doanh nghiệp phải tốn một khoản phí không nhỏ. Vậy các thủ tục hoàn thuế là gì? Hồ sơ hoàn thuế ra sao? Làm như thế nào để doanh nghiệp có thể hoàn thuế được nhiều nhất với chi phí ít nhất?`,
        us: `However, in reality, it is not easy to collect that excess tax, the process must go through many complicated procedures and documents, and it may even cost businesses a large fee. So what are the tax refund procedures? How is the tax refund file? How can businesses get the most tax refund with the least cost?`

      },
      content_3: {
        vi: ` Đã đăng ký kinh doanh, đăng ký nộp thuế giá trị gia tăng theo phương pháp khấu trừ, hoặc dự án tìm kiếm thăm dò và phát triển mỏ dầu khí đang trong giai đoạn đầu tư, chưa đi vào hoạt động, nếu thời gian đầu tư từ 01 năm trở lên thì được hoàn thuế giá trị gia tăng của hàng hóa, dịch vụ sử dụng cho đầu tư theo từng năm. Trường hợp, nếu số thuế giá trị gia tăng lũy kế của hàng hóa, dịch vụ mua vào sử dụng cho đầu tư từ 300 triệu đồng trở lên thì được hoàn thuế giá trị gia tăng.`,
        us: ` Having registered for business, registered to pay value added tax by deduction method, or the project of prospecting, exploration and development of oil and gas fields is in the investment stage, not yet put into operation, if the initial period Investments from 01 year or more will be refunded value added tax on goods and services used for investment each year. In case, if the accumulated value-added tax amount of goods and services purchased and used for investment is 300 million VND or more, the value-added tax will be refunded.`,
      
      },
      content_4: {
        vi: ` đang trong giai đoạn đầu tư thì cơ sở kinh doanh thực hiện kê khai riêng đối với dự án đầu tư và phải kết chuyển thuế GTGT đầu vào của dự án đầu tư để bù trừ với việc kê khai thuế GTGT của hoạt động sản xuất kinh doanh đang thực hiện. Số thuế GTGT được kết chuyển của dự án đầu tư tối đa bằng số thuế GTGT phải nộp của hoạt động sản xuất kinh doanh trong kỳ của cơ sở kinh doanh.`,
        us: ` being in the investment stage, the business establishment shall make a separate declaration for the investment project and must transfer the input VAT of the investment project to offset the VAT declaration of production and business activities. business is doing. The transferred VAT amount of an investment project is not equal to the payable VAT amount of production and business activities in the period of the business establishment.`,
      },
      content_5: {
        vi: `Ví dụ 2.31: Công ty B có trụ sở chính tại Hải Phòng, tháng 7/2016, Công ty có dự án đầu tư tại Hải Phòng, dự án đang trong giai đoạn đầu tư, Công ty B thực hiện kê khai riêng thuế GTGT đầu vào của dự án đầu tư này. Tháng 8/2016, số thuế GTGT đầu vào của dự án đầu tư là 500 triệu đồng; số thuế GTGT phải nộp của hoạt động sản xuất kinh doanh mà Công ty đang thực hiện là 200 triệu đồng. Công ty B phải bù trừ 200 triệu đồng thuế GTGT đầu vào của dự án đầu tư với số thuế phải nộp của hoạt động sản xuất kinh doanh đang thực hiện (200 triệu đồng). Vậy, kỳ tính thuế tháng 8/2016 Công ty B có số thuế GTGT đầu vào của dự án đầu tư chưa được khấu trừ hết là 300 triệu đồng. Công ty B được xét hoàn thuế GTGT cho dự án đầu tư.`,
        us: `Example 2.31: Company B has its head office in Hai Phong, in July 2016, the company has an investment project in Hai Phong, the project is in the investment stage, company B makes a separate declaration of VAT. input of this investment project. In August 2016, the input VAT amount of the investment project was 500 million VND; The payable VAT amount of production and business activities that the Company is performing is VND 200 million. Company B has to offset 200 million VND of input VAT of the investment project against the payable tax amount of the current production and business activities (200 million VND). So, in the tax period of August 2016, Company B has 300 million VND in input VAT of the investment project that has not been fully deducted. Company B is considered for VAT refund for investment projects.`
      },
      content_6: {
        vi: `nơi đóng trụ sở chính, đang trong giai đoạn đầu tư chưa đi vào hoạt động, chưa đăng ký kinh doanh, chưa đăng ký thuế thì cơ sở kinh doanh lập hồ sơ khai thuế riêng cho dự án đầu tư đồng thời phải kết chuyển thuế GTGT đầu vào của dự án đầu tư để bù trừ với việc kê khai thuế GTGT của hoạt động sản xuất kinh doanh đang thực hiện. Số thuế GTGT được kết chuyển của dự án đầu tư tối đa bằng số thuế GTGT phải nộp của hoạt động sản xuất kinh doanh trong kỳ của cơ sở kinh doanh.`,
        us: `Where the head office is located, is in the investment stage, has not yet been put into operation, has not yet registered for business, or has not yet registered for tax, the business establishment shall make a separate tax declaration file for the investment project and concurrently have to transfer the tax. Input VAT of an investment project to offset the VAT declaration of production and business activities being carried out. The transferred VAT amount of an investment project is not equal to the payable VAT amount of production and business activities in the period of the business establishment.`,
      },
      content_7: {
        vi: `Ví dụ 2.35: Công ty B có trụ sở chính tại Hải Phòng, tháng 7/2016, Công ty có dự án đầu tư mới tại Thái Bình, dự án đang trong giai đoạn đầu tư, chưa đi vào hoạt động, chưa đăng ký kinh doanh, chưa đăng ký thuế, Công ty B thực hiện kê khai riêng thuế GTGT đầu vào của dự án đầu tư này tại Hải Phòng trên Tờ khai thuế GTGT dành cho dự án đầu tư. Tháng 8/2016, số thuế GTGT đầu vào của dự án đầu tư là 500 triệu đồng; số thuế GTGT phải nộp của hoạt động sản xuất kinh doanh mà Công ty đang thực hiện là 200 triệu đồng. Công ty B phải bù trừ 200 triệu đồng thuế GTGT đầu vào của dự án đầu tư với số thuế phải nộp của hoạt động sản xuất kinh doanh đang thực hiện (200 triệu đồng). Vậy, kỳ tính thuế tháng 8/2016 Công ty B có số thuế GTGT đầu vào của dự án đầu tư mới chưa được khấu trừ hết là 300 triệu đồng. Công ty B được xét hoàn thuế GTGT cho dự án đầu tư.`,
        us: `Example 2.35: Company B has its head office in Hai Phong, in July 2016, the Company has a new investment project in Thai Binh, the project is in the investment stage, has not been put into operation, and has not been registered. business, not yet registered for tax, Company B makes a separate declaration of input VAT of this investment project in Hai Phong on the VAT declaration for the investment project. In August 2016, the input VAT amount of the investment project was 500 million VND; The payable VAT amount of production and business activities that the Company is performing is VND 200 million. Company B has to offset 200 million VND of input VAT of the investment project against the payable tax amount of the current production and business activities (200 million VND). So, in the tax period of August 2016, Company B has the input VAT amount of the new investment project that has not yet been fully deducted is 300 million VND. Company B is considered for VAT refund for investment projects.`
      },
      content_8: {
        vi: `Cơ sở kinh doanh trong tháng (đối với trường hợp kê khai theo tháng), quý (đối với trường hợp kê khai theo quý) có hàng hóa, dịch vụ xuất khẩu có số thuế giá trị gia tăng đầu vào chưa được khấu trừ từ 300 triệu đồng trở lên thì được hoàn thuế giá trị gia tăng theo tháng, quý; trường hợp trong tháng, quý số thuế giá trị gia tăng đầu vào chưa được khấu trừ chưa đủ 300 triệu đồng thì được khấu trừ vào tháng, quý tiếp theo.`,
        us: `Business establishments in the month (for the case of monthly declaration), (for the case of quarterly declaration) have exported goods and services whose input value-added tax amount has not yet been deducted from VND 300 million or more will be refunded value added tax on a monthly or quarterly basis; In case the input value-added tax amount that has not yet been deducted is less than VND 300 million in a month or quarter, it will be deducted in the following month or quarter.`
      },
      content_9: {
        vi: `Cơ sở kinh doanh trong tháng/quý vừa có hàng hoá, dịch vụ xuất khẩu, vừa có hàng hoá, dịch vụ bán trong nước thì cơ sở kinh doanh phải hạch toán riêng số thuế GTGT đầu vào sử dụng cho sản xuất kinh doanh hàng hóa, dịch vụ xuất khẩu. Trường hợp không hạch toán riêng được thì số thuế giá trị gia tăng đầu vào của hàng hóa, dịch vụ xuất khẩu được xác định theo tỷ lệ giữa doanh thu của hàng hóa, dịch vụ xuất khẩu trên tổng doanh thu hàng hóa, dịch vụ của các kỳ khai thuế giá trị gia tăng tính từ kỳ khai thuế tiếp theo kỳ hoàn thuế liền trước đến kỳ đề nghị hoàn thuế hiện tại.`,
        us: `If a business establishment has both exported goods and services in a month/quarter and domestically sold goods and services, the business establishment must separately account the input VAT amount used for the production and trading of goods and services. export goods and services. In case it is not possible to do separate accounting, the input value-added tax amount of exported goods and services is determined according to the ratio between the turnover of exported goods and services to the total turnover of goods and services. of value-added tax return periods from the tax return period following the preceding tax refund period to the current tax refund claim period.`
      },
      content_10: {
        vi: `Số thuế GTGT đầu vào của hàng hóa, dịch vụ xuất khẩu (bao gồm số thuế GTGT đầu vào hạch toán riêng được và số thuế GTGT đầu vào được phân bổ theo tỷ lệ nêu trên) nếu sau khi bù trừ với số thuế GTGT phải nộp của hàng hóa, dịch vụ tiêu thụ trong nước còn lại từ 300 triệu đồng trở lên thì cơ sở kinh doanh được hoàn thuế cho hàng hóa, dịch vụ xuất khẩu. Số thuế GTGT được hoàn của hàng hóa, dịch vụ xuất khẩu không vượt quá doanh thu của hàng hóa, dịch vụ xuất khẩu nhân với 10%.`,
        us: `The input VAT amount of exported goods and services (including the separately accounted input VAT amount and the input VAT amount allocated according to the above ratio) if, after clearing with the required VAT amount, If the remaining payment of domestically-consumed goods and services is 300 million VND or more, business establishments will be entitled to a tax refund for exported goods and services. The refundable VAT amount of exported goods and services must not exceed the turnover of exported goods and services multiplied by 10%.`
      },
      content_11: {
        vi: `Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: `With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`
      }
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
