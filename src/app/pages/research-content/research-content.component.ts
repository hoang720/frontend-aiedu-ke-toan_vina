import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-research-content',
  templateUrl: './research-content.component.html',
  styleUrls: ['./research-content.component.scss'],
})
export class ResearchContentComponent implements OnInit {
  @Input('data') data: any;
  @Input('index') index: any;
  
  ngOnInit(): void {
    
  }

  
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  
}
