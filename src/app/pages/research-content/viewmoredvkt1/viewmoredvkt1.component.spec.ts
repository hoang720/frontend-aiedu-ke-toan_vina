import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt1Component } from './viewmoredvkt1.component';

describe('Viewmoredvkt1Component', () => {
  let component: Viewmoredvkt1Component;
  let fixture: ComponentFixture<Viewmoredvkt1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
