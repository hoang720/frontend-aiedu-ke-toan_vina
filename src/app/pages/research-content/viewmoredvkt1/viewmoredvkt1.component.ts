import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt1',
  templateUrl: './viewmoredvkt1.component.html',
  styleUrls: ['./viewmoredvkt1.component.scss']
})
export class Viewmoredvkt1Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Tại Hà Nội, công ty kế toán Việt Nam (Kế toán Vina) tự hào là một trong những công ty cung cấp dịch vụ kế toán thuế trọn gói  chất lượng, uy tín được nhiều khách hàng sử dụng.`,
        us: `In Hanoi, Vietnam accounting firm (Vina Accounting) is proud to be one of the companies providing quality and reputable tax accounting services used by many customers.`,
      },
      content_2: {
        vi: `Giải thể: là việc doanh nghiệp chấm dứt sự tồn tại của mình một cách hợp pháp và vĩnh viễn`,
        us: `Dissolution: is the termination of its existence legally and permanently by an enterprise`

      },
      content_3: {
        vi: ` Tạm ngừng : là việc doanh nghiệp dừng hoạt động trong một thời gian. Theo quy định hiện hành, mỗi doanh nghiệp được phép tạm dừng mỗi lần 1 năm, không quá 2 lần (tức là DN được tạm dừng tổng thời gian không quá 2 năm)`,
        us: ` Suspended: is the cessation of business for a period of time. According to current regulations, each enterprise is allowed to suspend each time for 1 year, no more than 2 times (ie, enterprises are allowed to suspend for a total period of no more than 2 years).`,
      
      },
      content_4: {
        vi: ` Dịch vụ của chúng tôi giúp bạn tối đa ngân sách khi hạn chế tối đa đến các chi phí phát sinh như:`,
        us: ` Our services help you maximize your budget while minimizing costs incurred such as:`,
      },
      content_5: {
        vi: `Đội ngũ kế toán viên lành nghề của chúng tôi sẽ làm việc trên phần mềm kế toán chuyên nghiệp, ghi chép sổ sách theo cách thức đã được thống nhất. Kết quả là sẽ giúp sổ sách luôn suôn sẻ, gọn gàng và dễ theo dõi.`,
        us: `Our team of skilled accountants will work on professional accounting software, keeping records in an agreed-upon manner. As a result, the books will always be smooth, neat and easy to follow.`
      },
      content_6: {
        vi: `Luôn cập nhật kịp thời về luật và chế độ kế toán hiện hành mang lại lợi ích thiết thực cho Quý Công ty. Quý Công ty còn được Kế toán Việt Nam tư vấn một cách tối ưu các khoản thuế, chi phí phải nộp cho Nhà nước.`,
        us: `Staying up-to-date on current laws and accounting regimes brings practical benefits to your company. Your company is also advised by the Vietnamese Accountant to optimize the taxes and expenses payable to the State.`,
      },
      content_7: {
        vi: `Quý Công ty không sợ phải nộp phạt do làm thiếu, làm sai hay do chậm nộp các loại báo cáo vì Kế toán Việt Nam cam kết nộp phạt 100% nếu như làm sai.`,
        us: `Your company is not afraid to pay fines due to missing, wrong or late submission of all kinds of reports because Vietnam Accountants commits to pay 100% fine if doing wrong.`
      },
      content_8: {
        vi: `Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: `With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
