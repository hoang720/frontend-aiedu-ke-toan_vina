import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt6Component } from './viewmoredvkt6.component';

describe('Viewmoredvkt6Component', () => {
  let component: Viewmoredvkt6Component;
  let fixture: ComponentFixture<Viewmoredvkt6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
