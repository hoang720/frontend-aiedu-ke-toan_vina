import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt6',
  templateUrl: './viewmoredvkt6.component.html',
  styleUrls: ['./viewmoredvkt6.component.scss']
})
export class Viewmoredvkt6Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Doanh nghiệp của bạn mới thành lập, hay đã thành lập lâu nhưng có nhu cầu đóng bảo hiểm xã hội cho nhân viên. `,
        us: `Your business is newly established, or has been established for a long time but needs to pay social insurance for employees.`,
      },
      content_2: {
        vi: `Tiền lương để trả cho nhân viên làm thủ tục bảo hiểm theo khảo sát hiện nay rơi vào khoảng từ 4-6 triệu/tháng, nhưng nếu bạn sử dụng dịch vụ bảo hiểm của Kế toán Việt Nam thì sẽ tiết kiệm đáng kể khoản này.`,
        us: `The salary to pay insurance workers according to the survey currently ranges from 4-6 million/month, but if you use the insurance service of Vietnamese Accountants, you will save significantly this amount.`

      },
      content_3: {
        vi: ` Giúp Doanh nghiệp tiết kiệm thời gian, chi phí , không còn bận tâm về những thủ tục hành chính phức tạp mà tập trung vào mảng khác để phát triển công ty.`,
        us: ` Help businesses save time and costs, no longer bother about complicated administrative procedures, but focus on other areas to develop the company.`,
      
      },
      content_4: {
        vi: ` Kể từ ngày 01/01/2018 theo Quyết định 595/QĐ-BHXH quy định về Bảo hiểm xã hội, Bảo hiểm y tế, Bảo hiểm thất nghiệp bắt buộc đối với Doanh nghiệp trong đó nêu rõ việc bổ sung thêm các đối tượng tham gia BHXH:`,
        us: ` From January 1, 2018 according to Decision 595/QD-BHXH providing for compulsory social insurance, health insurance and unemployment insurance for enterprises, clearly stating the additional subjects: participating in social insurance:`,
      },
      content_5: {
        vi: `Người lao động là công dân nước ngoài vào làm việc tại Việt Nam có giấy phép lao động, chứng chỉ hành nghề hoặc giấy phép hành nghề do cơ quan có thẩm quyền của Việt Nam cấp được tham gia BHXH bắt buộc theo quy định của Chính phủ.`,
        us: `Employees who are foreign citizens coming to Vietnam to work with a work permit, practice certificate or practice license issued by a competent Vietnamese agency are entitled to participate in compulsory social insurance in accordance with the Government's regulations.`
      },
      content_6: {
        vi: `Với việc lựa chọn dịch vụ bảo hiểm tại Kế toán Việt Nam , quý khách hàng sẽ tiết kiệm được rất nhiều thời gian và chi phí, đồng thời hồ sơ bảo hiểm của Doanh nghiệp luôn phù hợp với quy định của Pháp luật.`,
        us: `With the choice of insurance services at Accounting Vietnam, customers will save a lot of time and costs, and at the same time, the enterprise's insurance records are always consistent with the provisions of the Law.`
      }
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
