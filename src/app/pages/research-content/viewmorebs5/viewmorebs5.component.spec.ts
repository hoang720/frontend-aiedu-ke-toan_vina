import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmorebs5Component } from './viewmorebs5.component';

describe('Viewmorebs5Component', () => {
  let component: Viewmorebs5Component;
  let fixture: ComponentFixture<Viewmorebs5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmorebs5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmorebs5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
