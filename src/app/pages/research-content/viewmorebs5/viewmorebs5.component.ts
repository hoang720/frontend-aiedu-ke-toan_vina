import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmorebs5',
  templateUrl: './viewmorebs5.component.html',
  styleUrls: ['./viewmorebs5.component.scss']
})
export class Viewmorebs5Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Trong quá trình hoạt động không tránh khỏi những khó khăn, khi DN chưa có hướng đi hoặc vì một lý do nào đó buộc phải dừng hoạt động. DN nên nắm được quy định của luật để tránh bị phạt`,
        us: `In the process of operation, it is inevitable that the enterprise will have no direction or for some reason have to stop operating. Businesses should understand the provisions of the law to avoid fines`,
      },
      content_2: {
        vi: `Tại sao bạn nên chọn sử dụng dịch vụ kế toán thuế trọn gói tại Kế toán Vina ?`,
        us: `Why should you choose to use tax accounting services at Vina Accounting?`

      },
      content_3: {
        vi: ` Theo khảo sát thì chi phí tối thiểu cho một kế toán thuế hiện nay sẽ rơi vào tầm từ 4 triệu VNĐ - 8 triệu VNĐ/tháng. Nhưng việc sử dụng dịch vụ kế toán trọn gói của Kế toán Việt Nam sẽ giúp bạn tiết kiệm đáng kể khoản chi này. `,
        us: ` According to the survey, the minimum cost for a tax accountant today will be in the range of 4 million VND - 8 million VND/month. But using the full accounting service of Accounting Vietnam will help you save a lot of this expense.`,
      
      },
      content_4: {
        vi: ` Dịch vụ của chúng tôi giúp bạn tối đa ngân sách khi hạn chế tối đa đến các chi phí phát sinh như:`,
        us: ` Example: THC enterprise applies for the first suspension for 1 year from March 1, 2022-February 28, 2023. Second time from 5/3/2023-4/3/2024`,
      },
      content_5: {
        vi: `Như vậy trong khoảng thời gian từ 1/3/2023-5/3/2023 doanh nghiệp có phải nộp những loại tờ khai gì?`,
        us: `So, during the period from March 1, 2023 to March 5, 2023, what types of declarations do businesses have to submit?`
      },
      content_6: {
        vi: `Trả lời : Theo quy định khi doanh nghiệp hết thời hạn tạm ngừng phải nộp các loại tờ khai.Cho dù không có phát sinh cũng phải nộp các tờ khai sau:`,
        us: `Answer: According to the regulations, when the enterprise ceases to be suspended, the following types of declarations must be submitted. Even if there are no arising, the following declarations must be submitted:`,
      },
      content_7: {
        vi: `P/s : đây là lỗi vi phạm của rất nhiều doanh nghiệp không nắm vững luật. Kế toán Vina khuyến cáo để anh chị nắm được`,
        us: `P/s: this is a violation of many businesses that do not know the law. Vina accountant recommends for you to understand`
      },
      content_8: {
        vi: `Sau khi thông tin doanh nghiệp trong Cơ sở dữ liệu quốc gia về đăng ký doanh nghiệp sang tình trạng đang làm thủ tục giải thể: Doanh nghiệp thực hiện thủ tục hoàn thành nghĩa vụ thuế với Cơ quan thuế theo quy định của Luật Quản lý thuế.`,
        us: `After the enterprise information in the National Business Registration Database changes to the status of undergoing dissolution procedures: The enterprise shall carry out procedures to fulfill tax obligations with the tax authority according to the provisions of the Law on Management tax.`,
      },
      content_9: {
        vi: `Báo cáo thanh lý tài sản doanh nghiệp; danh sách chủ nợ và số nợ đã thanh toán, gồm cả thanh toán hết các khoản nợ về thuế và nợ tiền đóng bảo hiểm xã hội, bảo hiểm y tế, bảo hiểm thất nghiệp cho người lao động sau khi quyết định giải thể doanh nghiệp (nếu có).`,
        us: `Report on liquidation of corporate assets; the list of creditors and the amount of debt paid, including payment of all tax debts and payment of social insurance, health insurance and unemployment insurance premiums for employees after the decision to dissolve the business occupation (if any).`,
      },
      content_10: {
        vi: `(Doanh nghiệp lưu ý: Đối với doanh nghiệp sử dụng con dấu do cơ quan công an cấp, doanh nghiệp có trách nhiệm trả con dấu, Giấy chứng nhận đã đăng ký mẫu con dấu cho cơ quan công an theo quy định khi làm thủ tục giải thể.)`,
        us: `222 / 5.000
        Kết quả dịch
        (Note: For enterprises using the seal issued by the police agency, the enterprise is responsible for returning the seal and certificate of seal sample registration to the police agency according to regulations when carrying out procedures. dissolution.) `,
      },
      content_11: {
        vi: `Cần một công ty uy tín làm dịch vụ thay đổi đăng ký kinh doanh hãy liên hệ với chúng tôi – Công ty Tư vấn Giải pháp Kế toán Việt Nam`,
        us: `Need a reputable company to do business registration change services, please contact us – Vietnam Accounting Solution Consulting Company`
      }
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
