import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-viewmoredvkt2',
  templateUrl: './viewmoredvkt2.component.html',
  styleUrls: ['./viewmoredvkt2.component.scss']
})
export class Viewmoredvkt2Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Công ty Kế toán Việt Nam nhận làm dịch vụ báo cáo tài chính cho các Doanh nghiệp với mức phí hợp lý và thời gian nhanh chóng nhất.`,
        us: `Vietnam Accounting Company accepts financial reporting services for enterprises with reasonable fees and fastest time.`,
      },
      content_2: {
        vi: `Để giảm thiểu chi phí, nhiều Doanh nghiệp không thuê kế toán thuế mà tự mình kê khai thuế hàng tháng, quý. Đến cuối năm, là thời hạn nộp báo cáo tài chính và hoàn thiện hệ thống sổ sách kế toán thì mới thuê ngoài.`,
        us: `In order to reduce costs, many businesses do not hire a tax accountant but declare their taxes on a monthly and quarterly basis. At the end of the year, which is the deadline for submitting financial statements and perfecting the accounting system, then it is time to outsource.`

      },
      content_3: {
        vi: ` Phí dịch vụ của chúng tôi luôn luôn thấp hơn nhiều lần so với chi phí mà doanh nghiệp bỏ ra để thuê nhân viên kế toán có trình độ, có kinh nghiệm để thực hiện những công việc tương đương. Quý Công ty có thể hoàn toàn yên tâm phát triển kinh doanh không cần lo lắng đến bất kì vấn đề gì liên quan tới các báo cáo thuế - kế toán`,
        us: ` Our service fees are always many times lower than the cost that businesses spend to hire qualified and experienced accountants to perform similar jobs. Your company can be completely assured of business development without worrying about any problems related to tax and accounting reports.`,
      
      },
      content_4: {
        vi: ` Chúng tôi cam kết bảo mật thông tin trên Báo cáo tài chính cho khách hàng và  có trách nhiệm giải trình số liệu trên Báo cáo tài chính do chúng tôi lập trước các Cơ quan quản lý Nhà nước về chuyên môn.`,
        us: ` We are committed to keeping the information on the financial statements confidential for our customers and are responsible for explaining the data in the financial statements prepared by us before professional state management agencies.`,
      },
      content_5: {
        vi: `Nội dung công việc làm dịch vụ báo cáo tài chính mà công ty Kế toán Việt Nam cung cấp:`,
        us: `Contents of work in financial reporting services provided by Vietnam Accounting Company:`
      },
      content_6: {
        vi: `Với phương châm “ Bảo vệ lợi ích của khách hàng là số 1” Công ty Kế toán Việt Nam luôn được sự tin tưởng và ủng hộ của Quý khách hàng. Nếu có vấn đề gì về Thuế và kế toán, hãy gọi ngay cho chúng tôi.`,
        us: `With the motto "Protecting the interests of customers is number 1", Vietnam Accounting Company has always been trusted and supported by customers. If you have any tax or accounting problems, call us immediately.`,
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
