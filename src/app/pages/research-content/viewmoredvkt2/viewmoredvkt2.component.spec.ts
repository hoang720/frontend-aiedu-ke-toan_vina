import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmoredvkt2Component } from './viewmoredvkt2.component';

describe('Viewmoredvkt2Component', () => {
  let component: Viewmoredvkt2Component;
  let fixture: ComponentFixture<Viewmoredvkt2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmoredvkt2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmoredvkt2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
