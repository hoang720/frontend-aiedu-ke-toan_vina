import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Viewmorebs2Component } from './viewmorebs2.component';

describe('Viewmorebs2Component', () => {
  let component: Viewmorebs2Component;
  let fixture: ComponentFixture<Viewmorebs2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Viewmorebs2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Viewmorebs2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
