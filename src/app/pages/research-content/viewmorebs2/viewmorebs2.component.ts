import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';


@Component({
  selector: 'app-viewmorebs2',
  templateUrl: './viewmorebs2.component.html',
  styleUrls: ['./viewmorebs2.component.scss']
})
export class Viewmorebs2Component implements OnInit {

  @Input('data') data: any;
  @Input('index') index: any;
  
  business_services: any =[
    {
      content_1: {
        vi: `Có rất nhiều nguyên nhân khiến cho doanh nghiệp rơi vào trạng thái “NTT ngừng hoạt động nhưng chưa hoàn thành thủ tục đóng MST”. Để hoạt động được trở lại công ty cần làm thủ tục mở mã số thuế`,
        us: `There are many reasons that cause businesses to fall into the state of "NTT has stopped operating but has not completed the procedure to close MST". In order to resume operations, the company needs to go through the procedures for opening a tax identification number`,
      },
      content_2: {
        vi: `Quy trình thủ tục xin mở lại mã số thuế doanh nghiệp (Thủ tục khôi phục mã số thuế doanh nghiệp)`,
        us: `Procedures for applying for re-opening of corporate tax identification numbers (Corporate tax identification number recovery procedures)`

      },
      content_2_1: {
        vi: `1. Đối với trường hợp người nộp thuế đề nghị khôi phục MST do cơ quan có thẩm quyền có văn bản hủy bỏ văn bản thu hồi Giấy phép và trường hợp người nộp thuế không thuộc trường hợp phải chấm dứt hiệu lực mã số thuế do lỗi của cơ quan thuế:`,
        us: `1. For the case where the taxpayer requests to restore the MST because the competent authority has written to cancel the document revoking the License and the taxpayer is not in the case of having to terminate the validity of the TIN due to Tax authority error:`
      },
      content_3: {
        vi: ` Văn bản đề nghị khôi phục mã số thuế (Mẫu 25/ĐK-TCT ban hành kèm theo thông tư 95/2016/TT-BTC);`,
        us: ` A written request for tax code restoration (Form 25/DK-TCT issued together with Circular 95/2016/TT-BTC);`,
      
      },
      content_4: {
        vi: ` Bản photo văn bản hủy bỏ văn bản thu hồi Giấy phép (đối với trường hợp NNT đề nghị khôi phục MST do cơ quan có thẩm quyền có văn bản hủy bỏ văn bản thu hồi Giấy phép);`,
        us: ` A copy of the written cancellation of the License revocation document (for the case where the taxpayer requests to restore the MST because the competent authority has written to cancel the document revoking the License);`,
      },
      content_5: {
        vi: `Trong thời hạn 03 (ba) ngày làm việc kể từ ngày cơ quan thuế nhận được hồ sơ đề nghị khôi phục mã số thuế, cơ quan thuế lập:`,
        us: `Within 03 (three) working days from the day on which the tax authority receives the application file for TIN restoration, the tax authority shall prepare:`,
      },
      content_6: {
        vi: ` Thông báo khôi phục mã số thuế mẫu số 19/TB-ĐKT ban hành kèm theo Thông tư này, gửi người nộp thuế;`,
        us: ` Notice of tax code restoration, form No. 19/TB-DKT issued together with this Circular, sent to taxpayers;`,
      },
      content_7: {
        vi: ` In lại Giấy chứng nhận đăng ký thuế hoặc Thông báo mã số thuế cho người nộp thuế trong trường hợp người nộp thuế đã nộp bản gốc cho cơ quan thuế theo hồ sơ chấm dứt hiệu lực mã số thuế`,
        us: ` Re-print the Tax Registration Certificate or Tax Identification Number Notice to the taxpayer in case the taxpayer has submitted the original to the tax authority according to the application for TIN deactivation.`,
      },
      content_8: {
        vi: `MST của NNT sẽ được khôi phục trên hệ thống ngay trong ngày làm việc hoặc chậm nhất là đầu giờ ngày làm việc tiếp theo kể từ ngày ban hành Thông báo khôi phục mã số thuế.`,
        us: `The taxpayer's MST will be restored on the system within a working day or at the latest at the beginning of the next working day from the date of issuance of the Notice of Reinstatement of Tax Identification Number.`,
      },
      content_9: {
        vi: ` 2. Đối với trường hợp NNT không hoạt động tại trụ sở`,
        us: ` 2. In case the taxpayer does not work at the head office`,
      },
      content_10:{
        vi: `Hồ sơ gồm: Văn bản đề nghị khôi phục mã số thuế (Mẫu 25/ĐK-TCT ban hành kèm theo thông tư 95/2016/TT-BTC)`,
        us: `Dossier includes: Written request for tax code restoration (Form 25/DK-TCT issued together with Circular 95/2016/TT-BTC)`
      },
      content_11:{
        vi: `Trong thời hạn 10 ngày làm việc kể từ ngày cơ quan thuế nhận được hồ sơ đề nghị khôi phục mã số thuế:`,
        us: `Within 10 working days from the date the tax authority receives the application file for tax code restoration:`
      },
      content_12:{
        vi: `Cơ quan thuế lập danh sách các hồ sơ kê khai còn thiếu, tình hình sử dụng hóa đơn, số tiền thuế còn nợ, số tiền chậm nộp (nếu có); thực hiện xử phạt đối với các hành vi vi phạm;`,
        us: `The tax authority shall make a list of the missing declaration dossiers, the use of invoices, the tax arrears, the amount of late payment (if any); impose penalties for violations;`
      },
      content_13:{
        vi: `Cơ quan thuế xuống trụ sở để xác minh và lập Biên bản xác minh tình trạng hoạt động của NNT (người nộp thuế phải ký xác nhận vào Biên bản);`,
        us: `The tax authority comes down to the head office to verify and make a Minute to verify the operation status of the taxpayer (the taxpayer must sign the Minute);`
      },
      content_14:{
        vi: `NNT phải thực hiện đầy đủ các nghĩa vụ thuế, yêu cầu của cơ quan thuế theo quy định Pháp luật.`,
        us: `Taxpayers must fulfill all tax obligations and requirements of tax authorities in accordance with the law.`
      },
      content_15: {
        vi: `Trong thời hạn 03 ngày làm việc kể từ ngày NNT chấp hành đầy đủ các nghĩa vụ, cơ quan thuế:`,
        us: `Within 03 working days from the date the taxpayer fully fulfills the obligations, the tax authority:`
      },
      content_16: {
        vi: `Lập Thông báo khôi phục mã số thuế mẫu 19/TB-ĐKT (mẫu ban hành kèm theo thông tư 95/2016/TT-BTC), gửi NNT chậm nhất là đầu giờ ngày làm việc tiếp theo kể từ ngày ban hành Thông báo, đồng thời thực hiện cập nhật trạng thái mã số thuế của người nộp thuế trên Hệ thống ứng dụng đăng ký thuế.`,
        us: `Prepare a Notice of Reinstatement of Tax Identification Number, form 19/TB-DKT (form issued together with Circular 95/2016/TT-BTC), and send it to taxpayers no later than the first hour of the next working day from the date of issuance of the Notice. , and update the taxpayer's tax identification status on the tax registration application system.`
      },
      content_17: {
        vi: `In lại Giấy chứng nhận đăng ký thuế/Thông báo MST cho NNT trong trường hợp NNT đã nộp bản gốc cho cơ quan thuế theo hồ sơ chấm dứt hiệu lực MST.`,
        us: `Reprint the Tax Registration Certificate/MST Notice to the taxpayer in case the taxpayer has submitted the original to the tax authority according to the MST invalidation dossier.`
      },
      content_18: {
        vi: `3. Đối với trường hợp NNT đã nộp hồ sơ xin đóng mã số thuế nhưng cơ quan thuế chưa ban hành Thông báo NNT chấm dứt hiệu lực mã số thuế`,
        us: `3. In case the taxpayer has submitted an application for tax identification but the tax authority has not issued a Notice of termination of the tax identification number.`
      },
      content_19: {
        vi: `Các bước thực hiện trường hợp này giống trường hợp 2, tuy nhiên không phải xác minh trụ sở.`,
        us: `The steps to implement this case are the same as in case 2, but the location is not verified.`
      },
    },
  ] 
  
  ngOnInit(): void {
    
  }
  
  bg_url = `url('assets/images/banner/14718639_852161524919073_849231402605319931_n 1.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, private route: ActivatedRoute) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

}
