export const jobs=[
  {
    id:1,
    img:'cover_Mesa de trabajo 1 1.png',
    name: 'Công ty Phương Linh',
    address: 'Đông Anh, Hà Nội',
    vacancies:'Kế toán thuế',
    salary: 10000000,
    link:'/phuong-linh'
  },
  {
    id:2,
    img:'cover_Mesa de trabajo 1 1.png',
    name: 'Công ty Clink',
    address: 'Thanh Xuân, Hà Nội',
    vacancies:'Kế toán thuế',
    salary: 10000000,
    link:'/clink'
  },
  {
    id:3,
    img:'cover_Mesa de trabajo 1 1.png',
    name: 'Công ty Bầu trời Á Đông',
    address: 'Hoàng Cầu, Đống Đa, Hà Nội',
    vacancies:'Kế toán thuế',
    salary: 10000000,
    link:'/a-dong'
  }
]
