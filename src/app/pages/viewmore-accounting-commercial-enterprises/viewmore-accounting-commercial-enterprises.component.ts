import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { RegisterComponent } from '../viewmore-content/register/register.component';

@Component({
  selector: 'app-viewmore-accounting-commercial-enterprises',
  templateUrl: './viewmore-accounting-commercial-enterprises.component.html',
  styleUrls: ['./viewmore-accounting-commercial-enterprises.component.scss']
})
export class ViewmoreAccountingCommercialEnterprisesComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1, 
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  data_translate: any ={
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
      
      /* 'accountant-calculating-profit-with-financial-analysis-graphs 1.png',
      'business-woman-working-teamwork-process 1.png',
      'working-business-lady 1.png',
      '240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
      'analyzing-statistics 1.png',
      '240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
      '240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png', */
      /* '240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
      'spreadsheet-document-information-financial-startup-concept 1.png' */
      
    
    content_1: [
      {
        title_1: {
          vi: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
          us: 'General accounting in commercial enterprises',
        },
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
        image_1: 'working-business-lady 1.png',
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Người chưa biết gì về kế toán`,
          us: `People who do not know anything about accounting`,
        },
        sub_main_content_1_2:{
          vi: `Người đã học nhưng quên kiến thức`,
          us: `People who have learned but forgotten knowledge`,
        },
        sub_main_content_1_3:{
          vi: `Người đang làm kế toán nhưng chưa lên được BCTC`,
          us: `People who are working as accountants but have not been able to submit financial statements yet`,
        },
        sub_main_content_1_4: {
          vi: `Giám đốc`,
          us: `Manager`
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Giúp người học hiểu được bản chất của kế toán. Nguyên lý hạch toán kế toán trong doanh nghiệp`,
          us: `Help learners understand the nature of accounting. Principles of accounting in business`,
        },
        sub_main_content_2_2: {
          vi: `Biết cách tính - nộp thuế cho đơn vị mình. Nắm được các luật thuế mới nhất `,
          us: `Know how to calculate - pay tax for your unit. Stay up to date with the latest tax laws`,
        },
        sub_main_content_2_3: {
          vi: `Giúp cho kế toán tự tin lên được báo cáo tài chính cho công ty mình. Giúp người học hiểu hơn về bản chất kế toán, các định khoản và nhập liệu và xử lý chứng từ để lên BCTC tại doanh nghiệp thương mại`,
          us: `Help accountants confidently prepare financial statements for their company. Help learners better understand the nature of accounting, terms and data entry and processing of documents to prepare financial statements at commercial enterprises`,
        }
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Sau khi học xong người học tự tin định khoản được nghiệp vụ kế toán trong bất kỳ doanh nghiệp thương mại nào. Và có thể lên được Báo cáo tài chính thủ công tại doanh nghiệp thương mại`,
          us: `After completing the course, students can confidently account for accounting in any commercial enterprise. And it is possible to create manual financial statements at commercial enterprises`,
        },
        sub_main_content_3_2: {
          vi: `Biết cách tính thuế, biết kê khai làm báo cáo thuế và nộp tờ khai cho đơn vị của mình`,
          us: `Know how to calculate tax, know how to declare and make tax reports and submit declarations to your unit`,
        },
        sub_main_content_3_3: {
          vi: `Sau khi học xong người học sẽ thành thạo 1 phần mềm kế toán và có kỹ năng làm các phần mềm khác.`,
          us: `After completing the course, learners will be proficient in 1 accounting software and have skills in making other software.`,
        },
        sub_main_content_3_4: {
          vi: `Kỹ năng làm việc trên excel phục vụ cho công việc kế toán`,
          us: `Skills to work on excel for accounting work`,
        },
        sub_main_content_3_5: {
          vi: `Tự tin lên được báo cáo tài chính tại DN thương mại`,
          us: `Confidence in financial statements at commercial enterprises`,
        }
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Khóa học có 2 giai đoạn :`,
          us: `The course has 2 stages:`,
        },
        sub_main_content_4_2: {
          vi: `Giai đoạn 1: học kiến thức nền tảng.`,
          us: `Stage 1: learn the basic knowledge.`,
        },
        sub_main_content_4_3: {
          vi: `Nội dung gồm :`,
          us: `Content includes:`,
        },
        sub_main_content_4_4: {
          vi: `Hệ thống tài khoản kế toán`,
          us: `System of accounting accounts`,
        },
        sub_main_content_4_5: {
          vi: `Hạch toán kế toán các phần hành :tài sản, nguồn vốn, doanh thu, chi phí, xác định kết quả kinh doanh.`,
          us: `Accounting for operating parts: assets, sources of capital, revenue, expenses, determination of business results.`,
        },
        sub_main_content_4_6: {
          vi: `Lên báo cáo tài chính bằng thủ công`,
          us: `Manually prepare financial statements`,
        },
        sub_main_content_4_7: {
          vi: `Phí môn bài`,
          us: `License fee`,
        },
        sub_main_content_4_8: {
          vi: `Các công việc ở doanh nghiệp mới thành lập`,
          us: `Jobs in start-up businesses`,
        },
        sub_main_content_4_9: {
          vi: `Thuế GTGT (thuế nhà thầu, thuế vãng lai)`,
          us: `GTGT tax (contractor tax, current tax)`,
        },
        sub_main_content_4_10: {
          vi: `Thuế thu nhập cá nhân`,
          us: `Personal income tax`,
        },
        sub_main_content_4_11: {
          vi: `Thuế xuất - nhập khẩu`,
          us: `Import - export tax`,
        },
        sub_main_content_4_12: {
          vi: `Thuế thu nhập doanh nghiệp`,
          us: `Corporate income tax`,
        },
        sub_main_content_4_13: {
          vi: `Phân loại và hạch toán hóa đơn mua vào bán ra ở Doanh nghiệp`,
          us: `Classification and accounting of purchase and sale invoices at the Enterprise`,
        },
        sub_main_content_4_14: {
          vi: `Nhập liệu hóa đơn mua vào, bán ra, chứng từ ngân hàng`,
          us: `Input purchase and sale invoices, bank documents`,
        },
        sub_main_content_4_15: {
          vi: `Cân đối chi phí tiền lương và hạch toán chi phí lương`,
          us: `Balancing salary costs and accounting for salary expenses`,
        },
        sub_main_content_4_16: {
          vi: `Xử lý chứng từ để lên báo cáo tài chính`,
          us: `Processing documents to prepare financial statements`,
        },
        sub_main_content_4_17: {
          vi: `In sổ sách, chứng từ kế toán`,
          us: `Printing accounting books and vouchers`,
        },
        sub_main_content_4_18: {
          vi: `Giai đoạn 2 : thực tập trong phòng kế toán tại doanh nghiệp. Tại đây người học sẽ thực tập thêm các doanh nghiệp trong mảng thương mại. Mỗi người sẽ lên báo cáo tài chính ít nhất 5 công ty.`,
          us: `Stage 2: Internship in the accounting department at the enterprise. Here, learners will practice more businesses in the commercial sector. Each person will report the financial statements of at least 5 companies.`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 40 buổi',
          us: 'Length of study: 40 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 4.000.000 đồng',
          us: 'Tuition: 4,000,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  }
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService,public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit(): void {
  }

}
