import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreAccountingCommercialEnterprisesComponent } from './viewmore-accounting-commercial-enterprises.component';

describe('ViewmoreAccountingCommercialEnterprisesComponent', () => {
  let component: ViewmoreAccountingCommercialEnterprisesComponent;
  let fixture: ComponentFixture<ViewmoreAccountingCommercialEnterprisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreAccountingCommercialEnterprisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreAccountingCommercialEnterprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
