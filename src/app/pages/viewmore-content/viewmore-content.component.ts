import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegisterComponent } from './register/register.component';

@Component({
  selector: 'app-viewmore-content',
  templateUrl: './viewmore-content.component.html',
  styleUrls: ['./viewmore-content.component.scss']
})
export class ViewmoreContentComponent implements OnInit {
  @Input('index') index: any;
  slideConfig2 = { 
    slidesToShow: 3, 
    slidesToScroll: 1, 
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  data_translate: any = {
    parter: [
      {
        title: 'Nguyên lý kế toán',
        img: '../../../assets/images/courses/accountant-calculating-profit-with-financial-analysis-graphs 1.png',
        link: '/nguyen-ly-ke-toan/'
      },
      {
        title: 'Kế toán thuế',
        img: '../../../assets/images/courses/office-with-documents-money-accounts 1.png',
        link: '/ke-toan-thue/'
      },
      {
        title: 'Hướng dẫn lên báo cáo tài chính',
        img: '../../../assets/images/courses/business-woman-working-teamwork-process 1.png',
        link: '/huong-dan-len-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        img: '../../../assets/images/courses/working-business-lady 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
        img: '../../../assets/images/courses/240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
        img: '../../../assets/images/courses/analyzing-statistics 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp xây dựng',
        img: '../../../assets/images/courses/240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung/'
      },
      {
        title: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn',
        img: '../../../assets/images/courses/240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
        link: '/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san/'
      },
      {
        title: 'Đọc hiểu và phân tích báo cáo tài chính',
        img: '../../../assets/images/courses/240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
        link: '/doc-hieu-va-phan-tich-bao-cao-tai-chinh/'
      },
      {
        title: 'Kế toán trưởng',
        img: '../../../assets/images/courses/240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
        link: '/ke-toan-truong/'
      },
      {
        title: 'Quản trị tài chính doanh nghiệp',
        img: '../../../assets/images/courses/spreadsheet-document-information-financial-startup-concept 1.png',
        link: '/quan-tri-tai-chinh-doanh-nghiep/'
      },
    ],
    content_1: [
      {
        title_1: {
          vi: 'Nguyên lý kế toán',
          us: 'Accounting principles',
        },
        image_1: 'accountant-calculating-profit-with-financial-analysis-graphs.png',
        title_2: {
          vi: 'Các khóa học khác',
          us: 'Other courses',
        },
      },
    ],
    sub_content_1: [
      {
        sub_title_1: {
          vi: 'Đối tượng',
          us: 'Object',
        },
        sub_main_content_1: {
          vi: `Người chưa biết gì về kế toán`,
          us: `People who do not know anything about accounting`,
        },
        sub_main_content_1_2:{
          vi: `Người đã học nhưng quên kiến thức`,
          us: `People who have learned but forgotten knowledge`,
        }
      }
    ],
    sub_content_2: [
      {
        sub_title_2: {
          vi: 'Mục đích khóa học',
          us: 'Course purpose',
        },
        sub_main_content_2: {
          vi: `Giúp người học hiểu được bản chất của kế toán. Nguyên lý hạch toán kế toán trong doanh nghiệp`,
          us: `Help learners understand the nature of accounting. Principles of accounting in business`,
        },
      }
    ],
    sub_content_3: [
      {
        sub_title_3: {
          vi: 'Kết quả đạt được',
          us: 'Result achieved',
        },
        sub_main_content_3: {
          vi: `Sau khi học xong người học tự tin định khoản được nghiệp vụ kế toán trong bất kỳ doanh nghiệp nào. Và có thể lên được Báo cáo tài chính thủ công`,
          us: `After completing the course, the learners can confidently determine the accounting profession in any business. And can make Manual financial statements`,
        },
      }
    ],
    sub_content_4: [
      {
        sub_title_4: {
          vi: 'Nội dung khóa học',
          us: 'Course content',
        },
        sub_main_content_4: {
          vi: `Hệ thống tài khoản kế toán`,
          us: `System of accounting accounts`,
        },
        sub_main_content_4_2: {
          vi: `Hạch toán kế toán các phần thành: tài sản, nguồn vốn, doanh thu, chi phí, xác định kết quả kinh doanh `,
          us: `Accounting for the following components: assets, capital sources, revenue, expenses, determination of business results`,
        },
        sub_main_content_4_3: {
          vi: `Lên báo cáo tài chính bằng thủ công`,
          us: `Manually prepare financial statements`,
        },
      }
    ],
    sub_content_5: [
      {
        sub_title_5: {
          vi: 'Thời lượng học : 7 buổi',
          us: 'Length of study: 7 sessions',
        },
      }
    ],
    sub_content_6: [
      {
        sub_title_6: {
          vi: 'Học phí : 1.000.000 đồng',
          us: 'Tuition: 1,000,000 VND',
        },
      }
    ],
    button: {
      vi: 'ĐĂNG KÝ NGAY',
      us: 'REGISTER NOW',
    }
  };

  openDialog(){
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public slide = '0%';
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService, public dialog: MatDialog) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
    setInterval(() => {
      if (this.index < this.data_translate.parter.length / 4) {
        this.slide = this.index * 100 + '%';
        this.index++;
      }else{
        this.index = 0;
        this.slide = this.index * 100 + '%';
      }
    }, 3000);
  }

  ngOnInit(): void {
  }
}
