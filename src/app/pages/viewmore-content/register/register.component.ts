import { Component, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  translate_data:any = {
    register_now: {
      vi: 'ĐĂNG KÝ KHÓA HỌC',
      us: 'REGISTER THE COURSE',
    },
    name: {
      vi: 'Họ và tên:',
      us: 'First and last name:',
    },
    email: {
      vi: 'Email:',
      us: 'Email:',
    },
    phone: {
      vi: 'Số điện thoại:',
      us: 'Phone number:',
    },
    course_register: {
      vi: 'Khóa học đăng ký:',
      us: 'Registration course:',
    },
    option_1: {
      vi: 'Nguyên lý kế toán',
      us: 'Accounting principles',
    },
    option_2: {
      vi: 'Kế toán thuế',
      us: 'Tax accounting',
    },
    option_3: {
      vi: 'Hướng dẫn lên báo cáo tài chính',
      us: 'Guide to preparing financial statements',
    },
    option_4: {
      vi: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
      us: 'General accounting in commercial enterprises',
    },
    option_5: {
      vi: 'Kế toán tổng hợp trong doanh nghiệp sản xuất',
      us: 'General accounting in manufacturing enterprises' ,
    },
    option_6: {
      vi: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ',
      us: 'General accounting in service enterprises',
    },
    register: {
      vi: 'ĐĂNG KÝ',
      us: 'SIGN UP'
    }
  }

  language_name = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

  ngOnInit(): void {
  }

}
