import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreContentComponent } from './viewmore-content.component';

describe('ViewmoreContentComponent', () => {
  let component: ViewmoreContentComponent;
  let fixture: ComponentFixture<ViewmoreContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
