import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  submited: boolean = false;
  is_error: boolean = false;
  returnURL: any;
  formg: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router:Router,
    private route:ActivatedRoute,
    private authenticationService: AuthenticationService,
  ) {
    this.formg = fb.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    })
  }
  get loginFormValue(){
    return this.formg.value;
  }

  ngOnInit(): void {
    if(this.authenticationService.userValue)
    {
      this.router.navigate(['/login'])
    }
    else{
      let returnURL_ =  this.route.snapshot.queryParamMap.get('returnURL') 
      this.returnURL = returnURL_ ? returnURL_ : "admin/news-management"
    }
  }

  login(){
    this.submited = true
    this.authenticationService.login(this.loginFormValue).pipe(first()).subscribe({
      next: (data: any) => {
        if(data){
          this.submited = true;
          this.router.navigateByUrl(this.returnURL)
          this.is_error = false;
          console.log(data);
        }
      },
      error: (error: HttpErrorResponse) => {
        this.submited = false;
        console.log(error)
        this.is_error = true;
      }
    })
  }
  ViewPassword(is_view_password: boolean){
    if(is_view_password){
      (document.getElementById('view_password') as HTMLInputElement).style.display = "block";
      (document.getElementById('hide_password') as HTMLInputElement).style.display = "none";
      (document.getElementById('password_input') as HTMLInputElement).type = "text";
    }else{
      (document.getElementById('view_password') as HTMLInputElement).style.display = "none";
      (document.getElementById('hide_password') as HTMLInputElement).style.display = "block";
      (document.getElementById('password_input') as HTMLInputElement).type = "password"
    }
  }
}
