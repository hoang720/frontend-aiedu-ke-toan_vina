

export const list: any = {
  base: [
    {
      img: 'accountant-calculating-profit-with-financial-analysis-graphs 1.png',
      title: { vi: 'Nguyên lý kế toán', us: 'Accounting principles' },
      content: {
        vi: `Khóa học giúp người học biết cách đọc hiểu và phân tích báo cáo tài chính cho doanh nghiệp và áp dụng đọc hiểu kiểm tra cho chính doanh nghiệp mình.`,
        us: `The course helps learners know how to read and analyze financial statements for businesses and apply reading comprehension tests to their own businesses.`,
      },
      link:'/nguyen-ly-ke-toan',
    },
    {
      img: 'office-with-documents-money-accounts 1.png',
      title: {
        vi: 'Kế toán thuế',
        us: 'Tax accounting',
      },
      content: {
        vi: `Giúp người học nắm vững luật thuế - kế toán. Từ đó biết cách cân đối tối ưu thuế, kê khai và làm các tờ khai quyết toán, báo cáo thuế và tự tin trong việc giải trình với cơ quan thuế.`,
        us: `Help learners master tax law - accounting. From there, know how to optimally balance taxes, declare and make finalization declarations, tax reports and be confident in explaining to the tax authorities.`,
      },
      link:'/ke-toan-thue',
    },
    {
      img: 'business-woman-working-teamwork-process 1.png',
      title: {
        vi: 'Hướng dẫn lên báo cáo tài chính',
        us: 'Guide to preparing financial statements',
      },
      content: {
        vi: `Khóa học giúp bạn tự tin lên được báo cáo tài chính, biết cách kiểm tra, cân đối chi phí tiền lương, các chi phí hợp lý. In sổ sách, chứng từ kế toán.`,
        us: `The course helps you confidently prepare financial statements, know how to check and balance salary costs and reasonable expenses. Print books and accounting vouchers.`,
      },
      link:'/huong-dan-len-bao-cao-tai-chinh',
    },
    {
      img: 'working-business-lady 1.png',
      title: {
        vi: 'Kế toán tổng hợp trong doanh nghiệp thương mại',
        us: 'General accounting in commercial enterprises',
      },
      content: {
        vi: `Khóa học giúp bạn nắm được nguyên lý cơ bản trong hạch toán kế toán tại doanh nghiệp thương mại. Cập nhật luật thuế, các kê khai và tính các loại thuế phải nộp và lên được báo cáo tài chính, làm tờ khai quyết toán ở DN Thương mại.`,
        us: `The course helps you to understand the basic principles of accounting in commercial enterprises. Updating tax laws, declaring and calculating payable taxes and making financial statements, making finalization declarations at commercial enterprises.`,
      },
      link:'/ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai',
    },
    {
      img: '240_F_155102843_P2X09BiFv9j3s7ci2TaQQwhAkT0cFO5U 1.png',
      title: { 
        vi: 'Kế toán tổng hợp trong doanh nghiệp sản xuất', 
        us: 'General accounting in manufacturing enterprises' 
      },
      content: {
        vi: `Khóa học giúp bạn nắm được nguyên lý cơ bản trong hạch toán kế toán tại doanh nghiệp sản xuất. Cập nhật luật thuế, các kê khai và tính các loại thuế phải nộp và lên được báo cáo tài chính, làm tờ khai quyết toán ở DN sản xuất. `,
        us: `The course helps you to understand the basic principles of accounting in manufacturing enterprises. Updating tax laws, declaring and calculating payable taxes and making financial statements, making finalization declarations in manufacturing enterprises.`,
      },
      link:'/ke-toan-tong-hop-trong-doanh-nghiep-san-xuat',
    },
    {
      img: 'analyzing-statistics 1.png',
      title: { 
        vi: 'Kế toán tổng hợp trong doanh nghiệp dịch vụ', 
        us: 'General accounting in service enterprises' 
      },
      content: {
        vi: `Khóa học giúp bạn nắm được nguyên lý cơ bản trong hạch toán kế toán tại doanh nghiệp dịch vụ. Cập nhật luật thuế, các kê khai và tính các loại thuế phải nộp và lên được báo cáo tài chính, làm tờ khai quyết toán ở DN dịch vụ.`,
        us: `The course helps you to understand the basic principles of accounting in a service business. Update tax laws, declare and calculate payable taxes and prepare financial statements, make finalization declarations at service enterprises.`,
      },
      link:'/ke-toan-tong-hop-trong-doanh-nghiep-dich-vu',
    },
    {
      img: '240_F_197510722_QQ7gKhqH9ycGuf2U4INvPHhnsasbPHjg 1.png',
      title: { 
        vi: 'Kế toán tổng hợp trong doanh nghiệp xây dựng', 
        us: 'General accounting in construction enterprises' 
      },
      content: {
        vi: `Khóa học giúp bạn nắm được nguyên lý cơ bản trong hạch toán kế toán tại doanh nghiệp xây dựng. Cập nhật luật thuế, các kê khai và tính các loại thuế phải nộp và lên được báo cáo tài chính, làm tờ khai quyết toán ở DN xây dựng.`,
        us: `The course helps you to understand the basic principles of accounting in a construction business. Updating tax laws, declaring and calculating payable taxes and making financial statements, making finalization declarations in construction enterprises.`,
      },
      link:'/ke-toan-tong-hop-trong-doanh-nghiep-xay-dung',
    },
    {
      img: '240_F_190614996_Uyh5au1m06aDoSn18mGB4O7yEsNCX1Vs 1.png',
      title: { 
        vi: 'Kế toán tổng hợp trong doanh nghiệp nhà hàng - khách sạn', 
        us: 'General accounting in the restaurant - hotel enterprises' 
      },
      content: {
        vi: `Khóa học giúp bạn nắm được nguyên lý cơ bản trong hạch toán kế toán tại doanh nghiệp nhà hàng - khách sạn. Cập nhật luật thuế, các kê khai và tính các loại thuế phải nộp và lên được báo cáo tài chính, làm tờ khai quyết toán ở DN nhà hàng - khách sạn.`,
        us: `The course helps you to understand the basic principles of accounting in the restaurant - hotel business. Update tax laws, declare and calculate payable taxes and make financial statements, make finalization declarations at restaurant - hotel businesses.`,
      },
      link:'/ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san',
    },
  ],
  advance: [
    {
      img: '240_F_170702420_S0imon3ugwQ912OJSUzYEphQeIIWYaKq 1.png',
      title: { 
        vi: 'Đọc hiểu và phân tích báo cáo tài chính', 
        us: 'Reading comprehension and analyzing financial statements' 
      },
      content: {
        vi: `Khóa học giúp người học biết cách đọc hiểu và phân tích báo cáo tài chính cho doanh nghiệp và áp dụng đọc hiểu kiểm tra cho chính doanh nghiệp mình.`,
        us: `The course helps learners know how to read and analyze financial statements for businesses and apply reading comprehension tests to their own businesses.`,
      },
      link:'/doc-hieu-va-phan-tich-bao-cao-tai-chinh',
    },
    {
      img: '240_F_99871299_m5VLbIWxiCmnBwbOG0t5nkmPHbXpmWXO 1.png',
      title: { 
        vi: 'Kế toán trưởng', 
        us: 'Chief accountant' 
      },
      content: {
        vi: `Khóa học giúp kế toán trưởng trang bị những kiến thức và kỹ năng cần thiết phục vụ cho công việc chuyên môn của mình.`,
        us: `The course helps the chief accountant to equip the necessary knowledge and skills for his professional work.`,
      },
      link:'/ke-toan-truong',
    },
    {
      img: 'spreadsheet-document-information-financial-startup-concept 1.png',
      title: { 
        vi: 'Quản trị tài chính doanh nghiệp', 
        us: 'Enterprise Financial Management' 
      },
      content: {
        vi: `Khóa học giúp CEO, chủ doanh nghiệp có thêm kiến thức về mặt tài chính. Giúp quản trị tài chính doanh nghiệp của mình hiệu quả hơn.`,
        us: `The course helps CEOs and business owners gain more financial knowledge. Help manage your business finances more effectively.`,
      },
      link:'/quan-tri-tai-chinh-doanh-nghiep',
    },
  ],
  base_1: [
    {img: '14718639_852161524919073_849231402605319931_n 1.png'}
  ],
  advance_1: [
    {img: '14606500_560308877495222_5923641216333351837_n 1.png'}
  ]
};
