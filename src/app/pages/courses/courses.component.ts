import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from './list';

@Component({
  selector: 'app-courses',
  styleUrls: ['./courses.component.scss'],
  templateUrl: './courses.component.html',
})
export class CoursesComponent {
  
  selectedTab = 'base';
  listContent = list;
  bg_url = `url('assets/images/banner/banner.png')`;
  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
