import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-media-content',
  templateUrl: './media-content.component.html',
  styleUrls: ['./media-content.component.scss']
})
export class MediaContentComponent implements OnInit {
  @Input('index') index: any;
  @Input('item') item:any;
  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ngOnInit(): void {
  }

}
