import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from '../source';

@Component({
  selector: 'app-tt113',
  templateUrl: './tt113.component.html',
  styleUrls: ['./tt113.component.scss']
})
export class Tt113Component implements OnInit {

  selectedTab = 'Specialized_knowledge';
  listContent = list;
  system_vn: any =[
    {
      content_1: {
        vi: `Căn cứ Nghị định số 215/2013/NĐ-CP ngày 13 tháng 12 năm 2013 của Chính phủ quy định chức năng, nhiệm vụ, quyền hạn và cơ cấu tổ chức của Bộ Tài chính;`,
        us: `Pursuant to Decree No. 215/2013/ND-CP dated December 13, 2013 of the Government defining the functions, tasks, powers and organizational structure of the Ministry of Finance;`
      },
      content_2: {
        vi: `Thông tư này hướng dẫn nguyên tắc ghi sổ kế toán, lập và trình bày Báo cáo tài chính của doanh nghiệp nhỏ và vừa, không áp dụng cho việc xác định nghĩa vụ thuế của doanh nghiệp đối với ngân sách Nhà nước.`,
        us: `This Circular guides the principles of bookkeeping, preparation and presentation of financial statements of small and medium-sized enterprises, not applicable to the determination of tax obligations of enterprises to the State budget.`
      },
      content_3: {
        vi: `Thông tư này áp dụng đối với các doanh nghiệp nhỏ và vừa (bao gồm cả doanh nghiệp siêu nhỏ) thuộc mọi lĩnh vực, mọi thành phần kinh tế theo quy định của pháp luật về hỗ trợ doanh nghiệp nhỏ và vừa trừ doanh nghiệp Nhà nước, doanh nghiệp do Nhà nước sở hữu trên 50% vốn điều lệ, công ty đại chúng theo quy định của pháp luật về chứng khoán, các hợp tác xã, liên hiệp hợp tác xã theo quy định tại Luật Hợp tác xã.`,
        us: `This Circular applies to small and medium enterprises (including micro enterprises) in all fields and all economic sectors in accordance with the law on supporting small and medium enterprises except State enterprises. countries, enterprises with more than 50% of charter capital owned by the State, public companies in accordance with the law on securities, cooperatives and unions of cooperatives as prescribed in the Law on Cooperatives.`
      },
      content_4: {
        vi: `Doanh nghiệp có quy mô nhỏ và vừa thuộc lĩnh vực đặc thù như điện lực, dầu khí, bảo hiểm, chứng khoán ... đã được Bộ Tài chính ban hành hoặc chấp thuận áp dụng chế độ kế toán đặc thù.`,
        us: `Small and medium-sized enterprises in specific fields such as electricity, oil and gas, insurance, securities ... have been issued or approved by the Ministry of Finance to apply specific accounting regimes.`
      },
    }
  ]

  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }


  ngOnInit(): void {
  }

}
