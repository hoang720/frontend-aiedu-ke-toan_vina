import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Tt113Component } from './tt113.component';

describe('Tt113Component', () => {
  let component: Tt113Component;
  let fixture: ComponentFixture<Tt113Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Tt113Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Tt113Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
