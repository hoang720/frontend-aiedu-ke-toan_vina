import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreSystemComponent } from './viewmore-system.component';

describe('ViewmoreSystemComponent', () => {
  let component: ViewmoreSystemComponent;
  let fixture: ComponentFixture<ViewmoreSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreSystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
