import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from '../source';


@Component({
  selector: 'app-viewmore-system',
  templateUrl: './viewmore-system.component.html',
  styleUrls: ['./viewmore-system.component.scss']
})
export class ViewmoreSystemComponent implements OnInit {
  selectedTab = 'Specialized_knowledge';
  listContent = list;
  system_vn: any =[
    {
      content_1: {
        vi: `Hệ thống chuẩn mực kế toán Việt Nam quy định và hướng dẫn các nguyên tắc và yêu cầu kế toán, các yếu tố và ghi nhận các yếu tố trên báo cáo tài chính của doanh nghiệp, nhằm:`,
        us: `The Vietnamese accounting standard system prescribes and guides the principles and requirements of accounting, elements and recognition of elements in the financial statements of enterprises, in order to:`
      },
      content_2: {
        vi: `Giúp cho doanh nghiệp ghi chép kế toán và lập báo cáo tài chính một cách thống nhất và xử lý các vấn đề chưa được quy định cụ thể nhằm đảm bảo cho các thông tin trên báo cáo tài chính phản ánh trung thực và hợp lý;`,
        us: `Help enterprises make accounting records and prepare financial statements in a uniform manner and handle issues that have not been specified in order to ensure that the information on the financial statements reflects truthfully and reasonably;`
      },
    },
  ]

  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ngOnInit(): void {
  }

}
