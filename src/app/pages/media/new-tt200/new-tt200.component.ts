import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from '../source';

@Component({
  selector: 'app-new-tt200',
  templateUrl: './new-tt200.component.html',
  styleUrls: ['./new-tt200.component.scss']
})
export class NewTT200Component implements OnInit {

  selectedTab = 'Specialized_knowledge';
  listContent = list;
  system_vn: any =[
    {
      content_1: {
        vi: `Các doanh nghiệp có nghiệp vụ thu, chi chủ yếu bằng ngoại tệ, đáp ứng đủ các tiêu chuẩn tại Điều 4 Thông tư này sẽ được chọn một loại ngoại tệ làm đơn vị tiền tệ để ghi sổ kế toán.`,
        us: `Enterprises whose revenue and expenditure are mainly in foreign currencies and fully satisfy the criteria in Article 4 of this Circular will be allowed to choose a foreign currency as the currency unit to record in their accounting books.`
      },
      content_2: {
        vi: `Doanh nghiệp sử dụng ngoại tệ làm đơn vị tiền tệ trong kế toán thì đồng thời với lập Báo cáo tài chính (BCTC) theo ngoại tệ còn phải chuyển đổi BCTC sang Đồng Việt Nam.`,
        us: `Enterprises using foreign currency as the currency in accounting, at the same time as preparing financial statements in foreign currency, must convert financial statements into Vietnam Dong.`
      },
      content_3: {
        vi: `BCTC mang tính pháp lý để công bố ra công chúng và nộp các cơ quan có thẩm quyền tại Việt Nam là BBTC được trình bày bằng Đồng Việt Nam.`,
        us: `Legal financial statements to be disclosed to the public and submitted to the competent authorities in Vietnam are BBTC presented in Vietnam Dong.`
      },
      content_4: {
        vi: `Việc thay đổi đơn vị tiền tệ trong kế toán nếu không thỏa các tiêu chuẩn tại Điều 4 Thông tư này sẽ chỉ được thực hiện tại thời điểm bắt đầu niên độ kế toán mới.`,
        us: `The change in the accounting currency if the criteria in Article 4 of this Circular are not satisfied will only be made at the beginning of the new accounting year.`
      },
      content_5: {
        vi: `Phần đặc điểm hoạt động của doanh nghiệp bổ sung thêm: chu kỳ sản xuất, kinh doanh thông thường; cấu trúc doanh nghiệp.`,
        us: `The enterprise's operating characteristics added: normal production and business cycle; enterprise structure.`
      },
      content_6: {
        vi: `Phần chính sách kế toán áp dụng chia ra chỉ tiêu cụ thể cho 2 trường hợp Doanh nghiệp đáp ứng giả định hoạt động liên tục và Doanh nghiệp không.`,
        us: `The applied accounting policy section divides specific criteria for two cases where the enterprise meets the going concern assumption and the enterprise does not.`
      },
      content_7: {
        vi: `Phần thông tin bổ sung cho các khoản mục trình bày trong bảng Cân đối kế toán thêm chỉ tiêu: nợ xấu; vay và nợ thuê tài chính; tài sản dở dang dài hạn…`,
        us: `The additional information for items presented in the Balance Sheet adds the following criteria: bad debt; finance lease loans and debts; long-term unfinished assets…`
      },
      content_8: {
        vi: `Doanh nghiệp được chủ động xây dưng, thiết kế biểu mẫu chứng từ kế toán nhưng phải đáp ứng được các yêu cầu của Luật Kế toán và đảm bảo rõ ràng, minh bạch.`,
        us: `Enterprises are allowed to actively build and design accounting voucher forms but must meet the requirements of the Law on Accounting and ensure clarity and transparency.`
      },
      content_9: {
        vi: `Các loại chứng từ kế toán tại Phục lục 3 Thông tư chỉ mang tính hướng dẫn. Trường hợp doanh nghiệp không tự xây dựng, thiết kế cho riêng mình được thì có thể áp dụng theo Phụ lục 3.`,
        us: `Types of accounting vouchers in Appendix 3 to this Circular are for guidance only. In case enterprises cannot build or design their own, they can apply according to Appendix 3.`
      },
      content_10: {
        vi: `Doanh nghiệp được tự xây dựng biểu mẫu sổ kế toán cho riêng mình nhưng phải đảm bảo cung cấp thông tin về giao dịch kinh tế một cách minh bạch, đầy đủ. Trường hợp không tự xây dựng có thể áp dụng biểu mẫu sổ kế toán theo phụ lục 4 Thông tư.`,
        us: `Enterprises are allowed to build their own forms of accounting books but must ensure to provide information on economic transactions in a transparent and complete manner. In case of not self-built, the accounting book form according to Appendix 4 of the Circular can be applied.`
      },
      
    },
  ]

  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ngOnInit(): void {
  }

}
