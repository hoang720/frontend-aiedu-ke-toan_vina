import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTT200Component } from './new-tt200.component';

describe('NewTT200Component', () => {
  let component: NewTT200Component;
  let fixture: ComponentFixture<NewTT200Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTT200Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTT200Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
