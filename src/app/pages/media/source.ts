export const list: any = {
    Specialized_knowledge: [
      {
          content:{
              vi: `HỆ THỐNG CHUẨN MỰC KẾ TOÁN VIỆT NAM`,
              us: `VIETNAM ACCOUNTING STANDARDS SYSTEM`
          },
          link: '/he-thong-chuan-muc-ke-toan-viet-nam/'
      },
      {
          content: {
              vi: `DANH SÁCH CÁC CHUẨN MỰC KẾ TOÁN`,
              us: `LIST OF ACCOUNTING STANDARDS`
          },
          link: ''
      },
      {
          content: {
              vi: `Những điểm mới của TT200/2014/TT-BTC`,
              us: `New features of TT200/2014/TT-BTC`
          },
          link: '/nhung-diem-moi-cua-tt200/'
      },
      {
          content: {
              vi: `Những nội dung mới trong thông tư 200 so với quyết định 15`,
              us: `New contents in Circular 200 compared to Decision 15`
          },
          link: '/nhung-noi-dung-moi-trong-tt200/'
      },
      {
          content: {
              vi: `THÔNG TƯ 113 - HƯỚNG DẪN CHẾ ĐỘ KẾ TOÁN DOANH NGHIỆP NHỎ VÀ VỪA`,
              us: `CIRCULARS 113 - INSTRUCTIONS ON THE ACCOUNTING REGULATION OF SMALL AND MEDIUM ENTERPRISE`
          },
          link: '/thong-tu-113/'
      },
      {
          content: {
              vi: `Những điểm mới trong của Bộ luật Lao động`,
              us: `New points in the Labor Code`
          },
          link: '/nhung-diem-moi-cua-bo-luat-lao-dong/'
      },
    ],
    Work_experience: [
      {
          content: {
              vi: `ĐĂNG KÝ TÀI KHOẢN NỘP BẢO HIỂM ĐIỆN TỬ`,
              us: `SIGN UP FOR ELECTRONIC INSURANCE ACCOUNT`
          },
          link: ''
      },
      {
          content: {
              vi: `LỆCH THUẾ TRÊN TỜ KHAI VÀ BÁO CÁO TÀI CHÍNH`,
              us: `TAX DIFFERENCES ON FINANCIAL STATEMENTS AND STATEMENTS`
          },
          link: ''
      },
      {
          content: {
              vi: `XỬ LÝ DOANH NGHIỆP MỞ MÃ SỐ THUẾ`,
              us: `HANDLING OF ENTERPRISE OPENING TAX CODE`
          },
          link: ''
      },
      {
          content: {
              vi: `HƯỚNG DẪN KIỂM TRA VÀ HOÀN THIỆN BÁO CÁO TÀI CHÍNH`,
              us: `INSTRUCTIONS FOR INSPECTION AND COMPLETION OF FINANCIAL STATEMENTS`
          },
          link: ''
      },
      {
          content: {
              vi: `CÔNG VIỆC CỦA KẾ TOÁN TẠI DOANH NGHIỆP CHUYỂN ĐỊA CHỈ CÙNG QUẬN`,
              us: `ACCOUNTANT'S WORK AT SAME ENTERPRISE IN THE COUNTRY`
          },
          link: ''
      },
      {
          content: {
              vi: `CÔNG VIỆC CỦA KẾ TOÁN TẠI DOANH NGHIỆP CHUYỂN ĐỊA CHỈ KHÁC QUẬN`,
              us: `ACCOUNTANT'S JOB AT DIFFERENT ADDRESS TRANSFER ENTERPRISE`
          },
          link: ''
      },
    ],
    download:[

    ]
}
