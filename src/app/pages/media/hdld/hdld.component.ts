import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from '../source';

@Component({
  selector: 'app-hdld',
  templateUrl: './hdld.component.html',
  styleUrls: ['./hdld.component.scss']
})
export class HDLDComponent implements OnInit {

  selectedTab = 'Specialized_knowledge';
  listContent = list;
  system_vn: any =[
    {
      content_1: {
        vi: `Bộ luật Lao động (sửa đổi) đã được Quốc hội khoá XIII thông qua ngày 18/6/2012, bắt đầu có hiệu lực từ 1/5/2013. Bộ luật lao động (sửa đổi) gồm 17 chương, 242 điều, tăng 19 điều so với Bộ luật Lao động hiện hành (223 điều). Bộ luật Lao động (sửa đổi) đã đưa ra những quy định cụ thể hơn và bảo vệ lợi ích cho người lao động (NLĐ) nhiều hơn. Careerlink xin giới thiệu 5 nội dung mới đáng chú ý mà NLĐ và Doanh nghiệp cần biết trong Bộ luật mới này.`,
        us: `The Labor Code (amended) was approved by the 13th National Assembly on June 18, 2012, and took effect from May 1, 2013. The Labor Code (amended) includes 17 chapters, 242 articles, an increase of 19 articles compared to the current Labor Code (223 articles). The Labor Code (amended) has made more specific regulations and protected the interests of employees (employees). Careerlink would like to introduce 5 notable new contents that employees and businesses need to know in this new Code.`
      },
      content_2: {
        vi: `Chương III (điều 26, 27, 31) quy định: tiền lương trong thời gian thử việc của NLĐ “ít nhất phải bằng 85%” so với mức 70% của quy định theo Bộ luật đang hiện hành. Một điểm mới được bổ sung trong chương này đó là: Cho thuê lao động, đây là lần đầu tiên quy định này chính thức được công nhận tại Việt nam.`,
        us: `Chapter III (articles 26, 27, 31) stipulates: the salary during the probationary period of an employee "must be at least 85%" compared with 70% of the regulations under the current Code. A new point added in this chapter is: Labor hire, this is the first time this regulation has been officially recognized in Vietnam.`
      },
      content_3: {
        vi: `Chương VI, điều 93 và 94 Bộ luật Lao động đang hiện hành quy định: Người sử dụng lao động (NSDLĐ) phải đăng ký thang, bảng lương với cơ quan quản lý Nhà nước và phải được duyệt trước khi thực hiện trong doanh nghiệp. Trong Bộ luật (sửa đổi) áp dụng ngày 1/5/2013 đã bãi bỏ hình thức phải đăng ký thang, bảng lương thay bằng việc NSDLĐ chỉ cần sao gửi thang, bảng lương cho cơ quan, tổ chức có liên quan để quản lý, theo dõi. Điều này có nghĩa Nhà nước không can thiệp trực tiếp vào mức tiền lương của người lao động, chỉ quy định mức tiền lương thấp nhất mà NSDLĐ phải trả cho NLĐ …. Thang lương, bản lương, định mức lao động do các doanh nghiệp tự xây dựng và ban hành theo nguyên tắc quy định của Chính phủ. Trường hợp thay đổi hình thức trả lương phải thông báo cho NLĐ biết trước 10 ngày.`,
        us: `Chapter VI, Articles 93 and 94 of the Current Labor Code stipulates: Employers must register salary scales and tables with the State management agency and must be approved before implementing in enterprises. . In the (amended) Code applied on May 1, 2013, the form of having to register salary scales and payrolls was abolished, instead, employers only need to copy and send salary scales and tables to relevant agencies and organizations for management. management, monitoring. This means that the State does not directly intervene in the wages of employees, only stipulating the lowest salary that the employer must pay to the employee…. The salary scale, salary sheet, and labor norms are built and promulgated by enterprises themselves according to the principles prescribed by the Government. In case of changing the form of salary payment, the employee must be notified 10 days in advance.`
      },
      content_4: {
        vi: `Những sửa đổi của chương này dựa trên nguyên tắc: Nhà nước không can thiệp trực tiếp vào mức tiền lương của NLĐ, chỉ quy định mức lương thấp nhất mà NSDLĐ phải trả cho NLĐ. Đồng thời quy định rõ các mức lương tối thiểu được xác định theo ngày, tháng, giờ và được xác lập theo vùng ngành như: Điều kiện sống tối thiểu của người lao động và gia đình họ, điều kiện kinh tế - xã hội và mức tiền lương trên thị trường lao động.`,
        us: `The amendments of this chapter are based on the principle that the State does not directly intervene in the wages of employees, only stipulating the lowest salary that the employer must pay to the employees. At the same time, it is clear that minimum wages are determined by day, month, and hour and are established by industry sector such as: Minimum living conditions of workers and their families, socio-economic conditions and wages in the labor market.`
      },
      content_5: {
        vi: `Một điểm mới trong chương VI là việc quy định về Hội đồng tiền lương Quốc gia: “Hội đồng tiền lương quốc gia là cơ quan tư vấn cho Chính phủ, bao gồm các thành viên là đại diện của Bộ Lao động – thương binh và Xã hội, Tổng Liên đoàn Lao động Việt Nam và tổ chức đại diện người sử dụng lao động ở Trung Ương. Chính phủ được giao quy định cụ thể chức năng, nhiệm vụ và cơ cấu tổ chức của hội đồng này” - (khoản 1, điều 92).`,
        us: `A new point in Chapter VI is the regulation of the National Wage Council: “The National Wage Council is an advisory body for the Government, including members who are representatives of the Ministry of Labor, War Invalids and Social Affairs. Society, Vietnam General Confederation of Labor and the representative organization of employers at the central level. The Government is assigned to specify the functions, tasks and organizational structure of this council” - (Clause 1, Article 92).`
      },
      content_6: {
        vi: `Ngoài những quy định về giờ làm việc và nghỉ ngơi cho NLĐ như: Bảo đảm số giờ làm thêm của người lao động không quá 50% số giờ làm việc chính thức trong 01 ngày, trong trường hợp áp dụng quy định làm việc theo tuần thì tổng số giờ làm việc bình thường và số giờ làm thêm không quá 12 giờ trong 01 ngày; không quá 30 giờ trong 01 tháng và tổng số không quá 200 giờ trong 01 năm, trừ một số trường hợp đặc biệt do Chính phủ quy định thì được làm thêm giờ không quá 300 giờ trong 01 năm. Chương VII, Bộ luật còn quy định NLĐ được nghỉ 10 ngày/năm hưởng nguyên lương, trong đó tết âm lịch được nghỉ 5 ngày, tăng một ngày so với luật đang hiện hành (nghỉ 9 ngày/năm hưởng lương nguyên, trong đó tết âm lịch được nghỉ 4 ngày).`,
        us: `In addition to the regulations on working hours and rest for employees such as: Ensuring that the employee's overtime hours do not exceed 50% of the official working hours in a day, in the case of applying the regulations on weekly work. then the total number of normal working hours and overtime hours must not exceed 12 hours in 01 day; not exceeding 30 hours in a month and a total of not more than 200 hours in a year, except for some special cases prescribed by the Government, they are allowed to work overtime for no more than 300 hours in a year. Chapter VII of the Code also stipulates that employees are entitled to 10 days off/year with full salary, of which 5 days off for the Lunar New Year, an increase of one day compared to the current law (9 days off/year with full salary, of which Lunar New Year holiday is 4 days).`
      },
      content_7: {
        vi: `Ngoài ra, trong chương này bổ sung thêm quy định để NLĐ được nghỉ trong một số trường hợp cụ thể như bố, mẹ hoặc anh, chị em ruột chết: “Người lao động được nghỉ không hưởng lương 01 ngày và phải thông báo với người sử dụng lao động khi ông nội, bà nội, ông ngoại, bà ngoại hoặc anh, chị, em ruột chết; bố hoặc mẹ kết hôn; anh, chị, em ruột kết hôn (khoản 2 Điều 116). Riêng đối với lao động là người nước ngoài được nghỉ thêm một ngày tết cổ truyền và ngày quốc khánh của nước họ.`,
        us: `In addition, in this chapter, there are additional regulations for employees to take leave in some specific cases such as the death of a parent or sibling: “The employee is entitled to 01 day of unpaid leave and must notify the employer. the employer when the grandfather, grandmother, grandfather, grandmother or brother, sister or younger brother dies; married parent; siblings get married (Clause 2, Article 116). Particularly for foreign workers, they are entitled to one more traditional New Year day and national day of their country.`
      },
      content_8: {
        vi: `Theo Chương X: Những quy định riêng đối với lao động nữ: Lao động nữ được nghỉ trước và sau khi sinh con 6 tháng, thay vì 4 tháng như hiện nay. Trường hợp LĐ nữ sinh đôi trở lên thì tính từ con thứ 2 trở đi, cứ mỗi con, người mẹ được nghỉ thêm 1 tháng. Thời gian nghỉ trước khi sinh tối đa không quá 2 tháng. Trong đó, nếu đến ngày 1/5/2013 mà lao động nữ đang trong thời gian nghỉ thai sản sẽ được hưởng chế độ theo BLLĐ mới.`,
        us: `According to Chapter X: Specific regulations for female employees: Female employees are entitled to 6 months of leave before and after giving birth, instead of 4 months as at present. In case female employees give birth to twins or more, from the second child onwards, for each child, the mother is entitled to an extra month of leave. The maximum period of maternity leave must not exceed 2 months. In particular, if by May 1, 2013, female employees who are on maternity leave will be entitled to benefits according to the new Labor Code.`
      },
      content_9: {
        vi: `Mục đích của quy định này là: đưa ra các quy định nhằm thúc đẩy bình đẳng giới trong tuyển dụng, sử dụng, đào tạo, thời giờ làm việc, thời giờ nghỉ ngơi, tiền lương và các chế độ khác; pháp điển hoá Nghị định (là việc xây dựng, soạn thảo một văn bản pháp luật mới trên cơ sở hệ rà soát, hệ thống hoá, tập hợp các quy định ở nhiều văn bản quy phạm pháp luật hiện hành vào văn bản đó với sự sửa đổi, bổ sung, điều chỉnh để phù hợp với thực tiễn) 23-CP ngày 18/4/1996 của Chính phủ quy định chi tiết và hướng dẫn thi hành một số điều của Bộ luật Lao động về những quy định riêng đối với lao động nữ.`,
        us: `The purpose of this regulation is: to introduce regulations to promote gender equality in recruitment, employment, training, working hours, rest time, wages and other regimes; Decree codification (is the formulation and drafting of a new legal document on the basis of a system of review, systematization and collection of provisions in many existing legal documents into that document with amendments, supplements, adjustments to suit reality) 23-CP dated 18/4/1996 of the Government detailing and guiding the implementation of a number of articles of the Labor Code on specific regulations for female workers.`
      },
      content_10: {
        vi: `Về tuổi nghỉ hưu, Bộ luật vẫn giữ nguyên quy định độ tuổi nghỉ hưu của lao động nam là 60 tuổi, nữ 55 tuổi. Trong chương XII, khoản 1, điều 187, Bộ luật cho phép Chính phủ quy định cụ thể tuổi nghỉ hưu đối với các nhóm lao động bị suy giảm khả năng lao động, làm các công việc đặc biệt nặng nhọc, độc hại, nguy hiểm; làm việc ở vùng cao, vùng sâu, vùng xa, biên giới, hải đảo; nhóm lao động có trình độ chuyên môn kỹ thuật cao, nhóm lao động làm công tác quản lý và một số trường hợp đặc biệt khác, làm cơ sở để trong tương lai điều chỉnh tổng thể tuổi nghỉ hưu. Đối với NLĐ có trình độ chuyên môn kỹ thuật cao, NLĐ làm công tác quản lý và một số trường hợp đặc biệt khác có thể nghỉ hưu ở tuổi cao hơn nhưng không quá 05 năm so với quy định tại.`,
        us: `Regarding retirement age, the Code still stipulates that the retirement age for male employees is 60 years old and 55 years old for female employees. In Chapter XII, Clause 1, Article 187 of the Code, the Government allows the Government to specify the retirement age for groups of workers with reduced working capacity, doing particularly heavy, hazardous and dangerous jobs. dangerous; working in highland, deep-lying, remote, border and island areas; groups of workers with high professional and technical qualifications, management workers and some other special cases, as a basis for the overall adjustment of the retirement age in the future. For employees with high professional and technical qualifications, employees working in management and some other special cases, they can retire at an older age but not more than 05 years compared to the provisions in Clause 1 of this Article.`
      },
      content_11: {
        vi: `Ngoài một số quy định đã được sửa đổi, bộ luật lao động mới có thêm một số điều mới như: Các trường hợp hợp đồng lao động vô hiệu; đối thoại doanh nghiệp; thỏa ước lao động tập thể ngành. Bên cạnh đó, luật còn bổ sung thêm những quy định mới nhằm bảo vệ quyền lợi cho NLĐ giúp việc nhà, người lao động không trọn thời gian. Các chính sách về BHXH, BHYT, BHTN… cũng được chỉnh sửa cụ thể hơn, nhằm bảo vệ quyền lợi cho NLĐ trong các trường hợp rủi ro như: Ốm đau, tai nạn, mất việc…`,
        us: `In addition to some revised regulations, the new labor code has some new things such as: Cases of invalid labor contracts; business dialogue; industry collective bargaining agreement. In addition, the law also adds new regulations to protect the interests of domestic workers and part-time workers. The policies on social insurance, health insurance, unemployment insurance ... have also been revised more specifically, in order to protect the interests of employees in case of risks such as sickness, accident, job loss...`
      },
      content_12: {
        vi: `Bộ luật Lao động (sửa đổi) đã được Quốc hội khoá XIII thông qua ngày 18 tháng 6 năm 2012, có hiệu lực từ 1/5/2013. Bộ luật lao động (sửa đổi) gồm 17 chương, 242 điều, tăng 19 điều so với Bộ luật Lao động hiện hành (223 điều). Sau đây là một số nội dung mới của Luật này.`,
        us: `The Labor Code (amended) was approved by the 13th National Assembly on June 18, 2012, and took effect from May 1, 2013. The Labor Code (amended) includes 17 chapters, 242 articles, an increase of 19 articles compared to the current Labor Code (223 articles). Here are some new contents of this Law.`
      },
      content_13: {
        vi: `Chương này cơ bản vẫn giữ nguyên phạm vi điều chỉnh và đối tượng áp dụng. Tuy nhiên, Chương I bổ sung thêm Điều 3 giải thích từ ngữ, trong đó đưa ra khái niệm về quan hệ lao động “ là quan hệ xã hội phát sinh trong việc thuê mướn, sử dụng, trả lương giữa người lao động và người sử dụng lao động” (khoản 6 Điều 3) và Cưỡng bức lao động là “việc dùng vũ lực, đe dọa dùng vũ lực hoặc các thủ đoạn khác nhằm buộc người khác lao động trái ý muốn của họ” (khoản 10 Điều 3).`,
        us: `This chapter basically retains the scope of regulation and the object of application. However, Chapter I adds Article 3 to explain the wording, which introduces the concept of labor relationship "is the social relationship arising in the hiring, employing and paying wages between employees and employees". Use of labor” (Clause 6, Article 3) and Forced labor is “the use of force, threat of force or other tricks to force others to work against their will” (Clause 10, Article 3). ).`
      },
      content_14: {
        vi: `Một điểm mới trong Chương I là việc ghi nhận quyền đóng cửa tạm thời doanh nghiệp, quyền gia nhập hoạt động trong tổ chức nghề nghiệp của người sử dụng lao động; bổ sung các hành vi bị nghiêm cấm cho phù hợp với các qui định của Công ước quốc tế về lao động mà Việt Nam đã phê chuẩn như cấm lao động cưỡng bức.`,
        us: `A new point in Chapter I is the recognition of the employer's right to temporarily close an enterprise and join activities in a professional organization; supplementing the prohibited acts to be consistent with the provisions of the International Labor Convention that Vietnam has ratified such as the prohibition of forced labor.`
      },
      content_15: {
        vi: `Quy định về việc làm, giải quyết việc làm, quyền làm việc của người lao động, quyền tuyển chọn lao động của người sử dụng lao động, chính sách của Nhà nước hỗ trợ phát triển việc làm và tổ chức dịch vụ việc làm. Điểm mới của chương này là bổ sung thêm các quy định về trách nhiệm của người sử dụng lao động trong việc tham gia giải quyết việc làm`,
        us: `Regulations on employment, job creation, employee's right to work, employer's right to choose workers, State policies on job development support and employment service organization. A new feature of this chapter is to add regulations on the responsibility of employers in participating in job creation.`
      },
      content_16: {
        vi: `Về mức thời gian của hợp đồng lao động xác định thời hạn Điểm b, khoản 1, Điều 22 quy định: “Hợp đồng lao động xác định thời hạn là hợp đồng mà trong đó hai bên xác định thời hạn, thời điểm chấm dứt hiệu lực của hợp đồng trong khoảng thời gian từ đủ 12 tháng đến 36 tháng.”.`,
        us: `Regarding the duration of a definite-term labor contract, Point b, Clause 1, Article 22 stipulates: "A definite-term labor contract is a contract in which the two parties determine the term and the time of termination. validity of the contract for a period from full 12 months to 36 months”.`
      },
      content_17: {
        vi: `Chương III đã quy định cụ thể những hành vi mà người sử dụng lao động không được làm khi gia kết, thực hiện hợp đồng lao động; các trường hợp chấm dứt hợp đồng lao động; cụ thể hoá cách tính trợ cấp thôi việc, trợ cấp mất việc làm; bỏ Quỹ dự phòng mất việc làm.
        Một điểm mới nữa trong Chương III là việc qui định tăng mức tiền lương của người lao động trong thời gian thử việc “ít nhất phải bằng 85% ” so với 70% của qui định của Bộ luật Lao động hiện hành.`,
        us: `Chapter III specifically stipulates the acts that the employer is not allowed to do when entering into and performing the labor contract; cases of termination of labor contracts; specifying the calculation of severance allowance and job loss allowance; leave the Job Loss Reserve Fund.
        Another new point in Chapter III is the regulation that increases the salary of employees during the probationary period "must be at least 85%" compared with 70% of the provisions of the current Labor Code.`
      },
      content_18: {
        vi: `Chương này được sửa đổi theo hướng: quy định cụ thể hơn về tăng cường thực hiện quy chế dân chủ, tại nơi làm việc để bảo đảm quá trình thực hiện đối thoại, thương lượng, đồng thời cũng làm căn cứ để Chính phủ ban hành văn bản hướng dẫn thực hiện quy chế dân chủ trong các loại hình doanh nghiệp, hợp tác xã; thúc đẩy và phát huy tối đa sự thoả thuận, thương lượng để xác lập điều kiện lao động mới .`,
        us: `This chapter is revised in the direction of: more specific regulations on strengthening the implementation of democratic regulations at the workplace to ensure the implementation of dialogue and negotiation process, and at the same time serve as a basis for the Government to issue decisions. promulgate documents guiding the implementation of democratic regulations in various types of enterprises and cooperatives; promote and maximize the agreement and negotiation to establish new working conditions.`
      },
      content_19: {
        vi: `Đặc biệt, Chương V đã quy định 1 mục mới (Mục IV) về thỏa ước lao động tập thể ngành để thiết lập và thống nhất hóa những tiêu chuẩn lao động và trách nhiệm xã hội chủ yếu, cải thiện những điều kiện lao động có thể áp dụng chung trong ngành, đặc biệt là thương lượng và thỏa thuận về mức lương tối thiểu ngành mà Chính phủ không qui định. Nếu có thỏa ước ngành, các doanh nghiệp trong ngành sẽ phải áp dụng và chỉ khi có điều kiện thuận lợi hơn mới cần thương lượng và ký thỏa thuận doanh nghiệp.`,
        us: `In particular, Chapter V stipulates a new section (Section IV) on industry collective bargaining agreements to establish and unify key labor standards and social responsibilities, improve working conditions. can be applied in general in the industry, especially in negotiating and agreeing on industry minimum wages that are not regulated by the Government. If there is an industry agreement, enterprises in the industry will have to apply and only when there are more favorable conditions do they need to negotiate and sign an enterprise agreement.`
      },
      content_20: {
        vi: `Những sửa đổi của chương này dựa trên các nguyên tắc như Nhà nước không can thiệp trực tiếp vào mức tiền lương của người lao động, chỉ quy định mức tiền lương thấp nhất mà Người sử dụng lao động phải trả người lao động; bình đẳng về tiền lương giữa các loại hình doanh nghiệp…`,
        us: `The amendments to this chapter are based on principles such as that the State does not directly intervene in the wages of workers, only stipulating the lowest wage that the Employer must pay the employee; equality in wages between types of businesses…`
      },
      content_21: {
        vi: `Chương VI quy định rõ các loại mức lương tối thiểu gồm: “Mức lương tối thiểu được xác định theo tháng, ngày, giờ và được xác lập theo vùng, ngành” (Điều 91); quy định các yếu tố xác định mức lương tối thiểu: “nhu cầu sống tối thiểu của người lao động và gia đình họ, điều kiện kinh tế - xã hội và mức tiền lương trên thị trường lao động”(Điều 91).`,
        us: `Chapter VI clearly stipulates different types of minimum wages, including: “Minimum wages are determined by month, day, and hour and by region and industry” (Article 91); stipulates the factors determining the minimum wage: “minimum living needs of workers and their families, socio-economic conditions and wages in the labor market” (Article 91).`
      },
      content_22: {
        vi: `Đặc biệt, Chương VI bỏ quy định yêu cầu người sử dụng lao động phải đăng ký  thang, bảng lương, thay bằng sao gửi cho cơ quan, tổ chức có liên quan. Thang lương, bản lương, định mức lao động do các doanh nghiệp tự xây dựng và ban hành theo nguyên tắc qui định của Chính phủ.`,
        us: `In particular, Chapter VI removes the requirement that the employer must register the salary scale and payroll, replacing it with a copy and sending it to relevant agencies and organizations. The salary scale, salary sheet, and labor norms are built and promulgated by enterprises themselves according to the principles prescribed by the Government.`
      },
      content_23: {
        vi: `Một điểm mới trong Chương VI là việc quy định về Hội đồng tiền lương quốc gia: “Hội đồng tiền lương quốc gia là cơ quan tư vấn cho Chính phủ, bao gồm các thành viên là đại diện của Bộ Lao động - Thương binh và Xã hội, Tổng Liên đoàn Lao động Việt Nam và tổ chức đại diện người sử dụng lao động ở Trung ương” (Khoản 1, Điều 92)`,
        us: `A new point in Chapter VI is the regulation of the National Wage Council: “The National Wage Council is an advisory body for the Government, including members who are representatives of the Ministry of Labor, War Invalids and Social Affairs. Society, Vietnam General Confederation of Labor and the central representative organization of employers” (Clause 1, Article 92)`
      },
      content_24: {
        vi: `Bảo đảm số giờ làm thêm của người lao động không quá 50% số giờ làm việc chính thức trong 01 ngày, trong trường hợp áp dụng quy định làm việc theo tuần thì tổng số giờ làm việc bình thường và số giờ làm thêm không quá 12 giờ trong 01 ngày; không quá 30 giờ trong 01 tháng và tổng số không quá 200 giờ trong 01 năm, trừ một số trường hợp đặc biệt do Chính phủ quy định thì được làm thêm giờ không quá 300 giờ trong 01 năm;`,
        us: `Ensure that the number of overtime hours worked by employees does not exceed 50% of the official working hours in a day. In case of applying the regulation on working week, the total number of normal working hours and overtime hours must not exceed 50% of the official working hours. 12 hours in 01 day; no more than 30 hours in a month and a total of not more than 200 hours in a year, except for some special cases prescribed by the Government, who may work overtime for no more than 300 hours in a year;`
      },
      content_25: {
        vi: `Chương VI làm rõ mức lương làm thêm giờ vào ngày lễ là chưa kể tiền lương ngày lễ, ngày nghỉ có lương đối với người lao động hưởng lương ngày; qui định tăng mức lương làm thêm giờ vào ban đêm: “ Người lao động làm thêm giờ vào ban đêm thì ngoài việc trả lương theo quy định tại khoản 1 và khoản 2 Điều này, người lao động còn được trả thêm 20% tiền lương tính theo đơn giá tiền lương hoặc tiền lương theo công việc làm vào ban ngày.” (Khoản 3 Điều 97).`,
        us: `Chapter VI clarifies that overtime wages on public holidays are exclusive of wages for holidays and paid days off for employees receiving daily wages; Regulations on increasing the rate of overtime work at night: “The employee who works overtime at night, in addition to the salary specified in Clauses 1 and 2 of this Article, the employee is also paid an additional 20% of the salary. calculated according to the wage unit price or salary according to the work done during the day.” (Clause 3, Article 97).`
      },
      content_26: {
        vi: `Chương VII qui định tăng thời gian nghỉ tết cổ truyền từ 4 ngày lên 5 ngày; bổ sung quy định để người lao động được nghỉ trong một số trường hợp cụ thể như bố, mẹ hoặc anh, chị em ruột chết: “Người lao động được nghỉ không hưởng lương 01 ngày và phải thông báo với người sử dụng lao động khi ông nội, bà nội, ông ngoại, bà ngoại hoặc anh, chị, em ruột chết; bố hoặc mẹ kết hôn; anh, chị, em ruột kết hôn (2 Điều 116).`,
        us: `Chapter VII stipulates increasing the traditional Tet holiday from 4 days to 5 days; supplementing regulations for employees to take leave in some specific cases such as the death of a parent or sibling: “Employees are entitled to 01 day of unpaid leave and must notify the employer when a grandfather, grandmother, grandfather, grandmother or brother, sister or brother dies; married parent; siblings get married (2 Article 116).`
      },
      content_27: {
        vi: `Việc xử lý kỷ luật lao động có ý nghĩa hết sức quan trọng và ảnh hưởng trực tiếp đến quyền lợi của người lao động. Nếu không được qui định chặt chẽ, việc người lao động lạm dụng công cụ xử lý kỷ luật lao động là rất dễ xảy ra. Vi vậy, Chương VIII bổ sung những quy định cấm về xử lý kỷ luật lao động; nguyên tắc xử lý bồi thường thiệt hại.`,
        us: `The handling of labor discipline is very important and directly affects the interests of employees. If not strictly regulated, it is very easy for employees to abuse tools to handle labor discipline. Therefore, Chapter VIII adds prohibitions on labor discipline; principles of compensation for damage.`
      },
      content_28: {
        vi: `Điểm mới đáng chú ý của Chương XIII là việc đưa ra cơ chế bảo vê cán bộ công đoàn thông qua các qui định về quyền của cán bộ công đoàn, việc chấm dứt hợp đồng lao động, các hành vi nghiêm cấm và trách nhiệm của người sử dụng lao động.`,
        us: `A notable new feature of Chapter XIII is the introduction of a mechanism to protect trade union officials through provisions on the rights of trade union officials, termination of labor contracts, prohibited acts and responsibilities of trade union officials employer.`
      },
      content_29: {
        vi: `Một điểm mới đáng chú ý của Chương VIII là việc qui định vai trò, trách nhiệm của Công đoàn cấp trên cơ có trách nhiệm hỗ trợ công đoàn cơ sở; việc thực hiện trách nhiệm đại diện, bảo vệ quyền và lợi ích hợp pháp, chính đáng của đoàn viên công đoàn, người lao động ở những nơi chưa thành lập tổ chức công đoàn cơ sở.`,
        us: `A remarkable new point of Chapter VIII is the regulation of the roles and responsibilities of the upper-level trade union, which is responsible for supporting the grassroots trade union; the performance of responsibility to represent and protect the legitimate rights and interests of trade union members and workers in places where grassroots trade unions have not been established.`
      },
      content_30: {
        vi: `Chương XIII còn qui định Công đoàn cấp trên cơ sở có quyền và trách nhiệm vận động người lao động gia nhập công đoàn, thành lập công đoàn cơ sở tại doanh nghiệp, cơ quan, tổ chức; có quyền yêu cầu người sử dụng lao động và cơ quan quản lý nhà nước về lao động địa phương tạo điều kiện và hỗ trợ việc thành lập công đoàn cơ sở.`,
        us: `Chapter XIII also stipulates that the grassroots trade unions have the right and responsibility to mobilize employees to join trade unions and establish grassroots trade unions at enterprises, agencies and organizations; has the right to request employers and local state management agencies in charge of labor to facilitate and support the establishment of grassroots trade unions.`
      },
      content_31: {
        vi: `Bỏ qui định về Hội đồng hoà giải cơ sở, thay bằng Hoà giải viên lao động cấp huyện giải quyết. Điều này xuất phát từ thực tế nhiều doanh nghiệp chưa thành lập được công đoàn thì sẽ không thành lập được Hội đồng hòa giải lao động cơ sở. Mặt khác, nơi có Hội đồng hòa giải lao động cơ sở, thì phần lớn hoạt động không hiệu quả.`,
        us: `Remove the provisions on the grassroots conciliation council, and replace it with a district-level labor mediator to settle. This comes from the fact that many businesses that have not yet established a union will not be able to establish a grassroots Labor Reconciliation Council. On the other hand, where there is a grassroots Labor Reconciliation Council, it is largely ineffective.`
      },
      content_32: {
        vi: `Đối với các tranh chấp lao động tập thể về quyền (tranh chấp lao động trong những trường hợp người sử dụng lao động không thực hiện những nghĩa vụ pháp luật qui định đối với tập thể lao động thì tập thể người lao động không được phép đình công, mà phải đâu tranh bằng phương pháp khiếu nại hoặc khiếu kiện theo qui định của pháp luật. Khi có tranh chấp tập thể về quyền, hai bê không tự giải quyết thì yêu cầu cơ quan có thẩm quyền (hòa giải viên cấp huyện, Chủ tịch UBND cấp huyện hoặc Tòa án để giải quyết, yêu cầu người sử dụng lao động phải thực hiện đúng qui định của pháp luật lao động.`,
        us: `For collective labor disputes over rights (labor disputes in cases where the employer fails to fulfill the legal obligations prescribed for the labor collective, the employee collective is not allowed to If there is a collective dispute over rights, the two parties cannot resolve it by themselves, they must request a competent authority (a conciliator issued by a mediator) district, district-level People's Committee chairpersons or Courts to resolve and request the employer to strictly comply with the provisions of the labor law.`
      },
      content_33: {
        vi: `Chương XIV cho phép người sử dụng lao động được tạm thời đóng cửa doanh nghiệp trong quá trình tập thể lao động tiến hành đình công để bảo vệ tài sản của doanh nghiệp. Tuy nhiên, việc tạm thời đóng cửa doanh nghiệp chỉ được phép thực hiện sau thời điểm cuộc đình công bắt đầu, người sử dụng lao động phải trả lương cho người lao động không tham gia đình công, không được đến làm việc và phải mở cửa doanh nghiệp trở lại ngay khi cuộc đình công kết thúc, tập thể lao động trở lại làm việc.`,
        us: `Chapter XIV allows employers to temporarily close businesses during a collective strike to protect the assets of the enterprise. However, the temporary closure of businesses is only allowed after the start of the strike, the employer must pay wages to the employees who do not participate in the strike, cannot come to work and must open The business doors will be reopened as soon as the strike ends, the labor collective will return to work.`
      },
      content_34: {
        vi: `Một điểm mới trong Chương XIV là việc sửa đổi quy trình đình công như thay đổi tỷ lệ người lao động đồng ý đình công…. theo hướng đơn giản hoá trình tự, thủ tục giải quyết tranh chấp lao động; bỏ chủ thể “Đại diện tập thể lao động” ở nơi chưa có công đoàn được quyền lãnh đạo đình công.`,
        us: `A new point in Chapter XIV is the modification of the strike process such as changing the percentage of workers agreeing to strike…. towards simplifying the order and procedures for settling labor disputes; remove the subject "Representative of the collective labor" where there is no trade union entitled to lead a strike.`
      },
    }
  ]

  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ngOnInit(): void {
  }

}
