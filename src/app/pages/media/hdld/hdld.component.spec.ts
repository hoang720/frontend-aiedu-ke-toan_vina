import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HDLDComponent } from './hdld.component';

describe('HDLDComponent', () => {
  let component: HDLDComponent;
  let fixture: ComponentFixture<HDLDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HDLDComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HDLDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
