import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFeaturesTt200Component } from './new-features-tt200.component';

describe('NewFeaturesTt200Component', () => {
  let component: NewFeaturesTt200Component;
  let fixture: ComponentFixture<NewFeaturesTt200Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewFeaturesTt200Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFeaturesTt200Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
