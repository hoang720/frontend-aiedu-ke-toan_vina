import { Component, Input, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from '../source';

@Component({
  selector: 'app-new-features-tt200',
  templateUrl: './new-features-tt200.component.html',
  styleUrls: ['./new-features-tt200.component.scss']
})
export class NewFeaturesTt200Component implements OnInit {

  selectedTab = 'Specialized_knowledge';
  listContent = list;
  system_vn: any =[
    {
      content_1: {
        vi: `Hệ thống mẫu sổ kế toán và chứng từ kế toán không bắt buộc phải tuân thủ, doanh nghiệp có thể tự thiết kế mẫu sổ kế toán, chứng từ kế toán cho phù hợp với từng doanh nghiệp. Tuy nhiên, Bộ Tài chính vẫn ban hành mẫu sổ và chứng từ kế toán để mang tính chất hướng dẫn doanh nghiệp nếu doanh nghiệp không tự thiết kế mẫu sổ và chứng từ cho riêng mình.`,
        us: `The system of accounting book templates and accounting vouchers is not required to comply, enterprises can design their own accounting book and voucher templates to suit each enterprise. However, the Ministry of Finance still issues the form of accounting books and vouchers to guide enterprises if the enterprises do not design their own forms of books and vouchers.`
      },
      content_2: {
        vi: `Rất nhiều nghiệp vụ hạch toán thay đổi: Thay đổi số hiệu TK hạch toán, thay đổi quan điểm ghi nhận giao dịch. Ví dụ bổ một số TK 129, 139, 142, 144, 223, 311, 315, 342, 415, 512, 531, 532, … đổi tên TK 344, thêm TK 1534, 1557, …, một số tên gọi tài khoản thay đổi, khi hạch toán không quan niệm TK ngắn hạn hay dài hạn, chỉ khi lập BCTC mới quan tâm đến tài sản ngắn hạn và dài hạn.`,
        us: `Many accounting operations changed: Change the number of the accounting account, change the view of recording transactions. For example, add account number 129, 139, 142, 144, 223, 311, 315, 342, 415, 512, 531, 532, ... rename account 344, add account 1534, 1557, ..., some account names changes, when accounting does not consider short-term or long-term accounts, only when preparing financial statements will attention to short-term and long-term assets.`
      },
      content_3: {
        vi: `Tất cả DN đều được dùng ngoại tệ làm đơn vị tiền tệ trong kế toán (nếu thoả điều kiện quy định) nhưng phải lập BCTC chuyển đổi theo VNĐ, kiểm toán BCTC VNĐ và sử dụng BCTC VNĐ là Báo cáo pháp lý (trước đây chỉ DN FDI được sử dụng ngoại tệ là đơn vị tiền tệ trong kế toán và BCTC bằng ngoại tệ được kiểm toán cũng là BCTC mang tính pháp lý).`,
        us: `All enterprises are allowed to use foreign currency as their accounting currency (if they meet the prescribed conditions) but must prepare the converted financial statements in VND, audit the VND financial statements and use the VND financial statements as the legal report (previously). Only FDI enterprises are allowed to use foreign currency as the currency in accounting and the audited foreign currency financial statements are also legal financial statements).`
      },
      content_4: {
        vi: `DN phải ghi nhận Doanh thu, giá vốn điều chuyển sản phẩm, hàng hoá, dịch vụ nội bộ nếu việc điều chuyển này tạo ra giá trị gia tăng trong sản phẩm, hàng hoá, dịch vụ KHÔNG PHỤ THUỘC VÀO HÌNH THỨC CỦA CHỨNG TỪ KẾ TOÁN (Hoá đơn hay Chứng từ luân chuyển nội bộ).`,
        us: `Enterprises must record Revenue, cost of internal transfer of products, goods and services if this transfer creates added value in products, goods and services that do not depend on the FORM OF THE DOCUMENT ACCOUNTING (Invoices or Internal Transfer Documents).`
      },
      content_5: {
        vi: `Trường hợp giải thể công ty con và sáp nhập toàn bộ TS và Nợ phải trả của công ty con vào công ty mẹ, phải ghi giảm giá trị ghi sổ khoản đầu tư; Ghi nhận toàn bộ tài sản, nợ phải trả của công ty con bị giải thể theo giá trị hợp lý tại ngày sáp nhập. Phần chênh lệch giữa giá phí khoản đầu tư và giá trị hợp lý được ghi nhận vào Doanh thu TC hoặc Chi phí TC.`,
        us: `In case of dissolution of a subsidiary and merger of all assets and liabilities of the subsidiary into the parent company, a decrease in the carrying amount of the investment must be made; All assets and liabilities of the dissolved subsidiary are recorded at their fair value at the merger date. The difference between the cost of the investment and the fair value is recognized in TC Revenue or TC Expense.`
      },
      content_6: {
        vi: `Nếu BCC quy định các bên khác tham gia BCC được hưởng một khoản lợi nhuận cố định mà không phụ thuộc vào KQKD của hợp đồng, trường hợp này mặc dù hình thức pháp lý của hợp đồng là BCC nhưng bản chất của hợp đồng là thuê tài sản.=> phải ghi nhận theo bản chất là Thuê tài sản`,
        us: `If BCC stipulates that other parties participating in BCC are entitled to a fixed profit that does not depend on the business results of the contract, in this case, although the legal form of the contract is BCC, the essence of the contract is to lease Assets.=> must be recognized by its nature as Leases`
      },
      content_7: {
        vi: `Dự phòng tổn thất đầu tư vào đơn vị khác: Là khoản dự phòng tổn thất do DN nhận vốn góp đầu tư bị lỗ. Đối với khoản đầu tư vào công ty liên doanh, liên kết, chỉ trích lập khoản dự phòng BCTC không áp dụng phương pháp VCSH.`,
        us: `Provision for loss of investment in other entities: is the provision for loss because the enterprise receiving investment capital suffers a loss. For investments in joint ventures and associates, only provision is made for financial statements that do not apply the equity method.`
      },
      content_8: {
        vi: `Khi mua TSCĐ nếu được nhận kèm thêm thiết bị, phụ tùng thay thế thì phải xác định và ghi nhận riêng sản phẩm, phụ tùng thiết bị thay thế theo giá trị hợp lý.Nguyên giá mua TSCĐ mua được xác định bằng tổng giá trị của tài sản được mua trừ đi giá trị sản phẩm, thiết bị, phụ tùng thay thế.`,
        us: `When purchasing fixed assets, if they are received with additional equipment and spare parts, they must identify and record separately the replacement products, spare parts and equipment according to their fair value. The original purchase price of purchased fixed assets is determined by the total cost. the value of the property purchased minus the value of the product, equipment, or spare parts.`
      },
      content_9: {
        vi: `Chủ đầu tư xây dựng BĐS sử dụng tài khoản 241 để tập hợp chi phí xây dựng BĐS sử dụng cho nhiều mục đích. Khi công trình, dự án hoàn thành bàn giao đưa vào sử dụng, kế toán căn cứ cách thức sử dụng tài sản trong thực tế để kết chuyển chi phí đầu tư xây dựng phù hợp với bản chất của từng loại.`,
        us: `Real estate investors use account 241 to collect expenses for real estate construction used for many purposes. When the works or projects are completed, handed over and put into use, the accountants shall base on the actual use of assets to transfer construction investment costs in accordance with the nature of each type.`
      },
      content_10: {
        vi: `Xác định phần vốn góp của nhà đầu tư bằng ngoại tệ: khi giấy phép đầu tư quy định vốn điều lệ của DN được xác định bằng ngoại tệ tương đương với một số lượng tiền VND, việc xác định phần vốn góp của nhà đầu tư bằng ngoại tệ (thừa, thiếu, đủ so với vốn điều lệ) được căn cứ vào SỐ LƯỢNG NGOẠI TỆ ĐÃ THỰC GÓP, không xem xét tới việc quy đổi ngoại tệ ra VNĐ theo giấy phép đầu tư.`,
        us: `Determination of the investor's capital contribution in foreign currency: when the investment license stipulates that the charter capital of the enterprise is determined in foreign currency equivalent to an amount of VND, the determination of the contributed capital of the investor in foreign currency (excess, insufficient, sufficient compared to the charter capital) is based on the AMOUNT OF FOREIGN CURRENCY PAYED, regardless of the conversion of foreign currency into VND according to the investment license.`
      },
      content_11: {
        vi: `Doanh thu và chi phí tạo ra khoản doanh thu đó phải được ghi nhận đồng thời theo nguyên tắc phù hợp. Trong 1 số T.Hợp, nguyên tắc phù hợp có thể xung đột với nguyên tắc thận trọng trong kế toán, thì kế toán phải căn cứ vào bản chất và các Chuẩn mực kế toán để phản ánh giao dịch một cách trung thực, hợp lý.`,
        us: `Revenue and costs of generating such revenue must be recognized simultaneously on the matching principle. In some cases, the conformity principle may conflict with the prudential principle in accounting, the accountant must base on the nature and accounting standards to reflect the transaction honestly and legally. physical.`
      },
      content_12: {
        vi: `Nhấn mạnh: Thời điểm, căn cứ để ghi nhận doanh thu kế toán và doanh thu tính thuế có thể khác nhau tuỳ vào từng tình huống cụ thuể. Doanh thu tính thuế chỉ được sử dụng để xác định số thuế phải nộp theo luật định; Doanh thu ghi nhận trên sổ kế toán để lập BCTC phải tuân thủ các nguyên tắc kế toán và tuỳ theo từng trường hợp không nhất thiết phải bằng số đã ghi trên hoá đơn bán hàng.`,
        us: `Emphasis: The time and basis for recognizing accounting revenue and taxable revenue may be different depending on each specific situation. Taxable turnover is only used to determine the amount of tax payable according to the law; The revenue recorded in accounting books to prepare financial statements must comply with accounting principles and, depending on each case, not necessarily equal to the amount recorded on the sales invoice.`
      },
      content_13: {
        vi: `Nếu sản phẩm, HH, DV đã tiêu thụ từ các kỳ trước, đến kỳ sau phải giảm giá, phải chiết khấu thương mại, bị trả lại nhưng phát sinh trước thời điểm phát hành BCTC phải coi đây là một sự kiện cần điều chỉnh, phát sinh sau ngày lập bảng CĐKT.`,
        us: `If products, goods, and services that have been sold from previous periods, have to be reduced in price, have to be discounted in the next period, or returned, but arise before the time of issuance of financial statements, this must be considered as an event that needs to be adjusted. adjustments, arising after the balance sheet date.`
      },
      content_14: {
        vi: `Các khoản chi phí không được coi là chi phí tính thuế TNDN theo quy định của Luật thuế nhưng có đầy đủ hoá đơn chứng từ và đã hạch toán đúng theo Chế độ kế toán toán thì không được ghi giảm chi phí kế toán mà chỉ điều chỉnh trong quyết toán thuế TNDN để làm tăng số thuế TNDN phải nộp.`,
        us: `Expenses that are not considered as CIT calculation expenses according to the provisions of the Tax Law but have sufficient invoices and vouchers and have been properly accounted for according to the accounting regime shall not be recorded as a reduction in accounting expenses but only adjusted. adjusted in the CIT finalization to increase the payable CIT amount.`
      }
    },
  ]

  language_name:any;
  constructor(private languageService: LanguageChangeSharingService) {
    this.languageService.getLanguageObservable().subscribe(res => {
      this.language_name = res;
    })
   
  }

  ngOnInit(): void {
  }

}
