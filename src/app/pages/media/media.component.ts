import { Component, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { list } from './source'

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
  selectedTab = 'Specialized_knowledge';
  listContent = list;

  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }

  ngOnInit(): void {
    console.log(this.listContent)
  }
}
