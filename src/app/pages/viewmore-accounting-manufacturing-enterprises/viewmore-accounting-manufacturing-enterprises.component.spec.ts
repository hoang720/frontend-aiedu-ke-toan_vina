import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreAccountingManufacturingEnterprisesComponent } from './viewmore-accounting-manufacturing-enterprises.component';

describe('ViewmoreAccountingManufacturingEnterprisesComponent', () => {
  let component: ViewmoreAccountingManufacturingEnterprisesComponent;
  let fixture: ComponentFixture<ViewmoreAccountingManufacturingEnterprisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreAccountingManufacturingEnterprisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreAccountingManufacturingEnterprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
