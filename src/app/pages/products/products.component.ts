import { Component, OnInit } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { jobs } from '../data/jobs';
import { Observable } from 'rxjs';
import { JobManagementService } from '@core/services/job-management.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './products.component.html',
  styleUrls: ['products.component.scss'],
})
export class ProductComponent  implements OnInit{
  company:any;
  job_list: Observable<any> = new Observable<any>();
  ngOnInit(): void {
    this.job_list = this.getJob()
  }

  language_name: string = 'vi';
  constructor(
    private LanguageService: LanguageChangeSharingService,
    private jobService: JobManagementService,
    private router: Router,

    ) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
  formhttp(link: any){
    return link.replace('http://45.77.245.61:6868/',"https://ketoanvina.com.vn/image/")
  }
  getJob(){
    return this.jobService.getJob()
  }
  redirectToPage(page_name: any, job_name: any){
    this.router.navigateByUrl(`/${page_name}/${job_name}`);
  }
}
