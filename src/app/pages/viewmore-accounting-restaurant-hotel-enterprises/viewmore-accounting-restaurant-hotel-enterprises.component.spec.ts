import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmoreAccountingRestaurantHotelEnterprisesComponent } from './viewmore-accounting-restaurant-hotel-enterprises.component';

describe('ViewmoreAccountingRestaurantHotelEnterprisesComponent', () => {
  let component: ViewmoreAccountingRestaurantHotelEnterprisesComponent;
  let fixture: ComponentFixture<ViewmoreAccountingRestaurantHotelEnterprisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmoreAccountingRestaurantHotelEnterprisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmoreAccountingRestaurantHotelEnterprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
