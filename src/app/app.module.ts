import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { appInitializer } from '@core/initializer/app.initializer';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { FootersComponent } from '@features/footers/footers.component';
import { HeadersComponent } from '@features/headers/headers.component';
import { LayoutsComponent } from '@features/layouts/layouts.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductComponent } from './pages/products/products.component';
import { ResearchComponent } from './pages/research/research.component';
import { ContentComponent } from './pages/content/content.component';
import { SlideComponent } from './pages/slide/slide.component';
import { ResearchContentComponent } from './pages/research-content/research-content.component';


import { MediaComponent } from './pages/media/media.component';
import { MatCardModule} from '@angular/material/card';
import { MatCarouselModule } from 'ng-mat-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductContentComponent } from './pages/product-content/product-content.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ChatboxComponent } from './pages/chatbox/chatbox.component';
import { ViewmoreContentComponent } from './pages/viewmore-content/viewmore-content.component';
import { RegisterComponent } from './pages/viewmore-content/register/register.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { ViewmoreTaxAccountingComponent } from './pages/viewmore-tax-accounting/viewmore-tax-accounting.component';
import { ViewmorePrepareFinancialStatementsComponent } from './pages/viewmore-prepare-financial-statements/viewmore-prepare-financial-statements.component';
import { ViewmoreAccountingCommercialEnterprisesComponent } from './pages/viewmore-accounting-commercial-enterprises/viewmore-accounting-commercial-enterprises.component';
import { ViewmoreAccountingManufacturingEnterprisesComponent } from './pages/viewmore-accounting-manufacturing-enterprises/viewmore-accounting-manufacturing-enterprises.component';
import { ViewmoreAccountingServiceEnterprisesComponent } from './pages/viewmore-accounting-service-enterprises/viewmore-accounting-service-enterprises.component';
import { ViewmoreAccountingConstructionEnterprisesComponent } from './pages/viewmore-accounting-construction-enterprises/viewmore-accounting-construction-enterprises.component';
import { ViewmoreAccountingRestaurantHotelEnterprisesComponent } from './pages/viewmore-accounting-restaurant-hotel-enterprises/viewmore-accounting-restaurant-hotel-enterprises.component';
import { ViewmoreReadingComprehensionAndAnalyzingComponent } from './pages/viewmore-reading-comprehension-and-analyzing/viewmore-reading-comprehension-and-analyzing.component';
import { ViewmoreChiefAccountantComponent } from './pages/viewmore-chief-accountant/viewmore-chief-accountant.component';
import { ViewmoreFinancialManagementComponent } from './pages/viewmore-financial-management/viewmore-financial-management.component';


import { CommonModule } from '@angular/common';
import { NgxChatboxModule } from 'ngx-chatbox';
import { ViewmoreBsComponent } from './pages/research-content/viewmore-bs/viewmore-bs.component';
import { Viewmorebs2Component } from './pages/research-content/viewmorebs2/viewmorebs2.component';
import { Viewmorebs3Component } from './pages/research-content/viewmorebs3/viewmorebs3.component';
import { Viewmorebs4Component } from './pages/research-content/viewmorebs4/viewmorebs4.component';
import { Viewmorebs5Component } from './pages/research-content/viewmorebs5/viewmorebs5.component';
import { Viewmoredvkt1Component } from './pages/research-content/viewmoredvkt1/viewmoredvkt1.component';
import { Viewmoredvkt2Component } from './pages/research-content/viewmoredvkt2/viewmoredvkt2.component';
import { Viewmoredvkt3Component } from './pages/research-content/viewmoredvkt3/viewmoredvkt3.component';
import { Viewmoredvkt4Component } from './pages/research-content/viewmoredvkt4/viewmoredvkt4.component';
import { Viewmoredvkt5Component } from './pages/research-content/viewmoredvkt5/viewmoredvkt5.component';
import { Viewmoredvkt6Component } from './pages/research-content/viewmoredvkt6/viewmoredvkt6.component';
import { Viewmoredvkt7Component } from './pages/research-content/viewmoredvkt7/viewmoredvkt7.component';
import { Viewmoredvkt8Component } from './pages/research-content/viewmoredvkt8/viewmoredvkt8.component';
import { Slide1Component } from './pages/home/slide1/slide1.component';
import { MediaContentComponent } from './pages/media/media-content/media-content.component';
import { ViewmoreSystemComponent } from './pages/media/viewmore-system/viewmore-system.component';
import { NewTT200Component } from './pages/media/new-tt200/new-tt200.component';
import { NewFeaturesTt200Component } from './pages/media/new-features-tt200/new-features-tt200.component';
import { Tt113Component } from './pages/media/tt113/tt113.component';
import { HDLDComponent } from './pages/media/hdld/hdld.component';
import { ThueGtgtComponent } from './pages/home/thue-gtgt/thue-gtgt.component';
import { ThueGtgtTt80Component } from './pages/home/thue-gtgt-tt80/thue-gtgt-tt80.component';
import { ThueTncnComponent } from './pages/home/thue-tncn/thue-tncn.component';
import { ThueTndnComponent } from './pages/home/thue-tndn/thue-tndn.component';
import { WorkExperienceComponent } from './pages/home/work-experience/work-experience.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './core/services/authentication.service';
import { JwtInterceptor } from '@core/interceptors/jwt.interceptor';
import { ErrorInterceptor } from '@core/interceptors/error.interceptor';
import { TinTucDetailComponent } from './pages/home/tin-tuc-detail/tin-tuc-detail.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    FootersComponent,
    LayoutsComponent,
    HomeComponent,
    CoursesComponent,
    ResearchComponent,
    ProductComponent,
    BlogsComponent,
    ContactComponent,
    ContentComponent,
    SlideComponent,
    ResearchContentComponent,
    MediaComponent,
    ProductContentComponent,
    ChatboxComponent,
    ViewmoreContentComponent,
    RegisterComponent,
    ViewmoreTaxAccountingComponent,
    ViewmorePrepareFinancialStatementsComponent,
    ViewmoreAccountingCommercialEnterprisesComponent,
    ViewmoreAccountingManufacturingEnterprisesComponent,
    ViewmoreAccountingServiceEnterprisesComponent,
    ViewmoreAccountingConstructionEnterprisesComponent,
    ViewmoreAccountingRestaurantHotelEnterprisesComponent,
    ViewmoreReadingComprehensionAndAnalyzingComponent,
    ViewmoreChiefAccountantComponent,
    ViewmoreFinancialManagementComponent,
    ViewmoreBsComponent,
    Viewmorebs2Component,
    Viewmorebs3Component,
    Viewmorebs4Component,
    Viewmorebs5Component,
    Viewmoredvkt1Component,
    Viewmoredvkt2Component,
    Viewmoredvkt3Component,
    Viewmoredvkt4Component,
    Viewmoredvkt5Component,
    Viewmoredvkt6Component,
    Viewmoredvkt7Component,
    Viewmoredvkt8Component,
    Slide1Component,
    MediaContentComponent,
    ViewmoreSystemComponent,
    NewTT200Component,
    NewFeaturesTt200Component,
    Tt113Component,
    HDLDComponent,
    ThueGtgtComponent,
    ThueGtgtTt80Component,
    ThueTncnComponent,
    ThueTndnComponent,
    WorkExperienceComponent,
    TinTucDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatCarouselModule,
    SlickCarouselModule,
    MatDialogModule,
    FormsModule,
    NgxChatboxModule,
    CommonModule,
    AngularEditorModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    // {
    //   provide:APP_INITIALIZER,
    //   useFactory:appInitializer,
    //   multi:true,
    //   deps:[LanguageChangeSharingService, AuthenticationService]
    // },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
