import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsComponent } from '@features/layouts/layouts.component';
import { MediaComponent } from './pages/media/media.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductComponent } from './pages/products/products.component';
import { ResearchComponent } from './pages/research/research.component';
import { SlideComponent } from './pages/slide/slide.component';
import { ViewmoreContentComponent } from './pages/viewmore-content/viewmore-content.component';
import { ViewmoreTaxAccountingComponent } from './pages/viewmore-tax-accounting/viewmore-tax-accounting.component';
import { ViewmorePrepareFinancialStatementsComponent } from './pages/viewmore-prepare-financial-statements/viewmore-prepare-financial-statements.component';
import { ViewmoreAccountingCommercialEnterprisesComponent } from './pages/viewmore-accounting-commercial-enterprises/viewmore-accounting-commercial-enterprises.component';
import { ViewmoreAccountingManufacturingEnterprisesComponent } from './pages/viewmore-accounting-manufacturing-enterprises/viewmore-accounting-manufacturing-enterprises.component';
import { ViewmoreAccountingServiceEnterprisesComponent } from './pages/viewmore-accounting-service-enterprises/viewmore-accounting-service-enterprises.component';
import { ViewmoreAccountingConstructionEnterprisesComponent } from './pages/viewmore-accounting-construction-enterprises/viewmore-accounting-construction-enterprises.component';
import { ViewmoreAccountingRestaurantHotelEnterprisesComponent } from './pages/viewmore-accounting-restaurant-hotel-enterprises/viewmore-accounting-restaurant-hotel-enterprises.component';
import { ViewmoreReadingComprehensionAndAnalyzingComponent } from './pages/viewmore-reading-comprehension-and-analyzing/viewmore-reading-comprehension-and-analyzing.component';
import { ViewmoreChiefAccountantComponent } from './pages/viewmore-chief-accountant/viewmore-chief-accountant.component';
import { ViewmoreFinancialManagementComponent } from './pages/viewmore-financial-management/viewmore-financial-management.component';
import { ProductContentComponent } from './pages/product-content/product-content.component';
import { ViewmoreBsComponent } from './pages/research-content/viewmore-bs/viewmore-bs.component';
import { Viewmorebs2Component } from './pages/research-content/viewmorebs2/viewmorebs2.component';
import { Viewmorebs3Component } from './pages/research-content/viewmorebs3/viewmorebs3.component';
import { Viewmorebs4Component } from './pages/research-content/viewmorebs4/viewmorebs4.component';
import { Viewmorebs5Component } from './pages/research-content/viewmorebs5/viewmorebs5.component';
import { Viewmoredvkt1Component } from './pages/research-content/viewmoredvkt1/viewmoredvkt1.component';
import { Viewmoredvkt2Component } from './pages/research-content/viewmoredvkt2/viewmoredvkt2.component';
import { Viewmoredvkt3Component } from './pages/research-content/viewmoredvkt3/viewmoredvkt3.component';
import { Viewmoredvkt4Component } from './pages/research-content/viewmoredvkt4/viewmoredvkt4.component';
import { Viewmoredvkt5Component } from './pages/research-content/viewmoredvkt5/viewmoredvkt5.component';
import { Viewmoredvkt6Component } from './pages/research-content/viewmoredvkt6/viewmoredvkt6.component';
import { Viewmoredvkt7Component } from './pages/research-content/viewmoredvkt7/viewmoredvkt7.component';
import { Viewmoredvkt8Component } from './pages/research-content/viewmoredvkt8/viewmoredvkt8.component';
import { Slide1Component } from './pages/home/slide1/slide1.component';
import { ViewmoreSystemComponent } from './pages/media/viewmore-system/viewmore-system.component';
import { NewTT200Component } from './pages/media/new-tt200/new-tt200.component';
import { NewFeaturesTt200Component } from './pages/media/new-features-tt200/new-features-tt200.component';
import { Tt113Component } from './pages/media/tt113/tt113.component';
import { HDLDComponent } from './pages/media/hdld/hdld.component';
import { ThueGtgtComponent } from './pages/home/thue-gtgt/thue-gtgt.component';
import { ThueGtgtTt80Component } from './pages/home/thue-gtgt-tt80/thue-gtgt-tt80.component';
import { ThueTncnComponent } from './pages/home/thue-tncn/thue-tncn.component';
import { ThueTndnComponent } from './pages/home/thue-tndn/thue-tndn.component';
import { WorkExperienceComponent } from './pages/home/work-experience/work-experience.component';
import { TinTucDetailComponent } from './pages/home/tin-tuc-detail/tin-tuc-detail.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutsComponent,
    children: [
      {
        path: 'trang-chu',
        component:HomeComponent,
        pathMatch:'full'
      },
      {
        path: '',
        redirectTo:'/trang-chu',
        pathMatch:'full'
      },
      {
        path:'dao-tao',
        component:CoursesComponent,
        pathMatch:'full'
      },
      {
        path:'dich-vu-doanh-nghiep',
        component:ResearchComponent,
        pathMatch:'full'
      },
      {
        path:'viec-lam-ke-toan',
        component:ProductComponent,
        pathMatch:'full'
      },
      {
        path:'kien-thuc',
        component:MediaComponent,
        pathMatch:'full'
      },
      {
        path: 'truyen-thong',
        component: BlogsComponent,
        pathMatch: 'full'
      },
      {
        path:'lien-he',
        component:ContactComponent,
        pathMatch:'full'
      },
      {
        path: '',
        component: SlideComponent
      },
      {
        path: 'nguyen-ly-ke-toan',
        component: ViewmoreContentComponent
      },
      {
        path: 'ke-toan-thue',
        component: ViewmoreTaxAccountingComponent
      },
      {
        path: 'huong-dan-len-bao-cao-tai-chinh',
        component: ViewmorePrepareFinancialStatementsComponent
      },
      {
        path: 'ke-toan-tong-hop-trong-doanh-nghiep-thuong-mai',
        component: ViewmoreAccountingCommercialEnterprisesComponent
      },
      {
        path: 'ke-toan-tong-hop-trong-doanh-nghiep-san-xuat',
        component: ViewmoreAccountingManufacturingEnterprisesComponent
      },
      {
        path: 'ke-toan-tong-hop-trong-doanh-nghiep-dich-vu',
        component: ViewmoreAccountingServiceEnterprisesComponent
      },
      {
        path: 'ke-toan-tong-hop-trong-doanh-nghiep-xay-dung',
        component: ViewmoreAccountingConstructionEnterprisesComponent
      },
      {
        path: 'ke-toan-tong-hop-trong-doanh-nghiep-nha-hang-khach-san',
        component: ViewmoreAccountingRestaurantHotelEnterprisesComponent
      },
      {
        path: 'doc-hieu-va-phan-tich-bao-cao-tai-chinh',
        component: ViewmoreReadingComprehensionAndAnalyzingComponent
      },
      {
        path: 'ke-toan-truong',
        component: ViewmoreChiefAccountantComponent
      },
      {
        path: 'quan-tri-tai-chinh-doanh-nghiep',
        component: ViewmoreFinancialManagementComponent,
      },
      {
        path: 'detail/:job_name',
        component: ProductContentComponent},
      {
        path: 'thanh-lap-doanh-nghiep',
        component: ViewmoreBsComponent
      },
      {
        path: 'mo-ma-so-thue-doanh-nghiep',
        component: Viewmorebs2Component
      },
      {
        path: 'thay-doi-dang-ky-doanh-nghiep',
        component: Viewmorebs3Component
      },
      {
        path: 'chot-thue-chuyen-quan',
        component: Viewmorebs4Component
      },
      {
        path: 'giai-the-tam-ngung-hoat-dong',
        component: Viewmorebs5Component
      },
      {
        path: 'dich-vu-ke-toan-thue-tron-goi',
        component: Viewmoredvkt1Component
      },
      {
        path: 'lam-bao-cao-tai-chinh',
        component: Viewmoredvkt2Component
      },
      {
        path: 'quyet-toan-thue',
        component: Viewmoredvkt3Component
      },
      {
        path: 'hoan-thue',
        component: Viewmoredvkt4Component
      },
      {
        path: 'dich-vu-soat-xet-so-sach-ke-toan',
        component: Viewmoredvkt5Component
      },
      {
        path: 'dich-vu-bao-hiem',
        component: Viewmoredvkt6Component
      },
      {
        path: 'dich-vu-ke-toan-noi-bo',
        component: Viewmoredvkt7Component
      },
      {
        path: 'dao-tao-nhan-su-ke-toan-tai-don-vi',
        component: Viewmoredvkt8Component
      },
      {
        path: 'slide-1',
        component: Slide1Component
      },
      {
        path: 'he-thong-chuan-muc-ke-toan-viet-nam',
        component: ViewmoreSystemComponent
      },
      {
        path: 'nhung-diem-moi-cua-tt200',
        component: NewTT200Component
      },
      {
        path: 'nhung-noi-dung-moi-trong-tt200',
        component: NewFeaturesTt200Component
      },
      {
        path: 'thong-tu-113',
        component: Tt113Component
      },
      {
        path: 'nhung-diem-moi-cua-bo-luat-lao-dong',
        component: HDLDComponent
      },
      {
        path: 'viewmore-GTGT-2%',
        component: ThueGtgtComponent
      },
      {
        path: 'viewmore-GTGT-TT80',
        component: ThueGtgtTt80Component
      },
      {
        path: 'viewmore-TNCN',
        component: ThueTncnComponent
      },
      {
        path: 'viewmore-TNDN',
        component: ThueTndnComponent
      },
      {
        path: 'viewmore-work-ex',
        component: WorkExperienceComponent
      },
      {
        path: 'tin-tuc-detail/:news_name',
        component: TinTucDetailComponent
      }
    ],
  },
  { path: 'login', loadChildren: () => import('./pages/Account/login/login.module').then(m => m.LoginModule) },
  {
    path: 'admin',
    loadChildren: () =>
      import('./admin/layout/admin-layout.module').then(
        (m) => m.AdminLayoutModule
      ),
  },
  { path: 'news-management', loadChildren: () => import('./admin/page/news-management/news-management.module').then(m => m.NewsManagementModule) },
  { path: 'job-management', loadChildren: () => import('./admin/page/job-management/job-management.module').then(m => m.JobManagementModule) },
  { path: 'news-detail', loadChildren: () => import('./admin/page/news-management/news-detail/news-detail.module').then(m => m.NewsDetailModule) },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
